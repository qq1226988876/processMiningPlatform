package team.b8311.processMiningPlatform.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import team.b8311.processMiningPlatform.utils.HDFSUtils;

@RunWith(SpringJUnit4ClassRunner.class) // Spring单元测试
@ContextConfiguration({ "classpath*:spring-*.xml" }) // 上下文配置
public class HDFSUtilsTest {

	@Resource
	HDFSUtils hdfs;

	/**
	 * 测试上传文件
	 * 
	 * @throws Exception
	 */
	@Test
	public void testUploadFile() throws Exception {
		// 创建Spring的IOC容器对象
		// ApplicationContext ctx = new
		// ClassPathXmlApplicationContext("classpath:spring-hdfs.xml");
		// 从IOC容器中获取Bean实例
		// HDFSUtils hdfs = (HDFSUtils) ctx.getBean("hdfsUtils");
		InputStream input = new FileInputStream(new File("d:" + File.separator + "demo" + File.separator + "my.txt"));
		System.out.println(hdfs.uploadFile(input, HDFSUtils.RAWLOG_PATH_PREFIX));
		// 关闭输入流
		input.close();
	}

	@Test
	public void testDownloadFile() throws Exception {
		InputStream input = hdfs.downloadFile("/wqt/process_mining/rawlog/f59df595-0aa0-4847-b905-1e53723215f2");
		byte data[] = new byte[1024];
		int len = input.read(data);
		System.out.println("[" + new String(data, 0, len) + "]");
	}

}
