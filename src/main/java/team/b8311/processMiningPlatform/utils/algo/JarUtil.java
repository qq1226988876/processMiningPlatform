package team.b8311.processMiningPlatform.utils.algo;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import java.util.List;

import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import team.b8311.processMiningPlatform.controller.MiningController;



public class JarUtil {


	static File file;
	public static HashMap<String,HashMap<String, List<String>>> parseJar(File file){
		JarUtil.file = file;
		try {
			return findClassesFromJar(findClasses(file));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	private static HashMap<String, List<String>> findClasses(File file) {
		
		HashMap<String, List<String>> map= new HashMap<String, List<String>>(); 
		try { 
		final ZipFile zipfile = new ZipFile(file); 
		final Enumeration entries = zipfile.entries(); 
		while (entries.hasMoreElements()) { 
		final ZipEntry entry = (ZipEntry) entries.nextElement(); 
		final String name = entry.getName(); 
		List list = null; 
		if (name.endsWith(".class")) { 
		String packageStr = name.replace("/",".").substring(0, name.lastIndexOf("/")); 
		if (map.containsKey(packageStr)) { 
		list = (List ) map.remove(packageStr); 
		} 
		if (list == null) { 
		list = new ArrayList (); 
		} 
		list.add(name.replace("/",".").substring(0, name.length() - 6)); 
		map.put(packageStr, list); 
		} 
		} 
		
		} catch (IOException e) { 
		System.out.println("IOException:"+ e.getMessage()); 
		
		} 
		return map;
	}
	/**
	 * 返回包和类名对应的哈希表
	 * @return
	 * @throws ClassNotFoundException 
	 */
	private static HashMap<String,HashMap<String, List<String>>> findClassesFromJar(HashMap<String, List<String>> packMap) throws ClassNotFoundException {
		HashMap<String,HashMap<String, List<String>>> jarMap= new HashMap<String,HashMap<String, List<String>>>(); 
		HashMap<String, List<String>> clzMap = new HashMap<>();
		for(String packName:packMap.keySet()) {
			List<String> clzList = packMap.get(packName);
			for(String clz:clzList) {
				List<String> methodList = getClassMethods(clz);
				clzMap.put(clz, methodList);
			}
			jarMap.put(packName, clzMap);
		}
		return jarMap;
	}

	public static URLClassLoader getLoader() throws MalformedURLException { 
		
		URL url = file.toURI().toURL();

		URLClassLoader loader = new URLClassLoader(new URL[] { url }, JarUtil.class.getClassLoader());
		return loader; 
	}
	
	/**
	 * 获取类的所有方法
	 * @param className
	 * @return
	 * @throws ClassNotFoundException
	 */
	public static List<String> getClassMethods(String className) throws ClassNotFoundException {
		Class clz = null;
		try {
			clz = getLoader().loadClass(className);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		Method[] methods = clz.getMethods();
		int len = methods.length;
		List<String> methodList = new ArrayList<>();
		if(len>9) {
			//后9个函数为类的默认函数
			for(int i =0;i<len-9;i++) {
				methodList.add(methods[i].getName());	
			}
		}
		return methodList;
		
	}
	
	
}
