package team.b8311.processMiningPlatform.controller;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import team.b8311.processMiningPlatform.domain.ResponseObj;
import team.b8311.processMiningPlatform.domain.User;
import team.b8311.processMiningPlatform.service.impl.UserServiceImpl;
import team.b8311.processMiningPlatform.utils.MD5Utils;
import team.b8311.processMiningPlatform.utils.SendMailUtils;

/**
 * 用户请求相关控制器
 * 
 * @author qiuto
 *
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/userAction") // 外层地址
public class UserController {

	@Resource
	private UserServiceImpl userService; // 自动载入service对象
	private ResponseObj responseObj;

	/**
	 * 用户登录接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param user
	 *            发起请求后，spring接受请求，然后封装的bean数据
	 * @return 登录结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/login" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json数据类型
	@ResponseBody // 如果为了返回数据为json，那么我们需要设定某个方法对应的注解为@ResponseBody,否则会出现404错误
	public Object login(HttpServletRequest req, HttpServletResponse res, User user) throws Exception {
		Object result;
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<User>();
		if (null == user) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("登录信息不能为空");
			return responseObj;
		}
		if (user.getEmail().isEmpty() || user.getPassword().isEmpty()) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("用户名密码不能为空");
			return responseObj;
		}
		// 查找用户
		System.out.println("用户是：" + user.toString() + "\n");
		User user1 = userService.get(user.getEmail());
		if (null == user1) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("未找到该用户");
			result = responseObj;
		} else {
			if (MD5Utils.MD5(user.getPassword()).equals(user1.getPassword())) {
				responseObj.setCode(ResponseObj.OK);
				responseObj.setMsg(ResponseObj.OK_STR);
				user.setPassword(req.getSession().getId());
				responseObj.setData(user1);
				result = responseObj;
			} else {
				responseObj.setCode(ResponseObj.FAILED);
				responseObj.setMsg("用户名或密码错误");
				result = responseObj;
			}
		}
		return result;
	}

	/**
	 * 管理员登录接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param user
	 *            发起请求后，spring接受请求，然后封装的bean数据
	 * @return 登录结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/adminLogin" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json数据类型
	@ResponseBody // 如果为了返回数据为json，那么我们需要设定某个方法对应的注解为@ResponseBody,否则会出现404错误
	public Object adminLogin(HttpServletRequest req, HttpServletResponse res, User user) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<User>();
		User user1 = userService.get(user.getEmail());

		if ((user.getPassword()).equals(user1.getPassword())) {
			// 如果是管理员
			if (user1.getType()) {
				responseObj.setCode(ResponseObj.OK);
				responseObj.setMsg(ResponseObj.OK_STR);
				responseObj.setData(user1);
			} else {
				responseObj.setCode(ResponseObj.FAILED);
				responseObj.setMsg("账号错误，请检查");
			}

		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("密码错误，请检查");
		}

		return responseObj;
	}

	/**
	 * 添加用户
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param user
	 *            发起请求后，spring接受请求，然后封装的bean数据
	 * @return 登录结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/addUser" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json数据类型
	@ResponseBody // 如果为了返回数据为json，那么我们需要设定某个方法对应的注解为@ResponseBody,否则会出现404错误
	public Object addUser(HttpServletRequest req, HttpServletResponse res, User user) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<User>();
		boolean isSuccess = userService.insert(user);
		if (isSuccess) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setData(user);
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("账号错误，请检查");
		}
		return responseObj;
	}

	/**
	 * 添加用户
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param user
	 *            发起请求后，spring接受请求，然后封装的bean数据
	 * @return 登录结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteUser" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json数据类型
	@ResponseBody // 如果为了返回数据为json，那么我们需要设定某个方法对应的注解为@ResponseBody,否则会出现404错误
	public Object deleteUser(HttpServletRequest req, HttpServletResponse res, int id) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<User>();
		boolean isSuccess = userService.delete(id);
		if (isSuccess) {
			responseObj.setCode(ResponseObj.OK);
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("账号错误，请检查");
		}
		return responseObj;
	}

	/**
	 * 获取基本用户
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param user
	 *            发起请求后，spring接受请求，然后封装的bean数据
	 * @return 登录结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/getBasicUser" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json数据类型
	@ResponseBody // 如果为了返回数据为json，那么我们需要设定某个方法对应的注解为@ResponseBody,否则会出现404错误
	public Object getBasicUser(HttpServletRequest req, HttpServletResponse res, User user) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<User>();
		User user1 = userService.get(user.getEmail());
		if (null == user1) {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("账号错误，请检查");
		} else {
			if ((user.getPassword()).equals(user1.getPassword())) {
				// 如果是管理员
				if (user1.getType()) {
					responseObj.setCode(ResponseObj.OK);
					responseObj.setMsg(ResponseObj.OK_STR);
					responseObj.setData(userService.getBasicUser());
				} else {
					responseObj.setCode(ResponseObj.FAILED);
				}

			} else {
				responseObj.setCode(ResponseObj.FAILED);
				responseObj.setMsg("密码错误，请检查");
			}
		}
		return responseObj;
	}

	/**
	 * 用户注册接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param user
	 *            发起请求后，spring接受请求，然后封装的bean数据
	 * @param session
	 *            http会话
	 * @return 注册结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/register" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json数据类型
	@ResponseBody // 如果为了返回数据为json，那么我们需要设定某个方法对应的注解为@ResponseBody,否则会出现404错误
	public Object register(HttpServletRequest req, HttpServletResponse res, User user, HttpSession session)
			throws Exception {
		Object result;
		String originHeader = req.getHeader("Origin");
		res.setHeader("Access-Control-Allow-Origin", originHeader); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		System.out.println(user);
		responseObj = new ResponseObj<>();
		if (null == user) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("用户信息不能为空");
			return responseObj;
		}
		if (user.getEmail().isEmpty() || user.getPassword().isEmpty()) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("用户名密码不能为空");
			return responseObj;
		}
		if (!session.getAttribute("email").toString().equals(user.getEmail())) {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("该用户邮箱不是已验证邮箱！");
			return responseObj;
		}
		// 查找用户
		if (null != userService.get(user.getEmail())) {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("该用户邮箱已注册！");
			return responseObj;
		}
		if (!userService.insert(user)) {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("注册失败！");
			result = responseObj;
		} else {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("注册成功！");
			// user.setPassword(session.getId()); //防止前端返回真正的密码
			// responseObj.setData(user);
			// session.setAttribute("userInfo", user);//将一些基本信息写入到session中
			result = responseObj;
		}
		return result;
	}

	/**
	 * 发送邮箱验证码接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param session
	 *            http会话
	 * @param email
	 *            要发送到的邮箱地址
	 * @return 结果信息
	 * @throws Exception
	 */
	@CrossOrigin
	@RequestMapping(value = "/sendCode" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json数据类型
	@ResponseBody // 如果为了返回数据为json，那么我们需要设定某个方法对应的注解为@ResponseBody,否则会出现404错误
	public Object sendCode(HttpServletRequest req, HttpServletResponse res, HttpSession session, String email)
			throws Exception {
		Object result;
		String originHeader = req.getHeader("Origin");
		res.setHeader("Access-Control-Allow-Origin", originHeader); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		res.setHeader("Access-Control-Allow-Credentials", "true");
		res.addCookie(new Cookie("session-id", session.getId()));
		SendMailUtils smu = new SendMailUtils();
		responseObj = new ResponseObj<>();
		smu.sendMail(email);
		System.out.println(email);
		System.out.println(smu.getGenerateCode());
		if (smu.getGenerateCode() != null) {
			String code = smu.getGenerateCode();
			session.setAttribute("code", code);
			session.setAttribute("email", email); // 保存传入的email值，使email和code验证码相对应
			System.out.println("验证码" + session.getAttribute("code") + "sessionId:" + req.getSession().getId());
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("发送验证码成功！");
			result = responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("发送验证码失败！");
			result = responseObj;
		}
		return result;
	}

	/**
	 * 验证邮箱验证码接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param session
	 *            http会话
	 * @param code
	 *            传入待验证的验证码
	 * @return
	 */
	@CrossOrigin
	@RequestMapping(value = "/verifyCode" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json数据类型
	@ResponseBody // 如果为了返回数据为json，那么我们需要设定某个方法对应的注解为@ResponseBody,否则会出现404错误
	public Object verifyCode(HttpServletRequest req, HttpServletResponse res, HttpSession session, String email,
			String verificationCode) {
		String originHeader = req.getHeader("Origin");
		res.setHeader("Access-Control-Allow-Origin", originHeader); // 由于前台和后台的session要保持一直，设置了
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		res.setHeader("Access-Control-Allow-Credentials", "true"); // 为了保存session

		// 是否支持cookie跨域
		responseObj = new ResponseObj<>();
		System.out.println("sessionId:" + req.getSession().getId() + "code:" + session.getAttribute("code"));
		System.out.println(session);
		if (session.getAttribute("code").toString().equals(verificationCode)
				&& session.getAttribute("email").toString().equals(email)) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("验证成功！");
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("验证失败！");
		}
		return responseObj;
	}
}
