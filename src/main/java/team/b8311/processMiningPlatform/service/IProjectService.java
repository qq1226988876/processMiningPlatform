package team.b8311.processMiningPlatform.service;

import java.util.List;
import java.util.Set;

import team.b8311.processMiningPlatform.domain.Project;

public interface IProjectService {
	/**
	 * 实现项目的插入操作，本次操作要调用的IProjectDAO接口的如下方法：<br>
	 * <li>需要调用IProjectDAO.findByNameUserId()方法，判断要增加的项目名是否存在
	 * <li>如果不存在则调用IProjectDAO.doCreateAndGetKey()方法，返回操作结果
	 * 
	 * @param project
	 *            包含了要增加数据的VO对象
	 * @return 如果增加项目名称重复则返回false，否则返回true
	 * @throws Exception
	 *             SQL执行异常
	 */
	public int insert(Project project) throws Exception;

	/**
	 * 实现项目的修改操作，本次操作要调用的IProjectDAO接口的如下方法：<br>
	 * <li>需要调用IProjectDAO.findByNameUserId()方法，判断要修改的项目名是否存在
	 * <li>需要调用IProjectDAO.findById()方法，判断要修改的项目名是否是其原有的项目名
	 * <li>调用IProjectDAO.doUpdate()方法进行修改，本次修改属于全部内容的修改
	 * 
	 * @param vo
	 *            包含了要修改的VO对象
	 * @return 修改成功返回true，否则要修改的项目名称已存在返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean update(Project vo) throws Exception;

	/**
	 * 根据项目id实现项目的删除操作，可以删除多个项目，调用IProjectDAO.doRemoveBatch()方法
	 * 
	 * @param ids
	 *            包含了全部要删除的数据集合，没有重复数据
	 * @return 删除成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean delete(Set<Integer> ids) throws Exception;

	/**
	 * 通过项目id查询项目信息，调用IProjectDAO.findById()
	 * 
	 * 
	 * @param projectId
	 *            项目id
	 * @return 如果表中有数据则封装为VO对象返回，否则返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Project get(int projectId) throws Exception;

	/**
	 * 通过用户id和项目名称查询项目信息，调用IProjectDAO.findByNameUserId()
	 * 
	 * @param projectName
	 *            项目名称
	 * @param userId
	 *            用户id
	 * @return 如果表中有数据则封装为VO对象返回，否则返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Project get(String projectName, int userId) throws Exception;

	/**
	 * 实现项目的模糊查询，要调用IProjectDAO.findByname()方法，结果以集合的形式返回
	 * 
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param userId
	 *            项目所属的用户id
	 * @return 如果表中有数据则封装为VO对象以List集合的形式返回<br>
	 *         如果没有数据则集合的长度为0
	 * @throws Exception
	 *             SQL执行异常
	 */
	public List<Project> list(String keyWord, int userId) throws Exception;

}
