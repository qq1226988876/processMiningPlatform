package team.b8311.processMiningPlatform.utils.request;

import java.io.Serializable;
import java.util.List;

/**
 * 封装请求流程挖掘的数据
 * 
 * @author qiuto
 *
 */
public class MiningData implements Serializable {
	private static final long serialVersionUID = -2272929565556702825L;

	private Integer logId;

	private Integer methodId;

	private List<AlgoParam> paramList;

	private Integer imageType;

	private String rrAttr; // 力导向图的事件属性

	public Integer getLogId() {
		return logId;
	}

	public void setLogId(Integer logId) {
		this.logId = logId;
	}

	public Integer getMethodId() {
		return methodId;
	}

	public void setMethodId(Integer methodId) {
		this.methodId = methodId;
	}

	public Integer getImageType() {
		return imageType;
	}

	public void setImageType(Integer imageType) {
		this.imageType = imageType;
	}

	public List<AlgoParam> getParamList() {
		return paramList;
	}

	public void setParamList(List<AlgoParam> paramList) {
		this.paramList = paramList;
	}

	// 重写此方法的目的是将该类作为缓存中的key
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append("MiningData ");
		sb.append(this.logId);
		sb.append(";");
		sb.append(this.methodId);
		sb.append(";");

		if (this.paramList != null) {
			for (AlgoParam param : this.paramList) {
				sb.append(param.getValue());
				sb.append(";");
			}
		}
		return sb.toString();
	}

	public String getRrAttr() {
		return rrAttr;
	}

	public void setRrAttr(String rrAttr) {
		this.rrAttr = rrAttr;
	}
}
