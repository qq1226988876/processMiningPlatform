package team.b8311.processMiningPlatform.support.generateEventlog.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PetriNet {

	private Map<String, Place> places; // 库所集
	private Map<String, Transition> transitions; // 变迁集
	private List<FlowRelation> flowRelations; // 流关系集

	public PetriNet() {
		// 无参构造函数
	}

	public Map<String, Place> getPlaces() {
		return places;
	}

	public void setPlaces(Map<String, Place> places) {
		this.places = places;
	}

	public Map<String, Transition> getTransitions() {
		return transitions;
	}

	public void setTransitions(Map<String, Transition> transitions) {
		this.transitions = transitions;
	}

	public List<FlowRelation> getFlowRelations() {
		return flowRelations;
	}

	public void setFlowRelations(List<FlowRelation> flowRelations) {
		this.flowRelations = flowRelations;
	}

	@Override
	public String toString() {
		return "PetriNet [places=" + places + ", transitions=" + transitions + ", flowRelations=" + flowRelations + "]";
	}

	/**
	 * 初始化petri网
	 */
	public void initPetriNet() {
		this.getPlace(0).setTokenNum(1);
		this.getPlace(1).setTokenNum(0);
	}

	/**
	 * 获得结点的前驱编号
	 * 
	 * @param nodeId
	 * @return
	 */
	public List<Integer> priorNodes(int nodeId) {
		List<Integer> nodes = new ArrayList<Integer>();

		Iterator<FlowRelation> iter = this.flowRelations.iterator();
		while (iter.hasNext()) {
			FlowRelation fr = iter.next();
			if (fr.getTo().intValue() == nodeId) {
				nodes.add(fr.getFrom());
			}
		}
		return nodes;
	}

	/**
	 * 获得结点的后继编号
	 * 
	 * @param nodeId
	 * @return
	 */
	public List<Integer> nextNodes(int nodeId) {
		List<Integer> nodes = new ArrayList<Integer>();

		Iterator<FlowRelation> iter = this.flowRelations.iterator();
		while (iter.hasNext()) {
			FlowRelation fr = iter.next();
			if (fr.getFrom().intValue() == nodeId) {
				nodes.add(fr.getTo());
			}
		}
		return nodes;
	}

	/**
	 * 从库所集中获取库所
	 * 
	 * @param placeId
	 * @return
	 */
	public Place getPlace(int placeId) {
		return this.places.get(String.valueOf(placeId));
	}

	/**
	 * 从变迁集中获取变迁
	 * 
	 * @param transitionId
	 * @return
	 */
	public Transition getTransition(int transitionId) {
		return this.transitions.get(String.valueOf(transitionId));
	}

	/**
	 * 判断变迁是否使能
	 * 
	 * @param transitionId
	 * @return
	 */
	public boolean isenabled(int transitionId) {
		List<Integer> nodes = this.priorNodes(transitionId); // 取得前驱库所
		int tokenPlaceNum = 0; // 含有托肯的前驱库所数目
		Iterator<Integer> iter = nodes.iterator();
		while (iter.hasNext()) {
			int p = iter.next();
			if (this.getPlace(p).getTokenNum().intValue() > 0) { // 库所中含有托肯
				tokenPlaceNum++;
			}
		}
		if (tokenPlaceNum < nodes.size()) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 变迁消耗前驱库所的托肯并生成后继库所的托肯
	 * 
	 * @param transitionId
	 */
	public void tranformToken(int transitionId) {
		List<Integer> priorPlaces = this.priorNodes(transitionId); // 前驱库所
		Iterator<Integer> iter = priorPlaces.iterator();
		while (iter.hasNext()) {
			this.getPlace(iter.next()).consumeToken();
		}

		List<Integer> nextPlaces = this.nextNodes(transitionId); // 后继库所
		Iterator<Integer> iter2 = nextPlaces.iterator();
		while (iter2.hasNext()) {
			this.getPlace(iter2.next()).produceToken();
		}
	}

}
