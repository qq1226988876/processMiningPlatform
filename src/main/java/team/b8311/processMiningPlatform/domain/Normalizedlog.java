package team.b8311.processMiningPlatform.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 创建规范化日志表：<br>
 * CREATE TABLE `normalizedlog` (<br>
 * `normalizedlog_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT
 * '规范化日志id',<br>
 * `normalizedlog_name` varchar(50) NOT NULL COMMENT '规范化日志名',<br>
 * `normalizedlog_format` varchar(10) NOT NULL COMMENT '规范化日志格式',<br>
 * `normalizedlog_createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON
 * UPDATE CURRENT_TIMESTAMP COMMENT '规范化日 志创建时间',<br>
 * `normalizedlog_hdfs_id` varchar(50) NOT NULL COMMENT 'HDFS文件id',<br>
 * `rawlog_id` int(10) unsigned DEFAULT NULL COMMENT '原始日志id',<br>
 * `eventlog_id` int(10) unsigned DEFAULT NULL COMMENT '事件日志id',<br>
 * `project_id` int(10) unsigned DEFAULT NULL COMMENT '项目id',<br>
 * PRIMARY KEY (`normalizedlog_id`),<br>
 * UNIQUE KEY `nomalizedlog_name` (`normalizedlog_name`),<br>
 * KEY `IDX_NORMALIZEDLOG_PROJECT_ID` (`project_id`),<br>
 * KEY `IDX_NORMALIZEDLOG_EVENTLOG_ID` (`eventlog_id`),<br>
 * KEY `IDX_NORMALIZEDLOG_RAWLOG_ID` (`rawlog_id`)<br>
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 * 
 * @author qiuto
 *
 */
public class Normalizedlog implements Serializable {
	private Integer normalizedlogId; // 规范化日志id
	private String normalizedlogName; // 规范化日志名
	private String normalizedlogFormat; // 规范化日志格式
	private Date normalizedlogCreatetime; // 规范化日志创建时间
	private String normalizedlogHDFSId; // HDFS文件id

	private Project project; // 所属项目
	private Rawlog rawlog; // 对应的原始日志
	private Eventlog eventlog; // 对应的事件日志

	public Normalizedlog() {
		// 无参构造方法
	}

	public Integer getNormalizedlogId() {
		return normalizedlogId;
	}

	public void setNormalizedlogId(Integer normalizedlogId) {
		this.normalizedlogId = normalizedlogId;
	}

	public String getNormalizedlogName() {
		return normalizedlogName;
	}

	public void setNormalizedlogName(String normalizedlogName) {
		this.normalizedlogName = normalizedlogName;
	}

	public String getNormalizedlogFormat() {
		return normalizedlogFormat;
	}

	public void setNormalizedlogFormat(String normalizedlogFormat) {
		this.normalizedlogFormat = normalizedlogFormat;
	}

	public Date getNormalizedlogCreatetime() {
		return normalizedlogCreatetime;
	}

	public void setNormalizedlogCreatetime(Date normalizedlogCreatetime) {
		this.normalizedlogCreatetime = normalizedlogCreatetime;
	}

	public String getNormalizedlogHDFSId() {
		return normalizedlogHDFSId;
	}

	public void setNormalizedlogHDFSId(String normalizedlogHDFSId) {
		this.normalizedlogHDFSId = normalizedlogHDFSId;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Rawlog getRawlog() {
		return rawlog;
	}

	public void setRawlog(Rawlog rawlog) {
		this.rawlog = rawlog;
	}

	public Eventlog getEventlog() {
		return eventlog;
	}

	public void setEventlog(Eventlog eventlog) {
		this.eventlog = eventlog;
	}

	@Override
	public String toString() {
		return "Normalizedlog [normalizedlogId=" + normalizedlogId + ", normalizedlogName=" + normalizedlogName
				+ ", normalizedlogFormat=" + normalizedlogFormat + ", normalizedlogCreatetime="
				+ normalizedlogCreatetime + ", normalizedlogHDFSId=" + normalizedlogHDFSId + "]";
	}

}
