package team.b8311.processMiningPlatform.support.generateEventlog.operation;

import java.util.concurrent.CountDownLatch;

/**
 * 遍历库所线程
 * 
 * @author qiuto
 *
 */
public class PlaceThread implements Runnable {
	private Traverse traverse; // 遍历类
	private CountDownLatch latch; // 线程类中持有CountDownLatch对象的引用

	public PlaceThread(Traverse traverse, CountDownLatch latch) {
		super();
		this.traverse = traverse;
		this.latch = latch;
	}

	@Override
	public void run() {
		while (!this.traverse.isEnd()) {
			this.traverse.traversePlace();
		}
		this.latch.countDown();
	}

}
