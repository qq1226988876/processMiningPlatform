package team.b8311.processMiningPlatform.controller;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.deckfour.xes.model.XLog;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import team.b8311.processMiningPlatform.domain.Algorithm;
import team.b8311.processMiningPlatform.domain.Eventlog;
import team.b8311.processMiningPlatform.domain.ResponseObj;
import team.b8311.processMiningPlatform.exception.BaseExceptionHandleAction;
import team.b8311.processMiningPlatform.service.IAlgorithmService;
import team.b8311.processMiningPlatform.service.IEventlogService;
import team.b8311.processMiningPlatform.service.IMiningService;
import team.b8311.processMiningPlatform.support.eventlogParse.IEventlogParse;
import team.b8311.processMiningPlatform.support.eventlogParse.impl.EventlogParseImpl;
import team.b8311.processMiningPlatform.utils.HDFSUtils;
import team.b8311.processMiningPlatform.utils.MiningResult;
import team.b8311.processMiningPlatform.utils.request.AlgoParam;
import team.b8311.processMiningPlatform.utils.request.MiningData;

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/miningAction")
public class MiningController extends BaseExceptionHandleAction {
	@Resource
	private IAlgorithmService algorithmService;

	@Resource
	private IEventlogService eventlogService;

	@Resource
	private IMiningService miningService;

	@Resource
	private HDFSUtils hdfs; // 自动载入操作HDFS对象

	private ResponseObj responseObj;

	@ResponseBody
	@RequestMapping(value = "/mining" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8")
	public Object mining(@RequestBody MiningData miningdata, HttpServletRequest req, HttpServletResponse res,
			HttpSession session) throws Exception {
		
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj();
		
		
		
		// 从hdfs下载事件日志
		Integer eventLogId = miningdata.getLogId();
		Eventlog log = (Eventlog) eventlogService.get(eventLogId);
		if (log == null) {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("事件日志不存在，请检查。");
			responseObj.setData(null);
			return responseObj;
		}

		InputStream input = hdfs.downloadFile(HDFSUtils.EVENTLOG_PATH_PREFIX + log.getEventlogHDFSId());
		// 事件日志转换为XLOG
		IEventlogParse parser = new EventlogParseImpl();
		XLog xlog = parser.eventLogParse(input);
		input.close();
		if(xlog==null) {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("解析xes事件日志出错");
			responseObj.setData(null);
			return responseObj;
		}
		// 获取算法实体
		Algorithm algo = algorithmService.get(miningdata.getMethodId());
		if(algo==null) {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("算法不存在，请检查。");
			responseObj.setData(null);
			return responseObj;
		}
		// 算法参数
		HashMap<String, Object> paramMap = new HashMap<String, Object>();

		for (AlgoParam param : miningdata.getParamList()) {
			// System.out.println(param.getName() + " : " + param.getValue());
			switch (param.getType()) {
			//Integer
			case 0:
				paramMap.put(param.getName(), Integer.valueOf(param.getValue()));
				break;
			//Double
			case 1:
				paramMap.put(param.getName(), Double.valueOf(param.getValue()));
				break;
			//String
			case 2:
				paramMap.put(param.getName(), String.valueOf(param.getValue()));
				break;
			//Boolean
			case 3:
				paramMap.put(param.getName(), Boolean.valueOf(param.getValue()));
				break;
			}
		}

		// 挖掘
		//参数，日志，算法
		MiningResult ret = miningService.logMine(xlog, algo, paramMap, miningdata.getImageType());
		
		String usedTime = ret.getUsedTime();
		List<String> traces = ret.getTraces();
		List<String> allTraces = ret.getAllTraces();
		
		// Net net = ret.getNet();
		// Object formatter = null;
		// 生成对应的图
		// switch (miningdata.getImageType()) {
		// case ImageType.PetriNet:
		// formatter = (PetriNet)ret.getNet();
		// break;
		// case ImageType.TransitionSystem:
		// formatter = (TransitionSystem)ret.getNet();
		// break;
		// case ImageType.Sankey:
		// formatter = (Sankey)ret.getNet();
		// break;
		// case ImageType.ResourceRelation:
		// formatter = (ResourceRelation)ret.getNet();
		// break;
		// }
		// loader.close();
		
		
		responseObj.setCode(ResponseObj.OK);
		responseObj.setMsg("挖掘成功");
		responseObj.setData(ret.getJsonObject());
		return responseObj;

	}
}
