package team.b8311.processMiningPlatform.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.ResponseObj;
import team.b8311.processMiningPlatform.domain.User;
import team.b8311.processMiningPlatform.service.IProjectService;

/**
 * 项目管理模块控制器
 * 
 * @author qiuto
 *
 */
@Controller
@RequestMapping("/projectAction") // 外层地址
public class ProjectController {

	@Resource
	private IProjectService projectService; // 自动载入service对象
	private ResponseObj responseObj; // Api接口返回对象

	/**
	 * 新增项目接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param project
	 *            发起请求后，spring接受到请求，然后封装bean数据
	 * @return 结果信息
	 * @throws UnsupportedEncodingException
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/insert" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object insert(HttpServletRequest req, HttpServletResponse res, Project project, int userId)
			throws UnsupportedEncodingException {
		req.setCharacterEncoding("UTF-8");
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		User user = new User();
		user.setId(userId);
		project.setUser(user);
		System.out.println(project);
		responseObj = new ResponseObj<Project>();
		if (null == project) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("项目信息不能为空！");
			return responseObj;
		}
		if (project.getProjectName().isEmpty() || project.getProjectDescription().isEmpty()) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("项目名称或项目描述不能为空！");
			return responseObj;
		}
		try {
			if (projectService.insert(project) != 0) {
				responseObj.setCode(ResponseObj.OK);
				responseObj.setMsg("创建项目成功！");
				responseObj.setData(projectService.get(project.getProjectId()));
				return responseObj;
			} else {
				responseObj.setCode(ResponseObj.FAILED);
				responseObj.setMsg("项目名称重复！");
				return responseObj;
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("创建项目失败！");
			return responseObj;
		}
	}

	/**
	 * 更新项目信息接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param project
	 *            发起请求后，spring接受到请求，然后封装bean数据
	 * @return 更新项目结果
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/update" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型
	@ResponseBody
	public Object update(HttpServletRequest req, HttpServletResponse res, Project project, int userId)
			throws UnsupportedEncodingException {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		User user = new User();
		user.setId(userId);
		project.setUser(user);
		System.out.println(project);
		responseObj = new ResponseObj();
		if (null == project) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("项目信息不能为空！");
			return responseObj;
		}
		if (project.getProjectName().isEmpty() || project.getProjectDescription().isEmpty()) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("项目名称或项目描述不能为空！");
			return responseObj;
		}
		try {
			if (projectService.update(project)) {
				responseObj.setCode(ResponseObj.OK);
				responseObj.setMsg("更新项目成功！");
				return responseObj;
			} else {
				responseObj.setCode(ResponseObj.FAILED);
				responseObj.setMsg("项目名已存在！");
				return responseObj;
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("更新过程中出现问题！");
			return responseObj;
		}
	}

	/**
	 * 删除项目接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param ids
	 *            要删除的项目id集合
	 * @return 删除项目结果
	 */
	@RequestMapping(value = "/delete" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型
	@ResponseBody
	public Object delete(HttpServletRequest req, HttpServletResponse res, @RequestParam("ids") Set<Integer> ids) {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码

		System.out.println(ids);

		responseObj = new ResponseObj();
		if (ids.size() == 0) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("请选择要删除的项目");
			return responseObj;
		}
		try {
			if (projectService.delete(ids)) {
				responseObj.setCode(ResponseObj.OK);
				responseObj.setMsg("删除项目成功！");
				return responseObj;
			} else {
				responseObj.setCode(ResponseObj.FAILED);
				responseObj.setMsg("删除项目失败！");
				return responseObj;
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("删除过程中出现问题！");
			return responseObj;
		}
	}

	/**
	 * 查询项目名称是否存在接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param projectName
	 *            需要判断的projectName
	 * @param userId
	 *            项目所属的用户id
	 * @return 返回的结果信息
	 */
	@RequestMapping(value = "/get" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型
	@ResponseBody
	public Object get(HttpServletRequest req, HttpServletResponse res, String projectName, int userId) {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码

		responseObj = new ResponseObj();
		if (projectName.isEmpty()) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("项目名称不能为空！");
			return responseObj;
		}
		try {
			if (projectService.get(projectName, userId) != null) {
				responseObj.setCode(ResponseObj.FAILED);
				responseObj.setMsg("项目名称重复！");
				return responseObj;
			} else {
				responseObj.setCode(ResponseObj.OK);
				responseObj.setMsg("项目名称可用");
				return responseObj;
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("查询过程中出现其他问题！");
			return responseObj;
		}
	}

	/**
	 * 模糊查询项目信息接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param userId
	 *            登录的用户id
	 * @return 查询结果信息
	 */
	@RequestMapping(value = "/list" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型
	@ResponseBody
	public Object list(HttpServletRequest req, HttpServletResponse res, String keyWord, int userId) {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码

		responseObj = new ResponseObj();
		try {
			List<Project> projectList = projectService.list(keyWord, userId);
			if (projectList.size() == 0) {
				responseObj.setCode(ResponseObj.FAILED);
				responseObj.setMsg("没有要搜索的项目");
				return responseObj;
			} else {
				responseObj.setCode(ResponseObj.OK);
				responseObj.setMsg("搜索项目成功！");
				responseObj.setData(projectList);
				return responseObj;
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("搜索过程中出现问题！");
			return responseObj;
		}
	}
}
