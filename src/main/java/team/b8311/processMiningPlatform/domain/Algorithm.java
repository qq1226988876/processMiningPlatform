package team.b8311.processMiningPlatform.domain;

import java.io.Serializable;
import java.util.List;

/**
 * 创建算法表：<br>
 * CREATE TABLE `algorithm` (<br>
 * `algorithm_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '算法id',<br>
 * `algorithm_name` varchar(50) NOT NULL COMMENT '算法名称',<br>
 * `algorithm_path` varchar(255) NOT NULL COMMENT '算法的路径',<br>
 * `algorithm_config_path` varchar(255) NOT NULL COMMENT '算法配置文件的路径',<br>
 * `algorithm_classname` varchar(50) NOT NULL COMMENT '算法类名',<br>
 * `algorithm_package` varchar(255) NOT NULL COMMENT '算法包名',<br>
 * `algorithm_information` tinytext NOT NULL COMMENT '算法信息',<br>
 * `algorithm_method` varchar(50) NOT NULL COMMENT '执行算法的方法名',<br>
 * `algorithm_type` tinyint(1) NOT NULL COMMENT '算法类型（0为融合算法，1为挖掘算法）',<br>
 * PRIMARY KEY (`algorithm_id`)<br>
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 * 
 * @author qiuto
 *
 */
public class Algorithm implements Serializable {
	private Integer algorithmId; // 算法id
	private String algorithmName; // 算法名称
	private String algorithmPath; // 算法的路径
	private String algorithmConfigPath; // 算法配置文件的路径
	private String algorithmClassName; // 算法类名
	private String algorithmPackage; // 算法包名
	private String algorithmInformation; // 算法信息
	private String algorithmMethod; // 执行算法的方法名
	private Boolean algorithmType; // 算法类型（false为融合算法，true为挖掘算法）
	
	/**
	 * 添加于2019.01.10 ysl
	 */
	private Boolean algorithmIsAvailable; //算法是否可用
	private String algorithmOutputType;//算法的输出类型
	private String algorithmErrorMsg;//算法的错误信息
	
	private List<Eventlog> eventlogs; // 使用该算法的融合日志

	public Algorithm() {
		// 无参构造方法
	}

	public Integer getAlgorithmId() {
		return algorithmId;
	}

	public void setAlgorithmId(Integer algorithmId) {
		this.algorithmId = algorithmId;
	}

	public String getAlgorithmName() {
		return algorithmName;
	}

	public void setAlgorithmName(String algorithmName) {
		this.algorithmName = algorithmName;
	}

	public String getAlgorithmPath() {
		return algorithmPath;
	}

	public void setAlgorithmPath(String algorithmPath) {
		this.algorithmPath = algorithmPath;
	}

	public String getAlgorithmConfigPath() {
		return algorithmConfigPath;
	}

	public void setAlgorithmConfigPath(String algorithmConfigPath) {
		this.algorithmConfigPath = algorithmConfigPath;
	}

	public String getAlgorithmClassName() {
		return algorithmClassName;
	}

	public void setAlgorithmClassName(String algorithmClassName) {
		this.algorithmClassName = algorithmClassName;
	}

	public String getAlgorithmPackage() {
		return algorithmPackage;
	}

	public void setAlgorithmPackage(String algorithmPackage) {
		this.algorithmPackage = algorithmPackage;
	}

	public String getAlgorithmInformation() {
		return algorithmInformation;
	}

	public void setAlgorithmInformation(String algorithmInformation) {
		this.algorithmInformation = algorithmInformation;
	}

	public String getAlgorithmMethod() {
		return algorithmMethod;
	}

	public void setAlgorithmMethod(String algorithmMethod) {
		this.algorithmMethod = algorithmMethod;
	}

	public Boolean getAlgorithmType() {
		return algorithmType;
	}

	public void setAlgorithmType(Boolean algorithmType) {
		this.algorithmType = algorithmType;
	}

	public List<Eventlog> getEventlogs() {
		return eventlogs;
	}

	public void setEventlogs(List<Eventlog> eventlogs) {
		this.eventlogs = eventlogs;
	}

	@Override
	public String toString() {
		return "Algorithm [algorithmId=" + algorithmId + ", algorithmName=" + algorithmName + ", algorithmPath="
				+ algorithmPath + ", algorithmConfigPath=" + algorithmConfigPath + ", algorithmClassName="
				+ algorithmClassName + ", algorithmPackage=" + algorithmPackage + ", algorithmInformation="
				+ algorithmInformation + ", algorithmMethod=" + algorithmMethod + ", algorithmType=" + algorithmType
				+ ", algorithmIsAvailable=" + algorithmIsAvailable + ", algorithmOutputType=" + algorithmOutputType
				+ ", algorithmErrorMsg=" + algorithmErrorMsg + ", eventlogs=" + eventlogs + "]";
	}



	public String getAlgorithmOutputType() {
		return algorithmOutputType;
	}

	public void setAlgorithmOutputType(String algorithmOutputType) {
		this.algorithmOutputType = algorithmOutputType;
	}

	public String getAlgorithmErrorMsg() {
		return algorithmErrorMsg;
	}

	public void setAlgorithmErrorMsg(String algorithmErrorMsg) {
		this.algorithmErrorMsg = algorithmErrorMsg;
	}

	public Boolean getAlgorithmIsAvailable() {
		return algorithmIsAvailable;
	}

	public void setAlgorithmIsAvailable(Boolean algorithmIsAvailable) {
		this.algorithmIsAvailable = algorithmIsAvailable;
	}

}
