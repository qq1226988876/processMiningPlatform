package team.b8311.processMiningPlatform.exception;

import java.lang.reflect.Executable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import team.b8311.processMiningPlatform.domain.ResponseObj;

public class BaseExceptionHandleAction {

	// 基于@ExceptionHandler异常处理
	@ExceptionHandler
	@ResponseBody
	public Object handleAndReturnData(HttpServletRequest req, HttpServletResponse res, Exception ex) {
		ex.printStackTrace();
		System.out.println(ex.getClass());
		CustomException customException = null;
		ResponseObj<Object> responseObj = new ResponseObj<Object>(); // 初始化返回的json对象
		responseObj.setCode(responseObj.FAILED);
		if(ex instanceof org.xml.sax.SAXParseException) {
		//	customException = new CustomException("解析xes事件日志出错，请检查日志");
			responseObj.setMsg("解析xes事件日志出错，请检查日志");
			responseObj.setData(ex);
			return responseObj;
		}
		if (ex instanceof CustomException) {
			// 如果抛出的是系统自定义的异常则直接转换
			customException = (CustomException) ex;
			
		} else {
			// 如果抛出的不是系统自定义异常则重新构造一个未知错误异常
			customException = new CustomException("未知错误，请与系统管理员联系");
		}
		responseObj.setMsg(customException.getMessage());
		responseObj.setData(ex.toString());
		return responseObj;
		
		
	}
}
