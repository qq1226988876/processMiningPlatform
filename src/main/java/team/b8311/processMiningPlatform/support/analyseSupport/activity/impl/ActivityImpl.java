package team.b8311.processMiningPlatform.support.analyseSupport.activity.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeContinuousImpl;
import org.deckfour.xes.model.impl.XAttributeDiscreteImpl;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;

import team.b8311.processMiningPlatform.support.analyseSupport.activity.Activity;
import team.b8311.processMiningPlatform.support.eventlogParse.impl.EventlogParseImpl;

public class ActivityImpl implements Activity {

	@Override
	public Map<String, List<XEvent>> getEventList(XLog log, Map<Integer, String> names) {
		Map<String, List<XEvent>> map = null;
		if (log.size() <= 0) {
			return map;
		} else {
			map = new HashMap<String, List<XEvent>>();
			for (int i = 0; i < names.size(); i++) {
				map.put(names.get(i), new ArrayList<XEvent>());
			}
			for (XTrace trace : log) {
				if (trace.size() <= 0) {
					continue;
				}
				for (XEvent event : trace) {

					map.get(((XAttributeLiteralImpl) event.getAttributes().get("concept:name")).getValue()).add(event);
					// list.add(event);

				}
			}
		}
		return map;
	}

	@Override
	public Map<String, String> getEventAttribute(XLog log, Map<Integer, String> names, String attributeName,
			int attrType) {
		Map<String, String> returnMap = new HashMap<String, String>();
		Map<String, List<XEvent>> map = getEventList(log, names);
		for (int i = 0; i < names.size(); i++) {
			List<XEvent> list = (List<XEvent>) map.get(names.get(i));
			Map<String, XEvent> attrMap = new HashMap<String, XEvent>();
			for (XEvent event : list) {
				String attrValue = null;
				if (attrType == 1) {
					XAttributeDiscreteImpl attr = (XAttributeDiscreteImpl) event.getAttributes().get(attributeName);
					if (attr != null)
						attrValue = attr.getValue() + "";
				} else if (attrType == 2) {
					XAttributeLiteralImpl attr = (XAttributeLiteralImpl) (event.getAttributes().get(attributeName));
					if (attr != null)
						attrValue = attr.getValue();
				} else if (attrType == 3) {
					XAttributeContinuousImpl attr = (XAttributeContinuousImpl) (event.getAttributes()
							.get(attributeName));
				}
				if (attrValue != null && !attrMap.containsKey(attrValue)) {
					attrMap.put(attrValue, event);
				}
				// System.out.println("attr size:"+attrValue);
			}
			Set keys = attrMap.keySet();
			returnMap.put(names.get(i), keys.toString());
			// System.out.println(names.get(i)+" " +keys.toString());
		}

		return returnMap;
	}

	@Override
	public Set<String> getEventAttrs(XLog log) {
		if (log.size() > 0) {
			if (log.get(0).size() > 0) {
				XEvent event = log.get(0).get(0);
				// Set<String> attrs=event.getAttributes().keySet();
				Set<String> attrs = new HashSet<String>(event.getAttributes().keySet());
				attrs.remove("time:timestamp");
				attrs.remove("concept:name");
				attrs.remove("lifecycle:transition");
				return attrs;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public static void main(String args[]) throws Exception {
		InputStream in = new FileInputStream(new File("E:\\mineTest\\event.xes"));
		EventlogParseImpl eventLogSumariseImpl = new EventlogParseImpl();
		XLog log = eventLogSumariseImpl.eventLogParse(in);
		ActivityImpl impl = new ActivityImpl();
		System.out.println(impl.getEventAttrs(log).toString());
	}

}
