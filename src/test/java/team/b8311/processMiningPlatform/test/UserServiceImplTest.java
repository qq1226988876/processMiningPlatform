package team.b8311.processMiningPlatform.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import team.b8311.processMiningPlatform.domain.User;
import team.b8311.processMiningPlatform.service.impl.UserServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class) // spring的单元测试
@ContextConfiguration({ "classpath*:spring-*.xml" }) // 上下文配置
public class UserServiceImplTest {

	@Resource
	private UserServiceImpl userService;

	@Test
	public void testGet() {
		try {
			User user = userService.get(1);
			System.out.println(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 新建用户测试
	 */
	@Test
	public void testInsert() {
		User user = new User();
		user.setEmail("yudayu@b8311.com");
		user.setPassword("b8311");
		user.setNickname("yudayu");
		try {
			if (userService.insert(user)) {
				System.out.println("插入成功");
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

}
