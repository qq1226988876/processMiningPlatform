package team.b8311.processMiningPlatform.domain;

import java.io.Serializable;

/**
 * 创建数据库用户表：<br>
 * CREATE TABLE `user` (<br>
 * `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',<br>
 * `email` varchar(50) NOT NULL COMMENT '邮箱号',<br>
 * `password` varchar(32) NOT NULL COMMENT '用户密码',<br>
 * `banned` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户禁用标志位',<br>
 * `activated` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户启动标志位',<br>
 * `nickname` varchar(20) NOT NULL COMMENT '昵称',<br>
 * `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户类型（普通用户，管理员用户）',<br>
 * PRIMARY KEY (`id`)<br>
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8<br>
 * 
 * @author qiuto
 *
 */
public class User implements Serializable {
	private Integer id; // 用户id
	private String email; // 邮箱
	private String password; // 用户密码
	private Boolean banned; // 用户禁用标志位
	private Boolean activated; // 用户启动标志位
	private String nickname; // 昵称
	private Boolean type; // 用户类型
	// private List<Project> projects; // 用户项目

	public User() {
		// 无参构造方法
		this.banned = false;
		this.activated = false;
		this.type = false;
	}

	// public List<Project> getProjects() {
	// return projects;
	// }
	//
	// public void setProjects(List<Project> projects) {
	// this.projects = projects;
	// }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getBanned() {
		return banned;
	}

	public void setBanned(Boolean banned) {
		this.banned = banned;
	}

	public Boolean getActivated() {
		return activated;
	}

	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Boolean getType() {
		return type;
	}

	public void setType(Boolean type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", password=" + password + ", banned=" + banned + ", activated="
				+ activated + ", nickname=" + nickname + ", type=" + type + "]";
	}

}
