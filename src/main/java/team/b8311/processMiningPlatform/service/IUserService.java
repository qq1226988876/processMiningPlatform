package team.b8311.processMiningPlatform.service;

import java.util.List;

import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.User;

public interface IUserService {
	/**
	 * 根据用户编号查找用户的完整信息，调用IUserDAO.findById()
	 * 
	 * @param id
	 *            要查找的用户id
	 * @return 如果找到了用户信息以vo类对象返回，否则返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public User get(int id) throws Exception;
	public boolean delete(int id) throws Exception;
	/**
	 * 根据用户邮箱查找用户的完整信息，调用IUserDAO.findByEmail()
	 * 
	 * @param email
	 *            要查找的用户邮箱
	 * @return 如果找到了用户信息以vo类对象返回，否则返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public User get(String email) throws Exception;

	/**
	 * 实行用户的增加操作，本次操作要调用IUserDAO的以下操作<br>
	 * 需要调用IUserDAO.findByEmail()方法，来确定该用户邮箱是否存在<br>
	 * 如果要增加的用户邮箱不存在，那么调用IUserDAO.doCreate()方法，返回操作结果
	 * 
	 * @param user
	 *            包含了要增加的数据对象
	 * @return 如果增加用户邮箱重复或者添加数据失败，返回false，否则返回true
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean insert(User user) throws Exception;
	
	/**
	 * 查找基本用户
	 * @throws Exception 
	 */
	public List<User> getBasicUser() throws Exception;
}
