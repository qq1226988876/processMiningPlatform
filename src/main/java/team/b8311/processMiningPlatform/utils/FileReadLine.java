package team.b8311.processMiningPlatform.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileReadLine {
	public static InputStream inputstream;
	public BufferedReader reader;
	public String line;

	public FileReadLine(InputStream input) throws IOException {
		System.out.println("FileReaderLine");
		this.inputstream = input;
		this.reader = new BufferedReader(new InputStreamReader(inputstream, "utf-8"));

	}

	public boolean hasNext() throws IOException {
		if (inputstream != null) {
			if ((this.line = reader.readLine()) != null) {
				System.out.println("line " + line);
				return true;
			} else {
				this.reader.close();
				return false;
			}
		}
		return false;
	}

	public String getLine() throws IOException {
		// System.out.println(this.line);
		return this.line;
	}
}
