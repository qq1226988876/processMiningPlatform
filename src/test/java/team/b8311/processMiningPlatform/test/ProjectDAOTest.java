package team.b8311.processMiningPlatform.test;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import team.b8311.processMiningPlatform.dao.IProjectDAO;
import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.User;

@RunWith(SpringJUnit4ClassRunner.class) // Spring单元测试
@ContextConfiguration({ "classpath*:spring-*.xml" }) // 上下文配置
public class ProjectDAOTest {

	@Resource
	private IProjectDAO projectDAO; // 初始化DAO层

	@Test
	public void testDoCreate() {
		Project p = new Project();
		User u = new User();
		u.setId(2);
		p.setProjectName("project");
		p.setProjectDescription("this is a project");
		p.setUser(u);
		try {
			if (projectDAO.doCreate(p)) {
				System.out.println("创建项目成功");
			} else {
				System.out.println("创建项目失败");
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	@Test
	public void testDoUpdate() {
		Project p = new Project();
		User u = new User();
		u.setId(2);
		p.setProjectId(2);
		p.setProjectName("模糊挖掘");
		p.setProjectDescription("这是一个模糊挖掘项目");
		p.setUser(u);
		try {
			if (projectDAO.doUpdate(p)) {
				System.out.println("更新项目成功");
			} else {
				System.out.println("更新项目失败");
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	@Test
	public void testDoRemoveBatch() {
		Set<Integer> set = new HashSet<Integer>();
		set.add(7);
		System.out.println(set);
		try {
			if (projectDAO.doRemoveBatch(set)) {
				System.out.println("删除项目成功");
			} else {
				System.out.println("删除项目失败");
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * 测试根据项目名称进行模糊查询
	 */
	@Test
	public void testFindByName() {
		try {
			System.out.println(projectDAO.findByName(2, "模糊"));
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * 测试新增数据并取得主键
	 */
	@Test
	public void testDoCreateAndGetKey() {
		Project p = new Project();
		User u = new User();
		u.setId(2);
		p.setProjectName("project");
		p.setProjectDescription("this is a project");
		p.setUser(u);
		try {
			System.out.println(projectDAO.doCreateAndGetKey(p));
			System.out.println(p.getProjectId());
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
}
