package team.b8311.processMiningPlatform.service;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import team.b8311.processMiningPlatform.domain.Algorithm;
import team.b8311.processMiningPlatform.utils.Config;

public interface IAlgorithmService {
	public Algorithm get(int id) throws Exception;

	@Deprecated
	public boolean acceptAlgorithm(HttpServletRequest req, MultipartFile[] files, Algorithm al) throws Exception;
	// public Miningresult applyAlgorithm();

	/**
	 * 实现算法的增加操作，本次操作要调用IAlgorithmDAO接口的以下方法：<br>
	 * <li>调用IAlgorithmDAO.doCreate()方法，返回操作结果
	 * 
	 * @param algo
	 *            包含了要增加数据的VO对象
	 * @return 增加成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean insert(Algorithm algo) throws Exception;

	/**
	 * 实现算法的删除操作，可以删除多个算法信息，调用IAlgorithmDAO接口的以下方法：<br>
	 * <li>调用IAlgorithmDAO.doRemoveBatch()删除数据
	 * 
	 * @param ids
	 *            要删除的数据集合，没有重复数据
	 * @return 删除成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常或错误信息异常
	 */
	public boolean delete(Set<Integer> ids) throws Exception;
	/**
	 * 更新算法是否可用
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public boolean updateAlgorithmAvailable(Algorithm vo) throws Exception;

	/**
	 * 进行数据的模糊查询和数据统计，要调用IAlgorithmDAO接口的两个方法：<br>
	 * <li>调用IAlgorithm.findAllSplit()方法，查询出所有的表数据
	 * <li>调用IAlgorithm.getAllCount()方法，查询出所有的数据量，返回Integer
	 * 
	 * @param currentPage
	 *            当前所在页
	 * @param lineSize
	 *            每页显示的记录数
	 * @param column
	 *            模糊查询的数据列
	 * @param keyWord
	 *            模糊查询的关键字
	 * @return 本方法需要返回多种数据类型，所以使用Map集合返回，由于类型不同，所有的value的类型置为Object<br>
	 *         <li>key = allAlgorithms,value =
	 *         IAlgorithm.findAllSplit()方法返回结果，List <Algorithm>;
	 *         <li>key = algorithmCount,value =
	 *         IAlgorithm.getAllCount()方法返回结果，Integer;
	 * @throws Exception
	 *             SQL执行异常或者错误信息异常
	 */
	public Map<String, Object> list(int currentPage, int lineSize, String column, String keyWord) throws Exception;
	/**
	 * 获取所有算法信息
	 * @return 
	 * @throws Exception
	 */
	public Map<String, Object> listAll() throws Exception;
	/**
	 * 获取可用的算法
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> listAvailabel() throws Exception;
	public Config getAlgoConfig(int algoId) throws Exception;
	
}
