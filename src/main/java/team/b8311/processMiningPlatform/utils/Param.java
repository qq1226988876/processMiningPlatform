package team.b8311.processMiningPlatform.utils;

import java.io.Serializable;

public class Param implements Serializable {
	private static final long serialVersionUID = 6884737487224431391L;

	private String parameterName;//参数名称
	private Integer parameterType;// 参数类型
	private String defaultValue;//默认值
	public String getParameterName() {
		return parameterName;
	}
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	public Integer getParameterType() {
		return parameterType;
	}
	public void setParameterType(Integer parameterType) {
		this.parameterType = parameterType;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	

}
