package team.b8311.processMiningPlatform.support.normalize;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.apache.commons.io.FileUtils;

public class NormalizeTest {

	public static void main(String[] args) throws Exception {
		String user = "abc";
		Integer type = 0;

		String name = "";

		String formats = "[QC],ABCD,A-B-CTD,A-B-CTD";
		String timeNames = "[QC]";
		String dataNames = "[Method];[Status]:EventName,[INFO]:INFO";
		String oriitemSeparator = "\t";
		String orinameValSeparator = " ";
		String orinulVal = "";

		String targetitemSeparator = "";
		String targetnameValSeparator = "";
		String targetnulVal = "";

		normalizeLog(user, type, name, formats, timeNames, dataNames, oriitemSeparator, orinameValSeparator, orinulVal,
				targetitemSeparator, targetnameValSeparator, targetnulVal);
	}

	public static void normalizeLog(String user, int type, String name, String formats, String timeNames,
			String dataNames, String oriitemSeparator, String orinameValSeparator, String orinulVal,
			String targetitemSeparator, String targetnameValSeparator, String targetnulVal) throws Exception {
		File logFile = new File("F:" + File.separator + "study" + File.separator + "process mining" + File.separator
				+ "example-logs" + File.separator + "test.txt");
		IPTimeStamp its = new IPTimeStamp();
		String newName = its.getIPTimeRandName();
		File folder = new File(logFile.getParentFile().getPath() + File.separator + newName);

		folder.mkdirs();
		File to = new File(folder.getPath() + File.separator + its.getNewFileName(newName, "original"));
		FileUtils.copyFile(logFile, to);

		String readFilePath = to.getPath();
		String writeFilePath = folder.getPath() + File.separator + its.getNewFileName(newName, "standard");
		// String eventLog = folder.getPath()+"\\"+its.getNewFileName(newName,
		// "eventLog");
		// TODO 下载文件的位置固定了，需要修改
		// String folderPath =
		// "C:\\mySoftware\\apache-tomcat-7.0.69\\me-webapps\\processMining\\upload\\"+its.getNewFileName(newName,
		// "transLog","zip");
		String targetTimeName = "Time";

		LogConfiguration LC = new LogConfiguration(formats, timeNames, targetTimeName, dataNames, oriitemSeparator,
				orinameValSeparator, orinulVal);

		String nulVal = " ";
		String seperator = "\t";

		File readFile = new File(readFilePath);
		File writeFile = new File(writeFilePath);

		if (!readFile.exists()) {
			System.out.println(readFilePath + " not exists!");
			return;
		}
		if (writeFile.exists()) {
			writeFile.delete();
		}
		writeFile.createNewFile();
		BufferedReader reader = new BufferedReader(new FileReader(readFile));
		FileWriter fw = new FileWriter(writeFile);
		/*---------------------------准备输入输出流--------------------------------*/

		int n = 0;// 记录总共写入多少条数据
		String tempLineString = null;// 对从输入流读出的行进行临时存储
		while ((tempLineString = reader.readLine()) != null && !tempLineString.equals("")) {
			// 将一行录入TempLine
			TempLine tempLine = null;
			if (LC.getNameValSeparator() == null)
				tempLine = new TempLine(tempLineString, LC.getItemSeparator(), LC.getNulVal());
			else
				tempLine = new TempLine(tempLineString, LC.getNameValSeparator(), LC.getItemSeparator(),
						LC.getNulVal());

			// 第一步：对所有数据项格式化
			FormatInfo[] tempFormatInfos = LC.getFormatInfos();
			for (int i = 0; i < tempFormatInfos.length; i++) {
				FormatInfo tempFormatInfo = tempFormatInfos[i];
				String temp = tempLine.getItemMap().get(tempFormatInfo.getItemNameOrIndex());
				temp = tempFormatInfos[i].formatTransform(temp,
						tempLine.getItemMap().get(LC.getFormatTypeNameOrIndex()));
				tempLine.modifyValue(tempFormatInfo.getItemNameOrIndex(), temp);
			}

			// 第二步：合并时间项
			tempLine.mergeTimeItems(LC.getTargetTimeName(), LC.getTimeItems());

			// 第三步：整合其他项
			tempLine.renameORmerge(LC.getRenameORmergeItems());

			// 第四步：写入新日志
			if (n == 0)
				fw.write(tempLine.generateItemNamesLine(seperator) + "\r\n");// 写入文件首行
			fw.write(tempLine.generateNewLine(seperator, nulVal) + "\r\n");

			n++;// 记录总共写入多少条数据
		}
		System.out.println("共写入" + n + "条数据");

		reader.close();
		fw.close();
		/*
		 * //将结果复制一份到upload目录下 File A = new File(folder.getPath()+
		 * "\\"+its.getNewFileName(newName, "eventLog")); //TODO 下载文件的位置固定了，需要修改
		 * //File B = new File(
		 * "C:\\mySoftware\\apache-tomcat-7.0.69\\me-webapps\\processMining\\upload\\"+its.getNewFileName(newName, "
		 * eventLog")); File B = new File(
		 * "D:\\upload\\"+its.getNewFileName(newName, "eventLog"));
		 * FileUtils.copyFile(A, B);
		 */

		System.out.println("ok");
		return;

	}
}
