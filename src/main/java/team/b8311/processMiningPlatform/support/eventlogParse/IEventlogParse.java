package team.b8311.processMiningPlatform.support.eventlogParse;

import java.io.InputStream;

import org.deckfour.xes.model.XLog;

/**
 * 事件日志流解析出XLog对象
 */
public interface IEventlogParse {
	public XLog eventLogParse(InputStream in) throws Exception;
}
