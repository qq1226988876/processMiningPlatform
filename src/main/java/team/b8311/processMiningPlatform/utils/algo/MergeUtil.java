package team.b8311.processMiningPlatform.utils.algo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.UUID;

import javax.annotation.Resource;

import org.deckfour.xes.model.XLog;
import org.springframework.stereotype.Service;

import team.b8311.processMiningPlatform.dao.IEventlogDAO;
import team.b8311.processMiningPlatform.domain.Algorithm;
import team.b8311.processMiningPlatform.domain.Eventlog;
import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.service.IEventlogService;
import team.b8311.processMiningPlatform.support.eventlogExport.IEventlogExport;
import team.b8311.processMiningPlatform.support.eventlogExport.impl.EventlogExportImpl;
import team.b8311.processMiningPlatform.support.eventlogParse.IEventlogParse;
import team.b8311.processMiningPlatform.support.eventlogParse.impl.EventlogParseImpl;
import team.b8311.processMiningPlatform.utils.HDFSUtils;

@Service
public class MergeUtil {
	@Resource
	private IEventlogDAO eventlogDAO; // 注入DAO层对象
	@Resource
	private IEventlogService eventlogService; // 自动载入Service
	@Resource
	private HDFSUtils hdfs; // 自动载入操作HDFS对象

	public Eventlog merge(Eventlog el1, Eventlog el2, Algorithm algo, Integer pid) throws Exception {
		String path = algo.getAlgorithmPath();

		Integer el1Id = el1.getEventlogId();
		InputStream input = hdfs.downloadFile(HDFSUtils.EVENTLOG_PATH_PREFIX + el1.getEventlogHDFSId());
		// InputStream input= new FileInputStream(new File("D:\\1"));
		IEventlogParse parser = new EventlogParseImpl();
		XLog xlog1 = parser.eventLogParse(input);
		input.close();

		Integer el2Id = el2.getEventlogId();
		InputStream input2 = hdfs.downloadFile(HDFSUtils.EVENTLOG_PATH_PREFIX + el2.getEventlogHDFSId());
		// InputStream input2=new FileInputStream(new File("D:\\2"));
		IEventlogParse parser2 = new EventlogParseImpl();
		XLog xlog2 = parser2.eventLogParse(input2);
		input2.close();

		Class c = XLog.class;

		File file = new File(path);// jar包的路径
		System.out.println(file.exists());
		// File file=new File("D:\\AIA.jar");
		URL url = file.toURI().toURL();
		ClassLoader loader = new URLClassLoader(new URL[] { url }, this.getClass().getClassLoader());// 创建类载入器
		Class<?> cls = loader.loadClass(algo.getAlgorithmPackage() + "." + algo.getAlgorithmClassName());
		System.out.print(algo.getAlgorithmPackage() + "." + algo.getAlgorithmClassName());// 载入指定类，注意一定要带上类的包名
		Method method = cls.getMethod(algo.getAlgorithmMethod(), c, c);// 方法名和相应的各个參数的类型
		Object o = cls.newInstance();
		XLog result = (XLog) method.invoke(o, xlog1, xlog2);// 调用得到的上边的方法method(静态方法，第一个參数能够为null)

		Eventlog resultEventLog = new Eventlog();
		resultEventLog.setEventlogFormat("xes");
		resultEventLog.setEventlogName(UUID.randomUUID().toString().substring(0, 10) + ".xes");
		Project p = new Project();
		p.setProjectId(pid);
		resultEventLog.setProject(p);

		SummarizeTool st = SummarizeXLog.summarize(result);
		resultEventLog.setEventlogAverageEvent(st.getAverage());
		resultEventLog.setEventlogProcessActivityEvent(st.getProcessActivityEvent());
		resultEventLog.setEventlogTotalEvent(st.getTotalEventNum());
		resultEventLog.setEventlogTotalInstance(st.getTotalInstanceNum());
		resultEventLog.setEventlogController(st.getController());

		File tempFile = null;
		try {
			tempFile = File.createTempFile("tmp", "log");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		IEventlogExport ele = new EventlogExportImpl();
		ele.convertXLogToXesFile(result, tempFile);

		FileInputStream fis = new FileInputStream(tempFile);
		eventlogService.upload(fis, resultEventLog);

		// 设置融合日志关系
		eventlogDAO.doCreateMerge(algo.getAlgorithmId(), resultEventLog.getEventlogId(), el1Id, el2Id);
		// MergeEventLog mel = new MergeEventLog();
		// if (eventlogService.selectByPK(evtLog1Id) != null) {
		// mel.setSource1name(evtLog1.getName());
		// mel.setSourceeventlog1id(evtLog1.getId());
		// }
		// if (eventlogService.selectByPK(evtLog2Id) != null) {
		// mel.setSource2name(evtLog2.getName());
		// mel.setSourceeventlog2id(evtLog2.getId());
		// }
		// mel.setTargeteventlogid(resultEventLog.getId());
		// mel.setTargetname(resultEventLog.getName());
		//
		// mergeEventLogService.insert(mel);

		return resultEventLog;
	}

	public static void main(String[] args)
			throws Exception {
		// String path=algo.getPath();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("maxNumGen", 10000);
		map.put("numInitSol", 100);
		map.put("maxNumNoOpt", 50);
		map.put("percBestSolForClone", 0.1);
		map.put("scoreSameAttValue", 5);
		map.put("scoreTimeDiff", 5);
		/*
		 * Integer evtLog1Id = evtLog1.getId(); InputStream input =
		 * eventLogService.downloadLog(evtLog1Id);
		 */
		InputStream input = new FileInputStream(new File("F:" + File.separator + "study" + File.separator
				+ "process mining" + File.separator + "example-logs" + File.separator + "exercise1.xes"));
		IEventlogParse parser = new EventlogParseImpl();
		XLog xlog1 = parser.eventLogParse(input);
		input.close();

		/*
		 * Integer evtLog2Id = evtLog2.getId(); InputStream input2 =
		 * eventLogService.downloadLog(evtLog1Id);
		 */
		InputStream input2 = new FileInputStream(new File("F:" + File.separator + "study" + File.separator
				+ "process mining" + File.separator + "example-logs" + File.separator + "exercise2.xes"));
		IEventlogParse parser2 = new EventlogParseImpl();
		XLog xlog2 = parser2.eventLogParse(input2);
		input2.close();

		// File file = new File(path);//jar包的路径
		File file = new File("F:" + File.separator + "study" + File.separator + "process mining" + File.separator
				+ "jars" + File.separator + "AIA.jar");
		System.out.println(file.exists());
		URL url = file.toURI().toURL();
		ClassLoader loader = new URLClassLoader(new URL[] { url });// 创建类载入器
		Class<?> cls = loader.loadClass("net.ProcessMining.logMerge.aia.AIAMergerPlugin");// 载入指定类，注意一定要带上类的包名
		Method method = cls.getMethod("merge", XLog.class, XLog.class);// 方法名和相应的各个參数的类型
		Object o = cls.newInstance();
		System.out.println("xxx");
		XLog result = (XLog) method.invoke(o, xlog1, xlog2);// 调用得到的上边的方法method(静态方法，第一个參数能够为null)
		System.out.println("xxx");
		Eventlog resultEventLog = new Eventlog();
		// resultEventLog.setCreatetime(new Date());
		resultEventLog.setEventlogFormat("xes");
		resultEventLog.setEventlogName(UUID.randomUUID().toString().substring(0, 10) + ".xes");

		SummarizeTool st = SummarizeXLog.summarize(result);
		resultEventLog.setEventlogAverageEvent(st.getAverage());
		resultEventLog.setEventlogProcessActivityEvent(st.getProcessActivityEvent());
		resultEventLog.setEventlogTotalEvent(st.getTotalEventNum());
		resultEventLog.setEventlogTotalInstance(st.getTotalInstanceNum());
		resultEventLog.setEventlogController(st.getController());
		System.out.println("平均每个实例中事件数:" + st.getAverage());
		System.out.println("流程活动事件:" + st.getProcessActivityEvent());
		System.out.println("总事件数:" + st.getTotalEventNum());
		System.out.println("总实例数:" + st.getTotalInstanceNum());
		System.out.println("流程活动操作人:" + st.getController());

		// File tempFile = null;
		// try {
		// tempFile = File.createTempFile("tmp", "log");
		// } catch (IOException e1) {
		// e1.printStackTrace();
		// }
		// EventLogExport ele=new EventLogExportImpl();
		// ele.convertXLogToXesFile(result, tempFile);
		//
		// FileInputStream fis=new FileInputStream(tempFile);
		// eventLogService.uploadLog(fis, resultEventLog);
		//
		// MergeEventLog mel=new MergeEventLog();
		// mel.setSource1name(evtLog1.getName());
		// mel.setSource2name(evtLog2.getName());
		// mel.setSourceeventlog1id(evtLog1.getId());
		// mel.setSourceeventlog2id(evtLog2.getId());
		// mel.setTargeteventlogid(resultEventLog.getId());
		// mel.setTargetname(resultEventLog.getName());
		//
		// mergeEventLogService.insert(mel);
		// //eventLogService.uploadLog(inputStream, log)
		// return null;
	}
}
