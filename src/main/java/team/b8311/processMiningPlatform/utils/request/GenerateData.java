package team.b8311.processMiningPlatform.utils.request;

import team.b8311.processMiningPlatform.support.generateEventlog.entity.PetriNet;

public class GenerateData {
	private PetriNet petriNet; // 传入的petri网
	private String eventlogName; // 事件日志名称
	private Integer projectId; // 事件日志所属的项目id
	private Integer traceNumber; // 需要生成的trace个数
	private Double noiseRatio; // 生成日志中的噪声比率

	public PetriNet getPetriNet() {
		return petriNet;
	}

	public void setPetriNet(PetriNet petriNet) {
		this.petriNet = petriNet;
	}

	public Integer getTraceNumber() {
		return traceNumber;
	}

	public void setTraceNumber(Integer traceNumber) {
		this.traceNumber = traceNumber;
	}

	public Double getNoiseRatio() {
		return noiseRatio;
	}

	public void setNoiseRatio(Double noiseRatio) {
		this.noiseRatio = noiseRatio;
	}

	public String getEventlogName() {
		return eventlogName;
	}

	public void setEventlogName(String eventlogName) {
		this.eventlogName = eventlogName;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	@Override
	public String toString() {
		return "GenerateData [petriNet=" + petriNet + ", eventlogName=" + eventlogName + ", projectId=" + projectId
				+ ", traceNumber=" + traceNumber + ", noiseRatio=" + noiseRatio + "]";
	}

}
