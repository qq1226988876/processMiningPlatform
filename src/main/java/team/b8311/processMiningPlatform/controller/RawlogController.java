package team.b8311.processMiningPlatform.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import team.b8311.processMiningPlatform.domain.Normalizedlog;
import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.Rawlog;
import team.b8311.processMiningPlatform.domain.ResponseObj;
import team.b8311.processMiningPlatform.exception.BaseExceptionHandleAction;
import team.b8311.processMiningPlatform.exception.CustomException;
import team.b8311.processMiningPlatform.service.IRawlogService;
import team.b8311.processMiningPlatform.utils.HDFSUtils;

/**
 * 原始日志模块控制器
 * 
 * @author qiuto
 *
 */
@Controller
@RequestMapping("/rawlogAction") // 外层地址
public class RawlogController extends BaseExceptionHandleAction {

	@Resource
	private IRawlogService rawlogService; // 自动载入service对象

	@Resource
	private HDFSUtils hdfs; // 自动载入操作HDFS对象

	private ResponseObj responseObj; // Api接口返回对象

	/**
	 * 上传原始日志接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param file
	 *            上传的原始日志文件
	 * @param format
	 *            上传的原始日志格式
	 * @param projectId
	 *            原始日志所属的项目id
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/upload" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object upload(HttpServletRequest req, HttpServletResponse res, MultipartFile file, String format,
			int projectId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码

		responseObj = new ResponseObj<Rawlog>();
		if (file.getSize() == 0) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("原始日志大小为0！");
			return responseObj;
		}

		Rawlog rl = new Rawlog();
		rl.setRawlogFormat(format);
		rl.setRawlogName(file.getOriginalFilename());
		Project p = new Project();
		p.setProjectId(projectId);
		rl.setProject(p);

		if (rawlogService.upload(file.getInputStream(), rl)) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("上传原始日志成功");
			responseObj.setData(rawlogService.get(rl.getRawlogId()));
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("上传原始日志失败！");
			return responseObj;
		}

	}

	/**
	 * 下载原始日志文件
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param rawlogId
	 *            要下载的原始日志id
	 */
	@RequestMapping(value = "/download" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	public void download(HttpServletRequest req, HttpServletResponse res, int rawlogId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		Rawlog rl = null;
		if ((rl = rawlogService.get(rawlogId)) == null) {
			throw new CustomException("原始日志不存在");
		} else {
			InputStream in = new BufferedInputStream(
					hdfs.downloadFile(HDFSUtils.RAWLOG_PATH_PREFIX + rl.getRawlogHDFSId()));

			res.addHeader("Content-Disposition",
					"attachment;filename=" + new String(rl.getRawlogName().getBytes("utf-8"), "iso-8859-1")); // 文件下载时获取文件名时的编码格式为iso-8859-1
			res.setContentType("multipart/form-data");

			OutputStream out = new BufferedOutputStream(res.getOutputStream());
			int temp = 0; // 表示每次读取的字节数据
			while ((temp = in.read()) != -1) {
				out.write(temp);
				out.flush();
			}
			out.close();
		}
	}

	/**
	 * 获取原始日志信息接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param currentPage
	 *            当前所在页
	 * @param lineSize
	 *            每页显示的记录数
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projectId
	 *            项目id
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/list" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object list(HttpServletRequest req, HttpServletResponse res, int currentPage, int lineSize, String keyWord,
			int projectId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<Map>();

		Map<String, Object> map = rawlogService.listDetails(currentPage, lineSize, "rawlog_name", keyWord, projectId);
		if (null != map) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("获取原始日志信息成功");
			responseObj.setData(map);
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("获取原始日志信息失败！");
			return responseObj;
		}
	}

	/**
	 * 删除原始日志接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param ids
	 *            要删除的原始日志id数组
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete" // 内层地址
			, method = RequestMethod.GET// 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object delete(HttpServletRequest req, HttpServletResponse res, @RequestParam("ids") Set<Integer> ids)
			throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常

		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码

		System.out.println(ids);

		responseObj = new ResponseObj<Object>();
		if (ids.size() == 0) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("请选择要删除的日志");
			return responseObj;
		}

		if (rawlogService.delete(ids)) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("删除日志成功！");
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("删除日志失败！");
			return responseObj;
		}
	}

	/**
	 * 规范化原始日志接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param id
	 *            原始日志id
	 * @param format
	 *            原始日志格式
	 * @param timeNames
	 *            时间项整合
	 * @param targetTimeName
	 * @param dataNames
	 *            数据项整合
	 * @param oriitemSeparator
	 *            原数据项分隔符
	 * @param orinameValSeparator
	 *            原名称值分隔符
	 * @param orinulVal
	 *            原空值
	 * @param targetitemSeparator
	 *            目标数据项分隔符
	 * @param targetnameValSeparator
	 *            目标名称值分隔符
	 * @param targetnulVal
	 *            目标空值
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/normalize" // 内层地址
			, method = RequestMethod.POST// 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object normalized(HttpServletRequest req, HttpServletResponse res, int id, String format, String timeNames,
			String targetTimeName, String dataNames, String oriitemSeparator, String orinameValSeparator,
			String orinulVal, String targetitemSeparator, String targetnameValSeparator, String targetnulVal)
			throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码

		responseObj = new ResponseObj<Object>();
		Normalizedlog nl = new Normalizedlog();
		if (rawlogService.convertToNormalizelog(id, format, timeNames, targetTimeName, dataNames, oriitemSeparator,
				orinameValSeparator, orinulVal, targetitemSeparator, targetnameValSeparator, targetnulVal, nl)) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("规范化原始日志成功！");
			responseObj.setData(nl);
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("规范化原始日志失败！");
		}
		return responseObj;
	}

}
