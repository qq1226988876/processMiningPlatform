package team.b8311.processMiningPlatform.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 创建原始日志表：<br>
 * CREATE TABLE `rawlog` (<br>
 * `rawlog_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '原始日志id',<br>
 * `rawlog_name` varchar(50) NOT NULL COMMENT '原始日志名称',<br>
 * `rawlog_format` varchar(10) NOT NULL COMMENT '原始日志文件格式',<br>
 * `rawlog_createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE
 * CURRENT_TIMESTAMP COMMENT '原始日志创建时间',<br>
 * `rawlog_hdfs_id` varchar(50) NOT NULL COMMENT 'HDFS文件id',<br>
 * `normalizedlog_id` int(10) unsigned DEFAULT NULL COMMENT '规范化日志id',<br>
 * `project_id` int(10) unsigned DEFAULT NULL COMMENT '项目id',<br>
 * PRIMARY KEY (`rawlog_id`),<br>
 * UNIQUE KEY `rawlog_name` (`rawlog_name`),<br>
 * KEY `IDX_RAWLOG_NORMALIZEDLOG_ID` (`normalizedlog_id`),<br>
 * KEY `IDX_RAWLOG_PROJECT_ID` (`project_id`)<br>
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 * 
 * @author qiuto
 *
 */
public class Rawlog implements Serializable {
	private Integer rawlogId; // 原始日志id
	private String rawlogName; // 原始日志名称
	private String rawlogFormat; // 原始日志文件格式
	private Date rawlogCreatetime; // 原始日志创建时间
	private String rawlogHDFSId; // HDFS文件id

	private Project project; // 所属项目
	private Normalizedlog normalizedlog; // 对应的规范化日志

	public Rawlog() {
	}

	public Integer getRawlogId() {
		return rawlogId;
	}

	public void setRawlogId(Integer rawlogId) {
		this.rawlogId = rawlogId;
	}

	public String getRawlogName() {
		return rawlogName;
	}

	public void setRawlogName(String rawlogName) {
		this.rawlogName = rawlogName;
	}

	public String getRawlogFormat() {
		return rawlogFormat;
	}

	public void setRawlogFormat(String rawlogFormat) {
		this.rawlogFormat = rawlogFormat;
	}

	public Date getRawlogCreatetime() {
		return rawlogCreatetime;
	}

	public void setRawlogCreatetime(Date rawlogCreatetime) {
		this.rawlogCreatetime = rawlogCreatetime;
	}

	public String getRawlogHDFSId() {
		return rawlogHDFSId;
	}

	public void setRawlogHDFSId(String rawlogHDFSId) {
		this.rawlogHDFSId = rawlogHDFSId;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Normalizedlog getNormalizedlog() {
		return normalizedlog;
	}

	public void setNormalizedlog(Normalizedlog normalizedlog) {
		this.normalizedlog = normalizedlog;
	}

	@Override
	public String toString() {
		return "Rawlog [rawlogId=" + rawlogId + ", rawlogName=" + rawlogName + ", rawlogFormat=" + rawlogFormat
				+ ", rawlogCreatetime=" + rawlogCreatetime + ", rawlogHDFSId=" + rawlogHDFSId + ", project=" + project
				+ ", normalizedlog=" + normalizedlog + "]";
	}

}
