package team.b8311.processMiningPlatform.test;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import team.b8311.processMiningPlatform.dao.INormalizedlogDAO;
import team.b8311.processMiningPlatform.domain.Eventlog;
import team.b8311.processMiningPlatform.domain.Normalizedlog;
import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.Rawlog;

@RunWith(SpringJUnit4ClassRunner.class) // Spring单元测试
@ContextConfiguration({ "classpath*:spring-*.xml" }) // 上下文配置
public class NormalizedDAOTest {

	@Resource
	private INormalizedlogDAO normalizedlogDAO; // 初始化DAO层

	/**
	 * 测试增加规范化项目
	 */
	@Test
	public void testdoCreate() {
		Normalizedlog nl = new Normalizedlog();
		nl.setNormalizedlogName("规范化项目");
		nl.setNormalizedlogFormat("txt");
		nl.setNormalizedlogHDFSId("asd");
		Project p = new Project();
		p.setProjectId(1);
		Rawlog rl = new Rawlog();
		rl.setRawlogId(12);
		Eventlog el = new Eventlog();
		el.setEventlogId(1);
		nl.setProject(p);
		nl.setRawlog(rl);
		nl.setEventlog(el);
		try {
			if (normalizedlogDAO.doCreate(nl)) {
				System.out.println("增加成功，规范化项目id:" + nl.getNormalizedlogId());
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * 测试更新规范化日志数据
	 */
	@Test
	public void testDOUpdate() {
		Normalizedlog nl = new Normalizedlog();
		nl.setNormalizedlogId(1);
		nl.setNormalizedlogName("规范化项目2");
		nl.setNormalizedlogFormat("txt");
		nl.setNormalizedlogHDFSId("asd2");
		Project p = new Project();
		p.setProjectId(1);
		Rawlog rl = new Rawlog();
		rl.setRawlogId(1);
		Eventlog el = new Eventlog();
		el.setEventlogId(1);
		nl.setProject(p);
		nl.setRawlog(rl);
		nl.setEventlog(el);
		try {
			if (normalizedlogDAO.doUpdate(nl)) {
				System.out.println("更新成功");
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * 测试删除规范化日志
	 */
	@Test
	public void testDoRemoveBatch() {
		Set<Integer> ids = new HashSet<Integer>();
		ids.add(2);
		try {
			if (normalizedlogDAO.doRemoveBatch(ids)) {
				System.out.println("删除成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 测试根据id查询规范化日志相关信息
	 */
	@Test
	public void testFindById() {
		try {
			System.out.println(normalizedlogDAO.findById(1));
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

}
