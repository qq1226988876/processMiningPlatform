package team.b8311.processMiningPlatform.service.impl;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

import org.deckfour.xes.model.XLog;
import org.springframework.stereotype.Service;

import net.sf.json.JSONObject;
import team.b8311.processMiningPlatform.controller.MiningController;
import team.b8311.processMiningPlatform.domain.Algorithm;
import team.b8311.processMiningPlatform.service.IMiningService;
import team.b8311.processMiningPlatform.support.C_netFormatter.format.Formatter;
import team.b8311.processMiningPlatform.support.C_netFormatter.format.PetriNet;
import team.b8311.processMiningPlatform.support.C_netFormatter.format.Sankey;
import team.b8311.processMiningPlatform.support.C_netFormatter.format.TransitionSystem;
import team.b8311.processMiningPlatform.utils.MiningResult;
import team.b8311.processMiningPlatform.utils.request.ImageType;
import team.b8311.processMiningPlatform.utils.request.MiningData;

@Service("MiningService")
public class MiningServiceImpl implements IMiningService {

	@Override
	public MiningResult logMine(XLog eventLog, Algorithm algo, Map<String, Object> map,
			Integer imageType) throws Exception {
		String path = algo.getAlgorithmPath();
		// System.out.println(path);
		File file = new File(path); // jar包的路径
		URL url = file.toURI().toURL();

		URLClassLoader loader = new URLClassLoader(new URL[] { url }, MiningController.class.getClassLoader());
		// 反射调用jar包里的方法
		Constructor c2 = loader.loadClass(algo.getAlgorithmPackage() + "." + algo.getAlgorithmClassName())
				.getConstructor();

		Object minerObj = c2.newInstance();
		long startTime=System.currentTimeMillis();   //获取开始时间
		Method method = minerObj.getClass().getMethod(algo.getAlgorithmMethod(), XLog.class, HashMap.class);// util.getMethod(hmObj.getClass(),
		long endTime=System.currentTimeMillis(); //获取结束时间
		String runTime = (endTime-startTime)+"ms";
		//目前图形的表示使用json格式数据
		JSONObject jsonData = (JSONObject)method.invoke(minerObj, eventLog, map);
		MiningResult result = new MiningResult();
		result.setUsedTime(runTime);
		result.setJsonObject(jsonData);
		
//		Formatter net = null; // 调用mine()挖掘
//		switch (imageType) {
//		case ImageType.PetriNet:
//			// formatter = new PetriNet(ret.getNet(), usedTime);
//			net = (PetriNet) method2.invoke(minerObj, eventLog, map);
//			break;
//
//		case ImageType.TransitionSystem:
//			// formatter = new TransitionSystem(ret.getNet(), usedTime, traces,
//			// allTraces);
//			net = (TransitionSystem) method2.invoke(minerObj, eventLog, map);
//			break;
//
//		case ImageType.Sankey:
//			// formatter = new Sankey(ret.getNet(), usedTime);
//			net = (Sankey) method2.invoke(minerObj, eventLog, map);
//			break;
//		}

		// 获取其他结果
//		Method m = minerObj.getClass().getMethod("getUsedTime");
//		String time = (String) m.invoke(minerObj);
//		System.out.println(time);

		// Method tracesM = minerObj.getClass().getMethod("getTraces");
		// List<String> traces = (List<String>) tracesM.invoke(minerObj);

		// Method allTracesM = minerObj.getClass().getMethod("getAllTraces");
		// List<String> allTraces = (List<String>) allTracesM.invoke(minerObj);

		// System.out.println(net.getClass().toString());
		
		// result.setAllTraces(allTraces);
		// result.setTraces(traces);
		
		//result.setMiningData(miningData);
		//result.setNet(net);

		return result;
	}

}
