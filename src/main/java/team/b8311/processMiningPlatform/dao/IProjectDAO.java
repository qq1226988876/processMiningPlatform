package team.b8311.processMiningPlatform.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import team.b8311.processMiningPlatform.domain.Project;

public interface IProjectDAO extends IDAO<Integer, Project> {
	/**
	 * 通过用户id，项目名称模糊查询用户相关的项目，查询结果以集合的形式返回
	 * 
	 * @param id
	 *            用户id
	 * @param keyWord
	 *            模糊查询的关键字
	 * @return 如果表中有数据，则所有的数据封装为VO对象利用List的集合返回<br>
	 *         如果没有数据，那么集合的长度为0（size == 0，而不是null）
	 * @throws Exception
	 *             SQL执行异常
	 */
	public List<Project> findByName(@Param("id") Integer id, @Param("keyWord") String keyWord) throws Exception;

	/**
	 * 通过用户id和项目名称查询项目
	 * 
	 * @param userId
	 *            用户id
	 * @param name
	 *            项目名称
	 * @return 如果有结果返回，则返回的数据封装为VO对象返回<br>
	 *         如果没有数据返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Project findByNameUserId(@Param("name") String name, @Param("userId") Integer userId) throws Exception;

	/**
	 * 实现数据的增加操作并返回key值
	 * 
	 * @param vo
	 *            包含了要增加数据的VO对象
	 * @return 数据保存成功返回1，否则返回0
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Integer doCreateAndGetKey(Project vo) throws Exception;
}
