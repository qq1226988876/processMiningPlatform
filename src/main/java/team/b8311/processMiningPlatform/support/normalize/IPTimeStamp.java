package team.b8311.processMiningPlatform.support.normalize;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class IPTimeStamp {
	public SimpleDateFormat sdf = null;
	public String ip = null;
	public String filename = null;

	public IPTimeStamp() {
	}

	public IPTimeStamp(String ip, String filename) {
		this.ip = ip;
		this.filename = filename;
	}

	public String getNewFileName(String newName, String state, String fileExt) {
		StringBuffer buf = new StringBuffer();
		buf.append(newName + "-" + state + "." + fileExt);
		return buf.toString();
	}

	public String getNewFileName(String newName, String state) {
		StringBuffer buf = new StringBuffer();
		buf.append(newName + "-" + state + ".txt");
		return buf.toString();
	}

	public String getIPTimeRandName() {
		StringBuffer buf = new StringBuffer();
		if (this.ip != null) {
			String str[] = this.ip.split("\\.");
			for (int i = 0; i < str.length; i++) {
				buf.append(this.addZero(str[i], 3));
			}
		}
		buf.append(this.getTimeStamp());
		Random random = new Random();
		for (int i = 0; i < 3; i++) {
			buf.append(random.nextInt(10));
		}
		// buf.append("."+this.getFileExt());
		return buf.toString();

	}

	private String addZero(String str, int len) {
		StringBuffer s = new StringBuffer();
		s.append(str);
		while (s.length() < len) {
			s.insert(0, "0");
		}
		return s.toString();
	}

	private String getTimeStamp() {
		this.sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return this.sdf.format(new Date());
	}

	private String getFileExt() {
		int i = this.filename.lastIndexOf(".");
		String extension = this.filename.substring(i + 1);
		return extension;
	}
}
