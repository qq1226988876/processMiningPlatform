package team.b8311.processMiningPlatform.support.generateEventlog.operation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XLogImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.deckfour.xes.out.XesXmlSerializer;

import team.b8311.processMiningPlatform.support.generateEventlog.entity.FlowRelation;
import team.b8311.processMiningPlatform.support.generateEventlog.entity.PetriNet;
import team.b8311.processMiningPlatform.support.generateEventlog.entity.Place;
import team.b8311.processMiningPlatform.support.generateEventlog.entity.Transition;

public class Test {

	public static void main(String[] args) throws Exception {
		PetriNet p = new PetriNet();
		Map<String, Place> places = new HashMap<String, Place>();
		places.put("0", new Place(0, "start", 1));
		places.put("1", new Place(1, "end", 0));
		places.put("3", new Place(3, "c1", 0));
		places.put("4", new Place(4, "c2", 0));
		places.put("8", new Place(8, "c3", 0));
		places.put("9", new Place(9, "c4", 0));
		places.put("12", new Place(12, "c5", 0));
		p.setPlaces(places);

		Map<String, Transition> transitions = new HashMap<String, Transition>();
		transitions.put("2", new Transition(2, "register request"));
		transitions.put("5", new Transition(5, "examine thoroughly"));
		transitions.put("6", new Transition(6, "examine casually"));
		transitions.put("7", new Transition(7, "check ticket"));
		transitions.put("10", new Transition(10, "decide"));
		transitions.put("11", new Transition(11, "reinitate request"));
		transitions.put("13", new Transition(13, "pay compensation"));
		transitions.put("14", new Transition(14, "reject request"));
		p.setTransitions(transitions);

		List<FlowRelation> flowRelations = new ArrayList<FlowRelation>();
		flowRelations.add(new FlowRelation(0, 2));
		flowRelations.add(new FlowRelation(2, 3));
		flowRelations.add(new FlowRelation(2, 4));
		flowRelations.add(new FlowRelation(3, 5));
		flowRelations.add(new FlowRelation(3, 6));
		flowRelations.add(new FlowRelation(4, 7));
		flowRelations.add(new FlowRelation(5, 8));
		flowRelations.add(new FlowRelation(6, 8));
		flowRelations.add(new FlowRelation(7, 9));
		flowRelations.add(new FlowRelation(8, 10));
		flowRelations.add(new FlowRelation(9, 10));
		flowRelations.add(new FlowRelation(10, 12));
		flowRelations.add(new FlowRelation(12, 11));
		flowRelations.add(new FlowRelation(11, 3));
		flowRelations.add(new FlowRelation(11, 4));
		flowRelations.add(new FlowRelation(12, 13));
		flowRelations.add(new FlowRelation(12, 14));
		flowRelations.add(new FlowRelation(13, 1));
		flowRelations.add(new FlowRelation(14, 1));
		p.setFlowRelations(flowRelations);

		XLog log = new XLogImpl(new XAttributeMapImpl());
		for (int i = 0; i < 100; i++) {
			traverse(p, log, i);
		}

		// 1.定义要输出文件的文件路径
		File file = new File("d:" + File.separator + "demo" + File.separator + "my.txt"); // 设置文件路径
		System.out.println(file.exists());
		// 1.此时由于目录不存在，所以文件不能够输出，那么应该首先创建目录
		if (!file.getParentFile().exists()) { // 如果父路径不存在
			file.getParentFile().mkdirs();
		}
		// 2.应该使用OutPutStream和其子类进行对象的实例化，此时目录存在，文件还不存在
		OutputStream output = new FileOutputStream(file);

		Thread.sleep(2000);
		XesXmlSerializer serializer = new XesXmlSerializer();
		serializer.serialize(log, output);
	}

	public static void traverse(PetriNet p, XLog log, int index) throws Exception {
		final CountDownLatch latch = new CountDownLatch(2);

		List<Integer> traversePlaces = new ArrayList<Integer>();
		traversePlaces.add(0);
		XAttributeMap attrMap = new XAttributeMapImpl();
		attrMap.put("concept:name", new XAttributeLiteralImpl("concept:name", String.valueOf(index)));
		XTrace trace = new XTraceImpl(attrMap);

		p.initPetriNet();
		Traverse t = new Traverse(traversePlaces, new ArrayList<Integer>(), new ArrayList<Integer>(), p, trace);
		new Thread(new PlaceThread(t, latch)).start();
		new Thread(new TransitionThread(t, latch)).start();

		latch.await();
		log.add(trace);
	}
}
