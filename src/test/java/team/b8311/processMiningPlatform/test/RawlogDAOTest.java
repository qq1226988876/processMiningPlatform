package team.b8311.processMiningPlatform.test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import team.b8311.processMiningPlatform.dao.IRawlogDAO;
import team.b8311.processMiningPlatform.domain.Normalizedlog;
import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.Rawlog;

@RunWith(SpringJUnit4ClassRunner.class) // Spring单元测试
@ContextConfiguration({ "classpath*:spring-*.xml" }) // 上下文配置
public class RawlogDAOTest {

	@Resource
	private IRawlogDAO rawlogDAO;

	/**
	 * 测试增加原始日志
	 */
	@Test
	public void testDoCreate() {
		Rawlog rawlog = new Rawlog();
		rawlog.setRawlogName("原始日志5");
		rawlog.setRawlogFormat("txt");
		rawlog.setRawlogHDFSId("f59df595-0aa0-4847-b905-1e53723215f2");
		Project p = new Project();
		p.setProjectId(1);
		Normalizedlog nl = new Normalizedlog();
		nl.setNormalizedlogId(1);
		rawlog.setNormalizedlog(nl);
		rawlog.setProject(p);
		try {
			if (rawlogDAO.doCreate(rawlog)) {
				System.out.println("添加成功，原始日志id为：" + rawlog.getRawlogId());
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * 测试删除原始日志
	 */
	@Test
	public void testDoRemoveBatch() {
		Set<Integer> ids = new HashSet<Integer>();
		ids.add(4);
		try {
			if (rawlogDAO.doRemoveBatch(ids)) {
				System.out.println("删除成功");
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * 测试根据原始日志id查询相关信息
	 */
	@Test
	public void testFindById() {
		try {
			System.out.println(rawlogDAO.findById(5));
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * 测试分页查询全部原始日志信息
	 */
	@Test
	public void testFindAllSplitDetails() {
		try {
			List<Rawlog> all = rawlogDAO.findAllSplitDetails(0, 10, "rawlog_name", "", 1);
			for (int x = 0; x < all.size(); x++) {
				System.out.println(all.get(x));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
