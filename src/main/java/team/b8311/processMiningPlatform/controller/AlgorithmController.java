package team.b8311.processMiningPlatform.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import team.b8311.processMiningPlatform.domain.Algorithm;
import team.b8311.processMiningPlatform.domain.ResponseObj;
import team.b8311.processMiningPlatform.exception.BaseExceptionHandleAction;
import team.b8311.processMiningPlatform.service.IAlgorithmService;
import team.b8311.processMiningPlatform.utils.Config;
import team.b8311.processMiningPlatform.utils.JsonFormatTool;

@Controller
@RequestMapping("/algorithmAction")
public class AlgorithmController extends BaseExceptionHandleAction {
	@Resource
	private IAlgorithmService algorithmService;

	private ResponseObj responseObj;

	@RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	@ResponseBody
	public Object upload(HttpServletRequest req, HttpServletResponse res, HttpSession session, MultipartFile jar,
			String algName, String outputType, String desc, boolean type, String packName, String className,
			String methodName, String params) {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<>();
		String jarFullName = jar.getOriginalFilename();
		String jarName = jarFullName.substring(0, jarFullName.indexOf("."));

		if (jar.isEmpty()) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("文件为空");
			return responseObj;
		}
		// Jar根目录
		String jarRootPath = "/algorithmForProcessMining/";
		File jarRootDirFile = new File(jarRootPath);
		boolean isSavedSuccess = true;
		String jarPath = "", configPath = "";
		// 判断Jar根目录是否存在
		if (!jarRootDirFile.exists()) {
			isSavedSuccess = jarRootDirFile.mkdir();
			responseObj.setMsg("创建Jar根目录失败");
		} else {
			// 每个Jar和配置文件放在一个文件夹下面，用当前时间保证文件夹名字不重复
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkmmssSSS");
			String parentDirPath = jarRootPath + File.separator + jarName + "_" + sdf.format(new Date());
			File jarParentFile = new File(parentDirPath);
			if (!jarParentFile.exists()) {
				isSavedSuccess = jarParentFile.mkdir();
			} else {
				// 生成随机数，减小重复命名的概率
				int random = (int) (Math.random() * 9 + 1) * 1000000;
				parentDirPath = jarRootPath + File.separator + jarName + "_" + sdf.format(new Date()) + "_" + random;
				jarParentFile = new File(parentDirPath);
				isSavedSuccess = jarParentFile.mkdir();
			}
			jarPath = jarParentFile + File.separator + jarFullName;
			configPath = jarParentFile + File.separator + "config.json";
			File jarFile = new File(jarPath);
			try {
				jar.transferTo(jarFile);
			} catch (IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				isSavedSuccess = false;
				responseObj.setMsg("获取前端传送的jar文件失败");
			}
			// 配置文件
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("packageName", packName);
			jsonObject.put("className", className);
			jsonObject.put("methodName", methodName);
			JSONArray paramArray = JSONArray.parseArray(params);
			jsonObject.put("params", paramArray);
			System.out.println(params);

			try {
				File configFile = new File(configPath);
				FileWriter fw = new FileWriter(configFile.getPath());
				PrintWriter out = new PrintWriter(fw);
				out.write(JsonFormatTool.formatJson(jsonObject.toString()));
				System.out.println(jsonObject.toString());
				out.println();
				fw.close();
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				isSavedSuccess = false;
				responseObj.setMsg("创建配置文件失败");
			}

		}
		if (isSavedSuccess) {
			// 算法的相关信息保存到数据库
			Algorithm al = new Algorithm();
			al.setAlgorithmName(algName);

			al.setAlgorithmOutputType(outputType);
			al.setAlgorithmInformation(desc);
			al.setAlgorithmType(type);
			al.setAlgorithmPackage(packName);
			al.setAlgorithmClassName(className);
			al.setAlgorithmMethod(methodName);
			al.setAlgorithmIsAvailable(true);
			al.setAlgorithmPath(jarPath);
			al.setAlgorithmConfigPath(configPath);
			try {
				if (algorithmService.insert(al)) {
					responseObj.setCode(ResponseObj.OK);
					responseObj.setMsg("上传成功");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			responseObj.setCode(ResponseObj.FAILED);
		}
		// String prefix = session.getServletContext().getRealPath("/jars");
		// System.out.println(prefix);
		return responseObj;
	}

	/**
	 * @RequestMapping(value = "/upload", method = RequestMethod.POST)
	 * @ResponseBody public Object uploadAlgorithm(HttpServletRequest req,
	 *               HttpServletResponse res, HttpSession session, MultipartFile
	 *               pack, MultipartFile conf, String algoName, String
	 *               description, boolean type) throws Exception {
	 *               res.setHeader("Access-Control-Allow-Origin", "*"); //
	 *               防止出现跨域访问异常 res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
	 *               responseObj = new ResponseObj<Algorithm>(); Algorithm al =
	 *               new Algorithm();
	 * 
	 *               String prefix =
	 *               session.getServletContext().getRealPath("/jars"); //jar包文件
	 *               String packFileName = UUID.randomUUID().toString(); //配置文件
	 *               String confFileName = UUID.randomUUID().toString(); File
	 *               packFile = null; File confFile = null; // try { packFile =
	 *               new File(prefix, packFileName); if
	 *               (!packFile.getParentFile().exists()) {
	 *               packFile.getParentFile().mkdirs(); } confFile = new
	 *               File(prefix, confFileName); if
	 *               (!confFile.getParentFile().exists()) {
	 *               confFile.getParentFile().mkdirs(); }
	 *               pack.transferTo(packFile); conf.transferTo(confFile); // }
	 *               catch (Exception e) { // return new
	 *               ResponseData(Code.SYSTEM_ERROR); // }
	 * 
	 *               Config config = ConfigParser.parseConfig(prefix + "/" +
	 *               confFileName);
	 * 
	 *               al.setAlgorithmClassName(config.getClassName());
	 *               al.setAlgorithmPackage(config.getPackageName());
	 *               al.setAlgorithmMethod(config.getMethodName());
	 *               al.setAlgorithmName(algoName);
	 *               al.setAlgorithmInformation(description);
	 *               al.setAlgorithmType(type); al.setAlgorithmPath(prefix + "/"
	 *               + packFileName); al.setAlgorithmConfigPath(prefix + "/" +
	 *               confFileName);
	 * 
	 *               if (algorithmService.insert(al)) {
	 *               responseObj.setCode(ResponseObj.OK);
	 *               responseObj.setMsg("上传算法成功"); responseObj.setData(al); }
	 *               else { responseObj.setCode(ResponseObj.FAILED);
	 *               responseObj.setMsg("上传算法失败"); }
	 * 
	 *               return responseObj; }
	 */
	/**
	 * 获取算法信息接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param currentPage
	 *            当前所在页
	 * @param lineSize
	 *            每页显示的记录数
	 * @param keyWord
	 *            模糊查询的关键字
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/list" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object list(HttpServletRequest req, HttpServletResponse res, int currentPage, int lineSize, String keyWord)
			throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<Map>();

		Map<String, Object> map = algorithmService.list(currentPage, lineSize, "algorithm_name", keyWord);
		if (null != map) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("获取算法信息成功");
			responseObj.setData(map);
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("获取算法信息失败！");
			return responseObj;
		}
	}

	@RequestMapping(value = "/listAll", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
	@ResponseBody
	public Object listAll(HttpServletRequest req, HttpServletResponse res) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<Map>();
		Map<String, Object> map = algorithmService.listAll();
		if (null != map) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("获取算法信息成功");
			responseObj.setData(map);
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("获取算法信息失败！");
			return responseObj;
		}
	}

	@RequestMapping(value = "/listAvailable", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
	@ResponseBody
	public Object listAvailabel(HttpServletRequest req, HttpServletResponse res) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<Map>();
		Map<String, Object> map = algorithmService.listAvailabel();
		if (null != map) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("获取算法信息成功");
			responseObj.setData(map);
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("获取算法信息失败！");
			return responseObj;
		}

	}

	@RequestMapping(value = "/delete" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object delete(HttpServletRequest req, HttpServletResponse res, int algoId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj();

		Algorithm algo = algorithmService.get(algoId);
		String jarPath = algo.getAlgorithmPath();
		String configPath = algo.getAlgorithmConfigPath();
		File jarFile = new File(jarPath);
		File config = new File(configPath);
		if (jarFile.exists()) {
			jarFile.delete();
		}
		if (config.exists()) {
			config.delete();
		}
		Set<Integer> ids = new HashSet<Integer>();
		ids.add(algoId);
		if (algorithmService.delete(ids)) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("删除算法成功！");
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("删除算法失败！");
		}

		return responseObj;
	}

	@RequestMapping(value = "/updateAlgorithmAvailable" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object updateAlgorithmAvailabel(HttpServletRequest req, HttpServletResponse res, int algoId,
			boolean isAvailable) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj();
		Algorithm algo = algorithmService.get(algoId);
		algo.setAlgorithmIsAvailable(isAvailable);
		boolean isSuccess = algorithmService.updateAlgorithmAvailable(algo);
		if (isSuccess) {
			responseObj.setCode(ResponseObj.OK);
		} else {
			responseObj.setCode(ResponseObj.FAILED);
		}
		return responseObj;
	}

	@RequestMapping(value = "/getConfig" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object getConfig(HttpServletRequest req, HttpServletResponse res, int algoId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj();

		Config config = algorithmService.getAlgoConfig(algoId);
		responseObj.setCode(ResponseObj.OK);
		responseObj.setData(config);
		responseObj.setMsg("参数信息返回");

		return responseObj;
	}
}
