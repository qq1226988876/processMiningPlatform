package team.b8311.processMiningPlatform.exception;

/**
 * 自定义异常处理类
 * 
 * @author qiuto
 *
 */
public class CustomException extends Exception {
	private String message; // 异常信息
	public CustomException(String message) {
		this.message = message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
