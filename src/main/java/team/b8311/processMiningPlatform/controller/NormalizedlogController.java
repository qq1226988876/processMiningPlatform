package team.b8311.processMiningPlatform.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import team.b8311.processMiningPlatform.domain.Eventlog;
import team.b8311.processMiningPlatform.domain.Normalizedlog;
import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.ResponseObj;
import team.b8311.processMiningPlatform.exception.BaseExceptionHandleAction;
import team.b8311.processMiningPlatform.exception.CustomException;
import team.b8311.processMiningPlatform.service.IEventlogService;
import team.b8311.processMiningPlatform.service.INormalizedlogService;
import team.b8311.processMiningPlatform.support.transToEvent.entity.Log4Normal;
import team.b8311.processMiningPlatform.support.transToEvent.service.ITransToEventService;
import team.b8311.processMiningPlatform.utils.HDFSUtils;

/**
 * 规范化日志模块控制器
 * 
 * @author qiuto
 *
 */
@Controller
@RequestMapping("/normalizedlogAction") // 外层地址
public class NormalizedlogController extends BaseExceptionHandleAction {
	@Resource
	private INormalizedlogService normalizedlogService; // 自动载入service
	@Resource
	private IEventlogService eventlogService; // 自动载入service
	@Resource
	private ITransToEventService transToEventService; // 自动载入service

	@Resource
	private HDFSUtils hdfs; // 自动载入操作HDFS对象

	private ResponseObj responseObj; // Api接口返回对象

	/**
	 * 上传规范化日志接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param file
	 *            上传的规范化日志文件
	 * @param format
	 *            上传的规范化日志格式
	 * @param projectId
	 *            规范化日志所属的项目id
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/upload" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object upload(HttpServletRequest req, HttpServletResponse res, MultipartFile file, String format,
			int projectId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码

		responseObj = new ResponseObj<Normalizedlog>();
		if (file.getSize() == 0) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("规范化日志大小为0！");
			return responseObj;
		}

		Normalizedlog nl = new Normalizedlog();
		nl.setNormalizedlogFormat(format);
		nl.setNormalizedlogName(file.getOriginalFilename());
		Project p = new Project();
		p.setProjectId(projectId);
		nl.setProject(p);

		if (normalizedlogService.upload(file.getInputStream(), nl)) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("上传规范化日志成功");
			responseObj.setData(normalizedlogService.get(nl.getNormalizedlogId()));
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("上传规范化日志失败！");
			return responseObj;
		}

	}

	/**
	 * 下载规范化日志文件
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param rawlogId
	 *            要下载的规范化日志id
	 */
	@RequestMapping(value = "/download" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	public void download(HttpServletRequest req, HttpServletResponse res, int normalizedlogId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码

		Normalizedlog nl = null;
		if ((nl = normalizedlogService.get(normalizedlogId)) == null) {
			throw new CustomException("规范化日志不存在");
		} else {
			InputStream in = new BufferedInputStream(
					hdfs.downloadFile(HDFSUtils.NORMALIZEDLOG_PATH_PREFIX + nl.getNormalizedlogHDFSId()));

			res.addHeader("Content-Disposition",
					"attachment;filename=" + new String(nl.getNormalizedlogName().getBytes("utf-8"), "iso-8859-1")); // 文件下载时获取文件名时的编码格式为iso-8859-1
			res.setContentType("multipart/form-data");

			OutputStream out = new BufferedOutputStream(res.getOutputStream());
			int temp = 0; // 表示每次读取的字节数据
			while ((temp = in.read()) != -1) {
				out.write(temp);
				out.flush();
			}
			out.close();
		}
	}

	/**
	 * 删除规范化日志接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param ids
	 *            要删除的原始日志id数组
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete" // 内层地址
			, method = RequestMethod.GET// 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object delete(HttpServletRequest req, HttpServletResponse res, @RequestParam("ids") Set<Integer> ids)
			throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常

		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码

		System.out.println(ids);

		responseObj = new ResponseObj<Object>();
		if (ids.size() == 0) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("请选择要删除的日志");
			return responseObj;
		}

		if (normalizedlogService.delete(ids)) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("删除日志成功！");
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("删除日志失败！");
			return responseObj;
		}
	}

	/**
	 * 获取规范化日志信息接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param currentPage
	 *            当前所在页
	 * @param lineSize
	 *            每页显示的记录数
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projectId
	 *            项目id
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/list" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object list(HttpServletRequest req, HttpServletResponse res, int currentPage, int lineSize, String keyWord,
			int projectId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<Map>();

		Map<String, Object> map = normalizedlogService.listDetails(currentPage, lineSize, "normalizedlog_name", keyWord,
				projectId);
		if (null != map) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("获取事件日志信息成功");
			responseObj.setData(map);
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("获取事件日志信息失败！");
			return responseObj;
		}
	}

	/**
	 * 转换为事件日志接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param session
	 *            http会话
	 * @param normalizedlogId
	 *            要事件化的规范化日志id
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/convertToEventlog" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object convertToEventlog(HttpServletRequest req, HttpServletResponse res, HttpSession session,
			int normalizedlogId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj();

		Normalizedlog nl = normalizedlogService.getDetails(normalizedlogId);
		int projectId = nl.getProject().getProjectId();
		String format = "xes";
		// 清除之前的事件日志
		if (nl.getEventlog() != null) {
			Set<Integer> ids = new HashSet<Integer>();
			ids.add(nl.getEventlog().getEventlogId());
			eventlogService.delete(ids);
		}
		String path = req.getSession().getServletContext().getRealPath("WEB-INF/temp/") + "/";

		// 封装规范化日志
		Log4Normal log4Norm = transToEventService.getLog(normalizedlogId);
		if (log4Norm != null) {
			// 事件化
			File outputFile = transToEventService.runEC(log4Norm, path);
			if (outputFile != null) {
				// 入库
				Eventlog el = new Eventlog();
				Project p = new Project();
				p.setProjectId(projectId);
				el.setProject(p);
				el.setEventlogName(log4Norm.getInputFile());
				el.setEventlogFormat(format);
				el.setNormalizedlog(nl);
				if (eventlogService.upload(new FileInputStream(outputFile), el)) {
					responseObj.setCode(ResponseObj.OK);
					responseObj.setMsg("事件化日志成功");
				} else {
					responseObj.setCode(ResponseObj.FAILED);
					responseObj.setMsg("事件化日志失败");
				}
			} else {
				responseObj.setCode(ResponseObj.FAILED);
				responseObj.setMsg("规范化日志没有关联");
			}
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("封装规范化日志失败");
		}
		return responseObj;
	}
}
