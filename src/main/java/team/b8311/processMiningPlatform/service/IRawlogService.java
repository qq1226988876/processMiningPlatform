package team.b8311.processMiningPlatform.service;

import java.io.InputStream;
import java.util.Map;
import java.util.Set;

import team.b8311.processMiningPlatform.domain.Normalizedlog;
import team.b8311.processMiningPlatform.domain.Rawlog;

public interface IRawlogService {

	/**
	 * 实现原始日志的添加操作，本次操作要调用IRawlogDAO接口的以下方法：<br>
	 * <li>调用IProjectDAO.findById()方法，判断要添加的日志是否存在
	 * <li>需要调用IRawlogtDAO.findByNameProjectId()方法，判断要添加的日志所属项目是否存在
	 * <li>嗲用IRawlogDAO.doCreate()方法，返回操作结果
	 * 
	 * @param rawlog
	 *            包含了要增加数据的vo对象
	 * @return 如果增加项目成功返回true，否则返回false（理论上不会返回，因为出错会抛出异常）
	 * @throws Exception
	 *             SQL执行异常或自定义的异常
	 */
	public boolean insert(Rawlog rawlog) throws Exception;

	/**
	 * 通过原始日志id查询相关内容，调用IRawlogDAO.findById()方法
	 * 
	 * @param id
	 *            项目id
	 * @return 如果表中有数据，封装为VO对象返回，否则返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Rawlog get(int id) throws Exception;

	/**
	 * 通过原始日志id获得字节输入流，调用IRawlogDAO接口的以下方法：<br>
	 * <li>调用IRawlogDAO.findById()方法,判断要下载的日志是否存在
	 * <li>调用HDFSUtils.downloadFile()方法，获得输入流
	 * 
	 * @param id
	 *            原始日志id
	 * @return 要下载日志的输入流
	 * @throws Exception
	 *             SQL执行异常或者错误信息异常
	 */
	// public InputStream download(int id) throws Exception;

	/**
	 * 实现原始日志的上传操作，本次操作要调用IRawlogDAO接口的以下方法：<br>
	 * <li>需要调用IRawlogtDAO.findByNameProjectId()方法，判断要添加的日志所属项目是否存在
	 * <li>调用IRawlogDAO.doCreate()方法，返回操作结果 <br>
	 * 调用HDFSUtils.uploadFile()方法，上传原始日志文件并返回HDFSId
	 * 
	 * @param in
	 *            原始日志文件流
	 * @param vo
	 *            要上传日志的vo对象
	 * @return 如果上传成功，返回true
	 * @throws Exception
	 *             SQL执行异常或者错误信息异常
	 */
	public boolean upload(InputStream in, Rawlog vo) throws Exception;

	/**
	 * 执行原始日志的删除操作，可以删除多个原始日志信息，调用IRawlogDAO的以下方法<br>
	 * <li>调用IRawlogDAO.findByIdDetails()查询原始日志对应规范化日志是否存在<br>
	 * 如果存在则调用INormalizedDAO.findById()获取规范化日志信息，并调用INormalizedDAO.
	 * doUpdate清除表之间的相关信息
	 * <li>调用IRawlogDAO.doRemoveBatch()
	 * 
	 * @param ids
	 *            包含了要删除的数据集合，没有重复数据
	 * @return 删除成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean delete(Set<Integer> ids) throws Exception;

	/**
	 * 根据项目id删除原始日志，调用IRawlogDAO.doRemoveByProjectId()方法
	 * 
	 * @param id
	 *            项目id
	 * @return 删除成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	// public boolean deleteByProjectId(int id) throws Exception;

	/**
	 * 实现数据的模糊查询与数据统计，要调用IRawlogDAO接口的两个方法：<br>
	 * <li>调用IRawlogDAO.findAllSplitDetails()方法，查询出所有的表数据，返回List<Rawlog>；
	 * <li>调用IRawlogDAO.getAllCount()方法，查询出所有的数据量，返回Integer
	 * 
	 * @param currentPage
	 *            当前所在页
	 * @param lineSize
	 *            每页显示的记录数
	 * @param column
	 *            模糊查询的数据列
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projectId
	 *            项目id
	 * @return 本方法需要返回多种数据类型，所以使用Map集合返回,由于类型不同，所有value的类型置为Object<br>
	 *         <li>key = allRawlogs,value =
	 *         IRawlogDAO.findAllSplitDetails()返回结果，List<Rawlog>;
	 *         <li>key = RawlogCount,value =
	 *         IRawlogDAO.getAllCount()返回结果，Integer;
	 * @throws Exception
	 *             SQL执行异常或错误信息异常
	 */
	public Map<String, Object> listDetails(int currentPage, int lineSize, String column, String keyWord, int projectId)
			throws Exception;

	/**
	 * 
	 * @param id
	 *            原始日志id
	 * 
	 * @param formats
	 *            原始日志格式配置
	 * @param timeNames
	 *            时间项整合
	 * @param targetTimeName
	 * @param dataNames
	 *            数据项整合
	 * @param oriitemSeparator
	 *            原数据项分隔符
	 * @param orinameValSeparator
	 *            原名称值分隔符
	 * @param orinulVal
	 *            原空值
	 * @param targetitemSeparator
	 *            目标数据项分隔符
	 * @param targetnameValSeparator
	 *            目标名称值分隔符
	 * @param targetnulVal
	 *            目标空值
	 * @return
	 * @throws Exception
	 */
	public boolean convertToNormalizelog(int id, String formats, String timeNames, String targetTimeName,
			String dataNames, String oriitemSeparator, String orinameValSeparator, String orinulVal,
			String targetitemSeparator, String targetnameValSeparator, String targetnulVal, Normalizedlog nl)
			throws Exception;
}
