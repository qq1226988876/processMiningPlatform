package team.b8311.processMiningPlatform.test;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import team.b8311.processMiningPlatform.dao.IEventlogDAO;
import team.b8311.processMiningPlatform.domain.Eventlog;
import team.b8311.processMiningPlatform.domain.Normalizedlog;
import team.b8311.processMiningPlatform.domain.Project;

@RunWith(SpringJUnit4ClassRunner.class) // Spring单元测试
@ContextConfiguration({ "classpath*:spring-*.xml" }) // 上下文配置
public class EventlogDAOTest {

	@Resource
	private IEventlogDAO eventlogDAO; // 初始化DAO层

	/**
	 * 测试添加日志事件
	 */
	@Test
	public void testDoCreate() {
		Eventlog el = new Eventlog();
		el.setEventlogName("事件日志2");
		el.setEventlogFormat("txt");
		el.setEventlogHDFSId("asd");
		el.setEventlogController("流程活动操作人");
		el.setEventlogTotalInstance(10);
		el.setEventlogTotalEvent(10);
		el.setEventlogAverageEvent(5);
		el.setEventlogProcessActivityEvent("流程活动事件");
		Project p = new Project();
		p.setProjectId(1);
		Normalizedlog nl = new Normalizedlog();
		nl.setNormalizedlogId(1);
		el.setProject(p);
		el.setNormalizedlog(nl);
		try {
			if (eventlogDAO.doCreate(el)) {
				System.out.println("添加成功，事件id为" + el.getEventlogId());
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * 测试删除事件日志
	 */
	@Test
	public void testDoRemoveBatch() {
		Set<Integer> ids = new HashSet<Integer>();
		ids.add(3);
		try {
			if (eventlogDAO.doRemoveBatch(ids)) {
				System.out.println("删除成功！");
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * 测试根据id查询事件日志信息
	 */
	@Test
	public void testFindById() {
		try {
			System.out.println(eventlogDAO.findById(4));
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	/**
	 * 测试根据名称和项目id查询事件日志信息
	 */
	@Test
	public void testFindByNameProjectId() {
		try {
			System.out.println(eventlogDAO.findByNameProjectId("事件日志2", 1));
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	@Test
	public void testFindAllSplitMergeDetails() {
		try {
			System.out.println(eventlogDAO.findAllSplitMergeDetails(0, 10, "eventlog_name", "", 1));
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	@Test
	public void testFindByIdLogDetails() {
		try {
			System.out.println(eventlogDAO.findByIdLogDetails(104));
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
}
