package team.b8311.processMiningPlatform.utils;

import java.io.Serializable;
import java.util.List;

import net.sf.json.JSONObject;
import team.b8311.processMiningPlatform.support.C_netFormatter.format.Formatter;
import team.b8311.processMiningPlatform.utils.request.MiningData;

/**
 * 挖掘结果的封装类
 * 
 * @author qiuto
 *
 */
public class MiningResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7086578963425483336L;

	private transient MiningData miningData; // 不需要缓存

	private Formatter net;
	private JSONObject jsonObject;

	private String usedTime;

	private List<String> traces;

	private List<String> allTraces;

	public Formatter getNet() {
		return net;
	}

	public void setNet(Formatter net) {
		this.net = net;
	}

	public String getUsedTime() {
		return usedTime;
	}

	public void setUsedTime(String usedTime) {
		this.usedTime = usedTime;
	}

	public List<String> getTraces() {
		return traces;
	}

	public void setTraces(List<String> traces) {
		this.traces = traces;
	}

	public List<String> getAllTraces() {
		return allTraces;
	}

	public void setAllTraces(List<String> allTraces) {
		this.allTraces = allTraces;
	}

	public MiningData getMiningData() {
		return miningData;
	}

	public void setMiningData(MiningData miningData) {
		this.miningData = miningData;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}
}
