package team.b8311.processMiningPlatform.support.generateEventlog.operation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XLogImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.deckfour.xes.out.XesXmlSerializer;

import team.b8311.processMiningPlatform.support.generateEventlog.entity.FlowRelation;
import team.b8311.processMiningPlatform.support.generateEventlog.entity.PetriNet;
import team.b8311.processMiningPlatform.support.generateEventlog.entity.Place;
import team.b8311.processMiningPlatform.support.generateEventlog.entity.Transition;

public class Test2 {

	public static void main(String[] args) throws Exception {
		// TODO 自动生成的方法存根
		PetriNet p = new PetriNet();
		Map<String, Place> places = new HashMap<String, Place>();
		places.put("0", new Place(0, "start", 1));
		places.put("1", new Place(1, "end", 0));
		places.put("3", new Place(3, "c1", 0));
		p.setPlaces(places);

		Map<String, Transition> transitions = new HashMap<String, Transition>();
		transitions.put("2", new Transition(2, "login"));
		transitions.put("4", new Transition(4, "succeed"));
		transitions.put("5", new Transition(5, "fail"));
		p.setTransitions(transitions);

		List<FlowRelation> flowRelations = new ArrayList<FlowRelation>();
		flowRelations.add(new FlowRelation(0, 2));
		flowRelations.add(new FlowRelation(2, 3));
		flowRelations.add(new FlowRelation(3, 4));
		flowRelations.add(new FlowRelation(4, 1));
		flowRelations.add(new FlowRelation(3, 5));
		flowRelations.add(new FlowRelation(5, 0));
		p.setFlowRelations(flowRelations);

		p.initPetriNet();

		XLog log = new XLogImpl(new XAttributeMapImpl());
		for (int i = 0; i < 100; i++) {
			traverse(p, log);
		}

		// 1.定义要输出文件的文件路径
		File file = new File("d:" + File.separator + "demo" + File.separator + "my.xes"); // 设置文件路径
		System.out.println(file.exists());
		// 1.此时由于目录不存在，所以文件不能够输出，那么应该首先创建目录
		if (!file.getParentFile().exists()) { // 如果父路径不存在
			file.getParentFile().mkdirs();
		}
		// 2.应该使用OutPutStream和其子类进行对象的实例化，此时目录存在，文件还不存在
		OutputStream output = new FileOutputStream(file);

		Thread.sleep(2000);
		XesXmlSerializer serializer = new XesXmlSerializer();
		serializer.serialize(log, output);
	}

	public static void traverse(PetriNet p, XLog log) throws Exception {
		final CountDownLatch latch = new CountDownLatch(2);

		List<Integer> traversePlaces = new ArrayList<Integer>();
		traversePlaces.add(0);
		XTrace trace = new XTraceImpl(new XAttributeMapImpl());
		p.initPetriNet();
		Traverse t = new Traverse(traversePlaces, new ArrayList<Integer>(), new ArrayList<Integer>(), p, trace);
		new Thread(new PlaceThread(t, latch)).start();
		new Thread(new TransitionThread(t, latch)).start();

		latch.await();
		log.add(trace);
	}
}
