package team.b8311.processMiningPlatform.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import team.b8311.processMiningPlatform.dao.IUserDAO;
import team.b8311.processMiningPlatform.domain.User;

@RunWith(SpringJUnit4ClassRunner.class) // spring的单元测试
@ContextConfiguration({ "classpath*:spring-*.xml" }) // 上下文配置
public class UserDAOTest {

	@Resource
	private IUserDAO userDAO; // 初始化DAO层，面向接口编程

	/**
	 * 根据id查找用户测试
	 */
	@Test
	public void testFindById() {
		User user = new User();
		user.setId(1);
		User result = null; // 受影响的行数默认为0
		try {
			result = userDAO.findById(user.getId());
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
			System.out.println("查找用户失败");
		}
		if (null != result) {
			System.out.println("查找用户成功\n" + result.toString());
		}
	}

	/**
	 * 根据email查找用户测试
	 */
	@Test
	public void testFindByEmail() {
		User result = null; // 受影响的行数默认为0
		try {
			result = userDAO.findByEmail("qiuqiu@b8311.com");
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
			System.out.println("查找用户失败");
		}
		if (null != result) {
			System.out.println("查找用户成功\n" + result.toString());
		}
	}

	/**
	 * 添加用户
	 */
	@Test
	public void testDoCreate() {
		User user = new User();
		user.setEmail("yudayu@b8311.com");
		user.setPassword("b8311");
		user.setNickname("yudayu");
		// User result = null; // 受影响的行数默认为0
		try {
			if (userDAO.doCreate(user)) {
				System.out.println("添加用户成功");
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
			System.out.println("添加用户失败");
		}
	}

	/**
	 * 删除用户
	 */
	@Test
	public void testDoRemove() {
		try {
			if (userDAO.doRemove(3)) {
				System.out.println("删除用户成功");
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
			System.out.println("删除用户失败");
		}
	}

	/**
	 * 更新用户信息
	 */
	@Test
	public void testDoUpdate() {
		User user = new User();
		user.setId(4);
		user.setEmail("yudayu@b8311.com");
		user.setPassword("b8311");
		user.setNickname("yudayu2");
		try {
			if (userDAO.doUpdate(user)) {
				System.out.println("更新用户成功");
			}
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
			System.out.println("更新用户失败");
		}
	}

}
