package team.b8311.processMiningPlatform.support.generateEventlog.entity;

/**
 * 变迁类
 * 
 * @author qiuto
 *
 */
public class Transition {
	private Integer transitionId; // 变迁编号
	private String transitionName; // 变迁名称
	private Boolean enabled; // 变迁使能

	public Transition() {
		// 无参构造函数
	}

	public Transition(Integer transitionId, String transitionName, Boolean enabled) {
		super();
		this.transitionId = transitionId;
		this.transitionName = transitionName;
		this.enabled = enabled;
	}

	public Transition(Integer transitionId, String transitionName) {
		this(transitionId, transitionName, false);
	}

	public Integer getTransitionId() {
		return transitionId;
	}

	public void setTransitionId(Integer transitionId) {
		this.transitionId = transitionId;
	}

	public String getTransitionName() {
		return transitionName;
	}

	public void setTransitionName(String transitionName) {
		this.transitionName = transitionName;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "Transition [transitionId=" + transitionId + ", transitionName=" + transitionName + ", enabled="
				+ enabled + "]";
	}

}
