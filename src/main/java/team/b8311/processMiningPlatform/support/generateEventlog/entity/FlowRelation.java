package team.b8311.processMiningPlatform.support.generateEventlog.entity;

/**
 * 流关系类
 * 
 * @author qiuto
 *
 */
public class FlowRelation {
	private Integer from;
	private Integer to;

	public FlowRelation() {
		// 无参构造函数
	}

	public FlowRelation(Integer from, Integer to) {
		super();
		this.from = from;
		this.to = to;
	}

	public Integer getFrom() {
		return from;
	}

	public void setFrom(Integer from) {
		this.from = from;
	}

	public Integer getTo() {
		return to;
	}

	public void setTo(Integer to) {
		this.to = to;
	}

	@Override
	public String toString() {
		return "FlowRelation [from=" + from + ", to=" + to + "]";
	}

}
