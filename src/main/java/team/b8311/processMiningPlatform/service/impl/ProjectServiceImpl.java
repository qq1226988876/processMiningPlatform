package team.b8311.processMiningPlatform.service.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import team.b8311.processMiningPlatform.dao.IProjectDAO;
import team.b8311.processMiningPlatform.dao.IRawlogDAO;
import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.service.IProjectService;

@Service("projectService") // 给业务层类起别名纳入到Spring容器
public class ProjectServiceImpl implements IProjectService {

	@Resource
	private IProjectDAO projectDAO; // 注入DAO层对象
	@Resource
	private IRawlogDAO rawlogDAO; // 注入DAO层对象

	@Override
	public int insert(Project project) throws Exception {
		try {
			// 要增加的项目不存在，则findByNameUserId()返回的结果就是null
			if (projectDAO.findByNameUserId(project.getProjectName(), project.getUser().getId()) == null) {
				if (projectDAO.doCreateAndGetKey(project) != 0) {
					System.out.println("项目id为" + project.getProjectId());
					return project.getProjectId();
				}
			}
			return 0;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean update(Project vo) throws Exception {
		try {
			// 要修改的项目名称不存在,或者要修改的项目名称就是该项目的原名称
			if ((projectDAO.findByNameUserId(vo.getProjectName(), vo.getUser().getId()) == null)
					|| (projectDAO.findById(vo.getProjectId()).getProjectName().equals(vo.getProjectName()))) {
				return projectDAO.doUpdate(vo);
			}
			return false;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean delete(Set<Integer> ids) throws Exception {
		try {
			if (projectDAO.doRemoveBatch(ids)) {
				Iterator<Integer> it = ids.iterator();
				while (it.hasNext()) {
					rawlogDAO.doRemoveByProjectId(it.next());
				}
				return true;
			}
			return false;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Project> list(String keyWord, int userId) throws Exception {
		try {
			return projectDAO.findByName(userId, keyWord);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Project get(String projectName, int userId) throws Exception {
		try {
			return projectDAO.findByNameUserId(projectName, userId);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Project get(int projectId) throws Exception {
		try {
			return projectDAO.findById(projectId);
		} catch (Exception e) {
			throw e;
		}
	}

}
