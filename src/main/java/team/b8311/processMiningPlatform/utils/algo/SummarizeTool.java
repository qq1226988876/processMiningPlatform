package team.b8311.processMiningPlatform.utils.algo;

public class SummarizeTool {
	private int totalInstanceNum; // instance数量
	private int totalEventNum; // event数量
	private int average; // 平均每个instance中event数量
	private String processActivityEvent; // 所有活动名称
	private String controller; // 活动执行者

	public int getTotalInstanceNum() {
		return totalInstanceNum;
	}

	public void setTotalInstanceNum(int totalInstanceNum) {
		this.totalInstanceNum = totalInstanceNum;
	}

	public int getTotalEventNum() {
		return totalEventNum;
	}

	public void setTotalEventNum(int totalEventNum) {
		this.totalEventNum = totalEventNum;
	}

	public int getAverage() {
		return average;
	}

	public void setAverage(int average) {
		this.average = average;
	}

	public String getProcessActivityEvent() {
		return processActivityEvent;
	}

	public void setProcessActivityEvent(String processActivityEvent) {
		this.processActivityEvent = processActivityEvent;
	}

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}
}
