package team.b8311.processMiningPlatform.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

/**
 * 全局的异常处理器
 * 
 * @author qiuto
 *
 */
public class CustomExceptionResolver implements HandlerExceptionResolver {

	@Deprecated
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		ex.printStackTrace();

		CustomException customException = null;

		if (ex instanceof CustomException) {
			// 如果抛出的是系统自定义的异常则直接转换
			customException = (CustomException) ex;
		} else {
			// 如果抛出的不是系统自定义异常则重新构造一个未知错误异常
			customException = new CustomException("未知错误，请与系统管理员联系");
		}

		return null;
	}

}
