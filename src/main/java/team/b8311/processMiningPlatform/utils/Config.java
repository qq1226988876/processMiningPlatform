package team.b8311.processMiningPlatform.utils;

import java.io.Serializable;
import java.util.List;

public class Config implements Serializable {
	private static final long serialVersionUID = -8958841906764029251L;

	private String packageName;
	private String className;
	private String methodName;
	private List<Param> params;

	private String returnClassName;

	public Config() {

	}

	public Config(String packageName, String className, String methodName, List<Param> params) {
		this.packageName = packageName;
		this.className = className;
		this.methodName = methodName;
		this.params = params;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public List<Param> getParams() {
		return params;
	}

	public void setParams(List<Param> params) {
		this.params = params;
	}

	public String getReturnClassName() {
		return returnClassName;
	}

	public void setReturnClassName(String returnClassName) {
		this.returnClassName = returnClassName;
	}
}
