package team.b8311.processMiningPlatform.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import team.b8311.processMiningPlatform.domain.Eventlog;

public interface IEventlogDAO extends IDAO<Integer, Eventlog> {
	/**
	 * 实现表之间关联字段的修改
	 * 
	 * @param vo
	 *            包含了要修改的信息
	 * @return 数据修改成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean doUpdateReference(Eventlog vo) throws Exception;

	/**
	 * 实现关联融合表的修改
	 * 
	 * @param vo
	 *            包含了要修改的信息
	 * @return 数据修改成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean doUpdateMergeReference(Eventlog vo) throws Exception;

	/**
	 * 删除融合日志的相关信息
	 * 
	 * @param id
	 *            融合日志id
	 * @return 删除成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean doRemoveMerge(Integer id) throws Exception;

	/**
	 * 查询事件日志的日志相关详细信息
	 * 
	 * @param id
	 *            要查询的事件日志id
	 * @return 所有对象以VO对象返回，如果没有返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Eventlog findByIdLogDetails(Integer id) throws Exception;

	/**
	 * 查询融合日志相关信息
	 * 
	 * @param id
	 *            要查询的融合日志id
	 * @return 所有的对象以VO对象返回，如果没有返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Eventlog findByIdMergeDetails(Integer id) throws Exception;

	/**
	 * 通过项目id和事件日志名称查询日志
	 * 
	 * @param name
	 *            事件日志名称
	 * @param projectId
	 *            项目id
	 * @return 如果表中有数据，则数据封装为VO对象返回<br>
	 *         如果没有数据返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Eventlog findByNameProjectId(@Param("name") String name, @Param("projectId") Integer projectId)
			throws Exception;

	/**
	 * 分页查询事件日志的完整信息
	 * 
	 * @param startRow
	 *            起始的位置
	 * @param lineSize
	 *            所要显示的数据行数
	 * @param column
	 *            要进行模糊查询的数据列
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projecId
	 *            项目id
	 * @return 如果表中有数据，则所有的数据会封装为VO对象而后利用List的集合返回<br>
	 *         如果表中没有数据，那么集合的长度为0
	 * @throws Exception
	 *             SQL执行异常
	 */
	public List<Eventlog> findAllSplitLogDetails(@Param("startRow") Integer startRow,
			@Param("lineSize") Integer lineSize, @Param("column") String column, @Param("keyWord") String keyWord,
			@Param("projectId") Integer projecId) throws Exception;

	/**
	 * 按照项目id进行模糊查询数据量的统计，如果表中没有数据就是0
	 * 
	 * @param column
	 *            要进行模糊查询的数据列
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projectId
	 *            所属的项目id
	 * @return 返回表中的数据量，如果没有数据返回0
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Integer getAllCountByProjectId(@Param("column") String column, @Param("keyWord") String keyWord,
			@Param("projectId") Integer projectId) throws Exception;

	/**
	 * 实现融合日志关联表的插入
	 * 
	 * @param vo
	 *            保存融合日志信息的关联操作
	 * @return 数据插入成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean doCreateMerge(@Param("algorithmId") Integer algorithmId,
			@Param("targetEventlogId") Integer targetEventlogId, @Param("sourceEventlog1Id") Integer sourceEventlog1Id,
			@Param("sourceEventlog2Id") Integer sourceEventlog2Id) throws Exception;

	/**
	 * 分页查询融合日志的相关信息
	 * 
	 * @param startRow
	 *            起始的位置
	 * @param lineSize
	 *            所要显示的数据行数
	 * @param column
	 *            要进行模糊查询的数据列
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projecId
	 *            项目id
	 * @return 如果表中有数据，则所有的数据会封装为VO对象而后利用List的集合返回<br>
	 *         如果表中没有数据，那么集合的长度为0
	 * @throws Exception
	 *             SQL执行异常
	 */
	public List<Eventlog> findAllSplitMergeDetails(@Param("startRow") Integer startRow,
			@Param("lineSize") Integer lineSize, @Param("column") String column, @Param("keyWord") String keyWord,
			@Param("projectId") Integer projecId) throws Exception;

	/**
	 * 按照项目id进行模糊查询数据量的统计，如果表中没有数据就是0
	 * 
	 * @param column
	 *            要进行模糊查询的数据列
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projectId
	 *            所属的项目id
	 * @return 返回表中的数据量，如果没有数据返回0
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Integer getAllMergeCountByProjectId(@Param("column") String column, @Param("keyWord") String keyWord,
			@Param("projectId") Integer projectId) throws Exception;

}
