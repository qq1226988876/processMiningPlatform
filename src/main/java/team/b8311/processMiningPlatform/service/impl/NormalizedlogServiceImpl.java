package team.b8311.processMiningPlatform.service.impl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import team.b8311.processMiningPlatform.dao.IEventlogDAO;
import team.b8311.processMiningPlatform.dao.INormalizedlogDAO;
import team.b8311.processMiningPlatform.dao.IProjectDAO;
import team.b8311.processMiningPlatform.dao.IRawlogDAO;
import team.b8311.processMiningPlatform.domain.Eventlog;
import team.b8311.processMiningPlatform.domain.Normalizedlog;
import team.b8311.processMiningPlatform.domain.Rawlog;
import team.b8311.processMiningPlatform.exception.CustomException;
import team.b8311.processMiningPlatform.service.INormalizedlogService;
import team.b8311.processMiningPlatform.utils.HDFSUtils;

@Service("NormalizedlogService") // 给业务层起别名纳入到Spring容器
public class NormalizedlogServiceImpl implements INormalizedlogService {

	@Resource
	private IRawlogDAO rawlogDAO; // 注入DAO层对象
	@Resource
	private INormalizedlogDAO normalizedlogDAO; // 注入DAO层对象
	@Resource
	private IEventlogDAO eventlogDAO; // 注入DAO层对象
	@Resource
	private IProjectDAO projectDAO; // 注入DAO层对象
	@Resource
	private HDFSUtils hdfs; // 注入HDFS工具类对象

	@Override
	public boolean upload(InputStream in, Normalizedlog vo) throws Exception {
		if (normalizedlogDAO.findByNameProjectId(vo.getNormalizedlogName(), vo.getProject().getProjectId()) != null) {
			throw new CustomException("日志名已存在！");
		} else {
			String hdfsId = hdfs.uploadFile(in, HDFSUtils.NORMALIZEDLOG_PATH_PREFIX);
			if (null != hdfsId) {
				vo.setNormalizedlogHDFSId(hdfsId);
				return normalizedlogDAO.doCreate(vo);
			} else {
				throw new CustomException("上传HDFS失败！");
			}
		}
	}

	@Override
	public boolean delete(Set<Integer> ids) throws Exception {
		Rawlog rl = null;
		Normalizedlog nl = null;
		Eventlog el = null;
		Iterator it = ids.iterator();
		while (it.hasNext()) {
			nl = normalizedlogDAO.findByIdDetails((Integer) it.next());
			System.out.println(nl);
			if (nl.getRawlog() != null) {// 存在对应的原始日志
				rl = rawlogDAO.findByIdDetails(nl.getRawlog().getRawlogId());
				rl.setNormalizedlog(null);
				rawlogDAO.doUpdate(rl);
			}
			if (nl.getEventlog() != null) { // 存在对应的事件日志
				el = eventlogDAO.findByIdLogDetails(nl.getEventlog().getEventlogId());
				el.setNormalizedlog(null);
				eventlogDAO.doUpdateReference(el);
			}
			hdfs.deleteFile(HDFSUtils.NORMALIZEDLOG_PATH_PREFIX + nl.getNormalizedlogHDFSId());
		}
		return normalizedlogDAO.doRemoveBatch(ids);
	}

	@Override
	public Normalizedlog get(int id) throws Exception {
		return normalizedlogDAO.findById(id);
	}

	@Override
	public Normalizedlog getDetails(int id) throws Exception {
		return normalizedlogDAO.findByIdDetails(id);
	}

	@Override
	public Map<String, Object> listDetails(int currentPage, int lineSize, String column, String keyWord, int projectId)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("allNormalizedlogs", normalizedlogDAO.findAllSplitDetails((currentPage - 1) * lineSize, lineSize,
				column, keyWord, projectId));
		map.put("normalizedlogCount", normalizedlogDAO.getAllCountByProjectId(column, keyWord, projectId));
		return map;
	}

	@Override
	public List<Normalizedlog> listDetails() throws Exception {
		return null;
	}

}
