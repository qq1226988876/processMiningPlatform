package team.b8311.processMiningPlatform.support.generateEventlog.entity;

/**
 * 库所类
 * 
 * @author qiuto
 *
 */
public class Place {
	private Integer placeId; // 库所编号
	private String placeName; // 库所名称
	private Integer tokenNum; // 托肯数量

	public Place() {
		// 无参构造函数
		this.tokenNum = 0;
	}

	public Place(Integer placeId, String placeName) {
		super();
		this.placeId = placeId;
		this.placeName = placeName;
		this.tokenNum = 0;
	}

	public Place(Integer placeId, String placeName, Integer tokenNum) {
		super();
		this.placeId = placeId;
		this.placeName = placeName;
		this.tokenNum = tokenNum;
	}

	public Integer getPlaceId() {
		return placeId;
	}

	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public Integer getTokenNum() {
		return tokenNum;
	}

	public void setTokenNum(Integer tokenNum) {
		this.tokenNum = tokenNum;
	}

	@Override
	public String toString() {
		return "Place [placeId=" + placeId + ", placeName=" + placeName + ", tokenNum=" + tokenNum + "]";
	}

	/**
	 * 消耗库所中的托肯
	 * 
	 */
	public void consumeToken() {
		this.tokenNum--;
	}

	/**
	 * 生产库所中的托肯
	 */
	public void produceToken() {
		this.tokenNum++;
	}

}
