package team.b8311.processMiningPlatform.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import team.b8311.processMiningPlatform.domain.Normalizedlog;

public interface INormalizedlogService {

	/**
	 * 实现规范化日志的上传操作，本次操作需要调用INormalizedlogDAO接口的以下方法:<br>
	 * <li>需要调用INormalizedlogDAO.findByNameProjectId()方法，判断要添加的日志所属项目是否存在
	 * <li>需要调用INormalizedlogDAO.doCreate()方法，返回操作结果<br>
	 * 调用HDFSUtils.uploadFile()方法，上传规范化日志文件并返回HDFSId
	 * 
	 * @param in
	 *            规范化日志文件流
	 * @param vo
	 *            要上传日志vo对象
	 * @return 如果上传成功，返回true
	 * @throws Exception
	 *             SQL执行异常或者错误信息异常
	 */
	public boolean upload(InputStream in, Normalizedlog vo) throws Exception;

	/**
	 * 执行规范化日志的删除操作，可以删除多个规范化日志信息，调用INormalizedlogDAO的以下方法：<br>
	 * <li>调用INormalizedlogDAO.findByIdDetails()查询规范化日志对应原始日志是否存在<br>
	 * 如果存在则调用IRawlogDAO.findByIdDetails()获取原始日志信息，并调用IRawlogDAO.doUpdate()
	 * 清除表之间的相关信息
	 * <li>调用INormalizedlogDAO.findByIdDetails()查询规范化日志队形的事件日志是否存在<br>
	 * 如果存在则调用用IEventlogDAO.findByIdDetails()获取事件日志信息，并调用IEventlogDAO.doUpdate()
	 * 清除表之间的相关信息
	 * <li>调用HDFSUtils.deleteFile()删除HDFS上的文件，并调用INormalizedlogDAO.doRemoveBatch
	 * ()删除数据
	 * 
	 * @param ids
	 *            包含了要删除的数据集合，没有重复数据
	 * @return 删除成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常或错误信息异常
	 */
	public boolean delete(Set<Integer> ids) throws Exception;

	/**
	 * 通过规范化日志id查询相关内容，调用INormalizedlogDAO.findById()方法
	 * 
	 * @param id
	 *            规范化日志id
	 * @return 如果表中有数据，封装为VO对象返回，否则返回null
	 * @throws Exception
	 *             SQL执行异常
	 * 
	 */
	public Normalizedlog get(int id) throws Exception;

	/**
	 * 通过规范化日志id查询日志的详细信息，调用INormalizedlogDAO.findByIdDetails()方法
	 * 
	 * @param id
	 *            规范化日志id
	 * @return 如果表中有数据，封装为VO对象返回，否则返回null
	 * @throws Exception
	 *             SQL执行异常
	 * 
	 */
	public Normalizedlog getDetails(int id) throws Exception;

	/**
	 * 查询全部的规范化日志信息，调用INormalizedlogDAO.findAllDetails()方法
	 * 
	 * @return 查询结果以List集合的形式返回，如果没有数据则集合的长度为0
	 * @throws Exception
	 *             SQL执行异常
	 */
	public List<Normalizedlog> listDetails() throws Exception;

	/**
	 * 实现数据的模糊查询和数据统计，要调用INormalizedlogDAO接口的两个方法：<br>
	 * <li>调用INormalizedlogDAO.findAllSplitDetails()方法，查询出所有的表数据，返回List<Rawlog>；
	 * <li>调用INormalizedlogDAO.getAllCount()方法，查询出所有的数据量，返回Integer
	 * 
	 * @param currentPage
	 *            当前所在页
	 * @param lineSize
	 *            每页显示的记录数
	 * @param column
	 *            模糊查询的数据列
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projectId
	 *            项目id
	 * @return 本方法需要返回多种数据类型，所以使用Map集合返回，由于类型不同，所有的value的类型置为Object<br>
	 *         <li>key = allNormalizedlogs,value =
	 *         INormalizedlogDAO.findAllSplitDetails()方法返回结果，List
	 *         <Normalizedlog>;
	 *         <li>key = normalizedlogCount,value =
	 *         INormalizedlogDAO.getAllCount()方法返回结果，Integer;
	 * @throws Exception
	 *             SQL执行异常或者错误信息异常
	 */
	public Map<String, Object> listDetails(int currentPage, int lineSize, String column, String keyWord, int projectId)
			throws Exception;
}
