package team.b8311.processMiningPlatform.service.impl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import team.b8311.processMiningPlatform.dao.IEventlogDAO;
import team.b8311.processMiningPlatform.dao.INormalizedlogDAO;
import team.b8311.processMiningPlatform.domain.Eventlog;
import team.b8311.processMiningPlatform.domain.Normalizedlog;
import team.b8311.processMiningPlatform.exception.CustomException;
import team.b8311.processMiningPlatform.service.IEventlogService;
import team.b8311.processMiningPlatform.utils.HDFSUtils;

@Service("EventlogService") // 给业务层起别名纳入到Spring容器
public class EventlogServiceImpl implements IEventlogService {

	@Resource
	private INormalizedlogDAO normalizedlogDAO; // 注入DAO层对象

	@Resource
	private IEventlogDAO eventlogDAO; // 注入DAO层对象

	@Resource
	private HDFSUtils hdfs; // 注入HDFS工具类对象

	@Override
	public boolean upload(InputStream in, Eventlog vo) throws Exception {
		if (eventlogDAO.findByNameProjectId(vo.getEventlogName(), vo.getProject().getProjectId()) != null) {
			throw new CustomException("日志名已存在！");
		} else {
			String hdfsId = hdfs.uploadFile(in, HDFSUtils.EVENTLOG_PATH_PREFIX);
			if (null != hdfsId) {
				vo.setEventlogHDFSId(hdfsId);
				return eventlogDAO.doCreate(vo);
			} else {
				throw new CustomException("上传HDFS失败！");
			}
		}
	}

	@Override
	public boolean delete(Set<Integer> ids) throws Exception {
		Normalizedlog nl = null;
		Eventlog el = null;
		Eventlog el2 = null;
		Iterator it = ids.iterator();
		while (it.hasNext()) {
			Integer deleteId = (Integer) it.next();
			el = eventlogDAO.findByIdLogDetails(deleteId);
			if (el.getNormalizedlog() != null) {// 存在对应的原始日志
				nl = normalizedlogDAO.findByIdDetails(el.getNormalizedlog().getNormalizedlogId());
				nl.setEventlog(null);
				normalizedlogDAO.doUpdate(nl);
			}
			el2 = eventlogDAO.findByIdMergeDetails(deleteId);
			if (el2 != null) {// 该日志是融合日志
				if ((el2.getSourceEvent1() != null) && (el2.getSourceEvent2() != null)) {
					eventlogDAO.doRemoveMerge(el2.getEventlogId());
				}
			}
			hdfs.deleteFile(HDFSUtils.EVENTLOG_PATH_PREFIX + el.getEventlogHDFSId());
		}
		return eventlogDAO.doRemoveBatch(ids);
	}

	@Override
	public Eventlog get(int id) throws Exception {
		return eventlogDAO.findById(id);
	}

	@Override
	public Map<String, Object> listDetails(int currentPage, int lineSize, String column, String keyWord, int projectId)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("allEventlogs",
				eventlogDAO.findAllSplitLogDetails((currentPage - 1) * lineSize, lineSize, column, keyWord, projectId));
		map.put("eventlogCount", eventlogDAO.getAllCountByProjectId(column, keyWord, projectId));
		return map;
	}

	@Override
	public Map<String, Object> listMergeDetails(int currentPage, int lineSize, String column, String keyWord,
			int projectId) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("allMergeEventlogs", eventlogDAO.findAllSplitMergeDetails((currentPage - 1) * lineSize, lineSize,
				column, keyWord, projectId));
		map.put("mergeEventlogCount", eventlogDAO.getAllMergeCountByProjectId(column, keyWord, projectId));
		return map;
	}

	@Override
	public boolean contains(String name, int id) throws Exception {
		return (eventlogDAO.findByNameProjectId(name, id) != null);
	}

}
