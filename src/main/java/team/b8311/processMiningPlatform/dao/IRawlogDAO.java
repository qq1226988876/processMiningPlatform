package team.b8311.processMiningPlatform.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import team.b8311.processMiningPlatform.domain.Rawlog;

public interface IRawlogDAO extends IDAO<Integer, Rawlog> {

	/**
	 * 通过项目id和原始日志名查询项目
	 * 
	 * @param name
	 *            原始日志名称
	 * @param projectId
	 *            项目id
	 * @return 如果表中有数据，则数据封装为VO对象返回<br>
	 *         如果没有数据返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Rawlog findByNameProjectId(@Param("name") String name, @Param("projectId") Integer projectId)
			throws Exception;

	/**
	 * 分页查询原始日志的完整信息
	 * 
	 * @param startRow
	 *            起始的位置
	 * @param lineSize
	 *            所要显示的数据行数
	 * @param column
	 *            要进行模糊查询的数据列
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projectId
	 *            项目id
	 * @return 如果表中有数据，则所有的数据会封装为VO对象而后利用List的集合返回<br>
	 *         如果没有数据，那么集合的长度为0
	 * @throws Exception
	 *             SQL执行异常
	 */
	public List<Rawlog> findAllSplitDetails(@Param("startRow") Integer startRow, @Param("lineSize") Integer lineSize,
			@Param("column") String column, @Param("keyWord") String keyWord, @Param("projectId") Integer projecId)
			throws Exception;

	/**
	 * 按照项目id进行模糊数据量的统计，如果表中没有数据就是0
	 * 
	 * @param column
	 *            要进行模糊查询的数据列
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projectId
	 *            所属的项目id
	 * @return 返回表中的数据量，如果没有数据返回0
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Integer getAllCountByProjectId(@Param("column") String column, @Param("keyWord") String keyWord,
			@Param("projectId") Integer projectId) throws Exception;

	/**
	 * 查询原始日志的详细信息
	 * 
	 * @param id
	 *            要查询的原始日志id
	 * @return 所有的数据以VO对象返回，如果没有则返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Rawlog findByIdDetails(Integer id) throws Exception;

	/**
	 * 根据项目id删除原始日志
	 * 
	 * @param id
	 *            项目id
	 * @return 删除成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean doRemoveByProjectId(Integer id) throws Exception;
}
