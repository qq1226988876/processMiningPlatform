package team.b8311.processMiningPlatform.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import team.b8311.processMiningPlatform.domain.Algorithm;

public interface IAlgorithmDAO extends IDAO<Integer, Algorithm> {
	/**
	 * 实现对算法信息的修改
	 * 
	 * @param vo
	 *            包含了要修改的信息
	 * @return 数据修改成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean doUpdateAlgoInfo(Algorithm vo) throws Exception;
	/**
	 * 设置算法是否可用
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	public boolean doUpdateAlgoAvailable(Algorithm vo) throws Exception;
	public List<Algorithm> findAllSplitAlgo(
			@Param("startRow") Integer startRow,
			@Param("lineSize") Integer lineSize) throws Exception;
	
	/**
	 * 查找所有的算法信息
	 * <br>
	 * 2019.01.10 ysl
	 */
	public List<Algorithm> findAll() throws Exception;
	/**
	 * 获取可用的算法
	 * @return
	 * @throws Exception
	 */
	public List<Algorithm> findAvailable() throws Exception;
	
}
