package team.b8311.processMiningPlatform.utils.request;

/**
 * 平台支持的流程挖掘的图的id
 * 
 * @author qiuto
 *
 */
public interface ImageType {
	// 工作流网
	public static final int PetriNet = 1;

	// 流程图
	public static final int TransitionSystem = 2;

	// 桑基图
	public static final int Sankey = 3;

	// 力导向图
	public static final int ResourceRelation = 4;
}
