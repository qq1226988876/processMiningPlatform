package team.b8311.processMiningPlatform.support.eventlogExport;

import java.io.File;

import org.deckfour.xes.model.XLog;

/**
 * 事件日志的持久化
 * 
 * @author qiuto
 *
 */
public interface IEventlogExport {
	public void convertXLogToXesFile(XLog xlog, File file);
}
