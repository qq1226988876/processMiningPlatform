package team.b8311.processMiningPlatform.controller;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XLogImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.deckfour.xes.out.XesXmlSerializer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import team.b8311.processMiningPlatform.domain.Eventlog;
import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.ResponseObj;
import team.b8311.processMiningPlatform.exception.BaseExceptionHandleAction;
import team.b8311.processMiningPlatform.service.IEventlogService;
import team.b8311.processMiningPlatform.support.generateEventlog.entity.PetriNet;
import team.b8311.processMiningPlatform.support.generateEventlog.operation.PlaceThread;
import team.b8311.processMiningPlatform.support.generateEventlog.operation.TransitionThread;
import team.b8311.processMiningPlatform.support.generateEventlog.operation.Traverse;
import team.b8311.processMiningPlatform.utils.algo.SummarizeTool;
import team.b8311.processMiningPlatform.utils.algo.SummarizeXLog;
import team.b8311.processMiningPlatform.utils.request.GenerateData;

/**
 * 生成事件日志控制器
 * 
 * @author qiuto
 *
 */
@CrossOrigin(origins = "*", maxAge = 3600) // 防止出现跨域问题
@Controller
@RequestMapping("/generateEventlogAction") // 外层地址
public class GenerateEventlogController extends BaseExceptionHandleAction {

	@Resource
	private IEventlogService eventlogService; // 自动载入service

	private ResponseObj responseObj; // Api接口返回对象

	/**
	 * 生成事件日志
	 * 
	 * @param generateData
	 *            生成事件日志的相关数据
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/generate" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8")
	public Object mining(@RequestBody GenerateData generateData, HttpServletRequest req, HttpServletResponse res)
			throws Exception {
		System.out.println(generateData);

		if (eventlogService.contains(generateData.getEventlogName(), generateData.getProjectId())) {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("日志名已存在");
			return responseObj;
		}

		XLog log = new XLogImpl(new XAttributeMapImpl());
		for (int i = 0; i < generateData.getTraceNumber(); i++) {
			System.out.println("第" + i + "次");
			traverse(generateData.getPetriNet(), log, i);
		}

		ByteArrayOutputStream output = new ByteArrayOutputStream(); // 字节缓冲
		new XesXmlSerializer().serialize(log, output);
		InputStream input = new ByteArrayInputStream(output.toByteArray());

		responseObj = new ResponseObj<Eventlog>(); // 返回事件日志相关信息
		Eventlog el = new Eventlog();
		el.setEventlogFormat(new String("xes"));
		el.setEventlogName(generateData.getEventlogName());
		Project p = new Project();
		p.setProjectId(generateData.getProjectId());
		el.setProject(p);
		SummarizeTool st = SummarizeXLog.summarize(log);
		el.setEventlogAverageEvent(st.getAverage());
		el.setEventlogProcessActivityEvent(st.getProcessActivityEvent());
		el.setEventlogTotalEvent(st.getTotalEventNum());
		el.setEventlogTotalInstance(st.getTotalInstanceNum());
		el.setEventlogController(st.getController());
		if (eventlogService.upload(input, el)) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("生成事件日志成功");
			responseObj.setData(eventlogService.get(el.getEventlogId()));
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("生成事件日志失败！");
			return responseObj;
		}
	}

	/**
	 * 遍历一次petri网
	 * 
	 * @param p
	 *            待遍历的petri网
	 * @param log
	 *            将遍历后的trace添加到该日志
	 * @throws Exception
	 */
	public void traverse(PetriNet p, XLog log, int index) throws Exception {
		final CountDownLatch latch = new CountDownLatch(2);

		List<Integer> traversePlaces = new ArrayList<Integer>();
		traversePlaces.add(0);
		XAttributeMap attrMap = new XAttributeMapImpl();
		attrMap.put("concept:name", new XAttributeLiteralImpl("concept:name", String.valueOf(index)));
		XTrace trace = new XTraceImpl(attrMap);

		p.initPetriNet();
		Traverse t = new Traverse(traversePlaces, new ArrayList<Integer>(), new ArrayList<Integer>(), p, trace);
		new Thread(new PlaceThread(t, latch)).start();
		new Thread(new TransitionThread(t, latch)).start();

		latch.await();
		log.add(trace);
	}
}
