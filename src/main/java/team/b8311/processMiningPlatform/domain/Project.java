/**
 * 
 */
package team.b8311.processMiningPlatform.domain;

import java.io.Serializable;
import java.util.List;

/**
 * 创建项目表: <br>
 * CREATE TABLE `project` (<br>
 * `project_id` int(10) unsigned NOT NULL AUTO_INCREMENT<br>
 * `project_name` varchar(30) NOT NULL COMMENT '项目名称',<br>
 * `user_id` int(10) unsigned NOT NULL COMMENT '用户ID',<br>
 * `project_decription` tinytext COMMENT '项目描述',<br>
 * PRIMARY KEY (`project_id``)<br>
 * UNIQUE KEY `user_id` (`user_id`,`project_name`)<br>
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8<br>
 * 
 * @author qiuto
 *
 */
public class Project implements Serializable {
	private Integer projectId; // 项目id
	private String projectName; // 项目名称
	private String projectDescription; // 项目描述
	private User user; // 所属用户

	private List<Rawlog> rawlogs; // 项目的原始日志
	private List<Normalizedlog> normalizedlogs; // 项目的原始日志
	private List<Eventlog> eventlogs; // 项目的原始日志

	public Project() {
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public List<Rawlog> getRawlogs() {
		return rawlogs;
	}

	public void setRawlogs(List<Rawlog> rawlogs) {
		this.rawlogs = rawlogs;
	}

	public List<Normalizedlog> getNormalizedlogs() {
		return normalizedlogs;
	}

	public void setNormalizedlogs(List<Normalizedlog> normalizedlogs) {
		this.normalizedlogs = normalizedlogs;
	}

	public List<Eventlog> getEventlogs() {
		return eventlogs;
	}

	public void setEventlogs(List<Eventlog> eventlogs) {
		this.eventlogs = eventlogs;
	}

	@Override
	public String toString() {
		return "Project [projectId=" + projectId + ", projectName=" + projectName + ", projectDescription="
				+ projectDescription + ", user=" + user + "]";
	}

}
