package team.b8311.processMiningPlatform.support.eventlogParse.impl;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.util.ArrayList;
import java.util.List;

import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XLog;

import team.b8311.processMiningPlatform.support.eventlogParse.IEventlogParse;

public class EventlogParseImpl implements IEventlogParse {

	@SuppressWarnings("unchecked")
	@Override
	public XLog eventLogParse(InputStream in) {
		try {
			//将原有读取的流清空，不清空的话，容易出现解析错误的情况
			in.reset();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		XLog xlog = null;

		// 创建XesParser对象
		XesXmlParser xxp = new XesXmlParser();
		List<XLog> listXLog = new ArrayList<XLog>();
		try {
			listXLog = xxp.parse(in);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 使用XesParser对象解析输入的文件流
		
		if (!listXLog.isEmpty()) {
			xlog = listXLog.get(0);
		}
		return xlog;
	}

	private static InputStream checkForUtf8BOMAndDiscardIfAny(InputStream inputStream) throws IOException {
		PushbackInputStream pushbackInputStream = new PushbackInputStream(new BufferedInputStream(inputStream), 3);
		byte[] bom = new byte[3];
		if (pushbackInputStream.read(bom) != -1) {
			if (!(bom[0] == (byte) 0xEF && bom[1] == (byte) 0xBB && bom[2] == (byte) 0xBF)) {
				pushbackInputStream.unread(bom);
			}
		}
		return pushbackInputStream;
	}

}
