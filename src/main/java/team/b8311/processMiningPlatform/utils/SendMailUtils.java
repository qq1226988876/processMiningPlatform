package team.b8311.processMiningPlatform.utils;

import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sun.mail.util.MailSSLSocketFactory;

public class SendMailUtils {
	private static final String EMAIL = "875457146@qq.com"; // 要发送的邮箱地址
	private static final String PASSWORD = "zvjlodjjpxlxbcef"; // 邮箱密码
	private static final String EMAILSMTPHOST = "smtp.qq.com";

	private Properties properties = new Properties();
	private MailSSLSocketFactory sf = null;
	private String generateCode; // 产生的验证码

	public void setGenerateCode(String generateCode) {
		this.generateCode = generateCode;
	}

	/**
	 * 发送邮件
	 * 
	 * @param email
	 *            邮件主题
	 */
	public void sendMail(String email) {
		this.properties.setProperty("mail.transport.protocol", "smtp");
		this.properties.setProperty("mail.smtp.host", EMAILSMTPHOST);
		this.properties.setProperty("mail.smtp.auth", "true");
		this.properties.setProperty("mail.stmp.starttls.enable", "true");
		try {
			this.sf = new MailSSLSocketFactory();
		} catch (GeneralSecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sf.setTrustAllHosts(true);
		properties.put("mail.smtp.ssl.enable", "true");
		properties.put("mail.smtp.ssl.socketFactory", sf);
		Session session = Session.getInstance(properties);
		session.setDebug(true);
		try {
			MimeMessage message = this.createMimeMessage(session, email);
			// MiniMultipart类是一个容器类，包含MimeBodyPart类型的对象
			Multipart mainPart = new MimeMultipart();
			// 创建一个包含HTML内容的MimeBodyPart
			BodyPart html = new MimeBodyPart();
			// 设置HTML内容
			html.setContent(message.getContent(), "text/html; charset=utf-8");
			mainPart.addBodyPart(html);
			// 将MiniMultipart对象设置为邮件内容
			// message.setContent(mainPart);
			Transport transport = session.getTransport();
			transport.connect(EMAIL, PASSWORD);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getGenerateCode() {
		return generateCode;
	}

	/**
	 * 设置发送邮件的内容
	 * 
	 * @param session
	 *            javax.mail包的session
	 * @param email
	 *            要发送到的邮箱地址
	 * @return
	 * @throws Exception
	 */
	public MimeMessage createMimeMessage(Session session, String email) throws Exception {
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(this.EMAIL, "流程挖掘平台", "UTF-8"));
		// message.addRecipient(Message.RecipientType.CC,new
		// InternetAddress(sendMail, "校园服务平台", "UTF-8"));
		message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(email, "流程挖掘平台", "UTF-8"));
		message.setSubject("流程挖掘平台账户电子邮箱验证", "UTF-8");
		StringBuffer content = new StringBuffer();
		String emailMD5 = MD5Utils.MD5(email);
		content.append("<div class=\"root\">");
		content.append("<div class=\"title\">");
		content.append("<span class=\"title-word\">流程挖掘平台</span>");
		content.append("</div>");
		content.append("<div class=\"center\">");
		content.append("您好，" + email);
		content.append("<br>");
		content.append("<br>");
		content.append("<span>这是您的注册验证码：</span>");

		char[] codeChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456".toCharArray(); // 验证码中所使用到的字符
		StringBuffer makeCodeChar = new StringBuffer(); // 存放产生的验证码
		Random random = new Random();
		for (int i = 0; i < 6; i++) {
			makeCodeChar.append(codeChar[random.nextInt(codeChar.length)]);
		}
		this.setGenerateCode(makeCodeChar.toString());

		content.append("</div>");
		content.append("<div class=\"code\">");
		content.append(makeCodeChar);
		content.append("</div>");
		content.append("<div class=\"footer\">");
		content.append("Copyright &copy;2018 SCUT All Rights Reserved SCUT B8-311 版权所有");
		content.append("</div>");
		content.append("</div>");
		content.append("<style type=\"text/css\">");
		content.append(
				".root {margin-left: auto;margin-right: auto;margin-top: 30px;width: 500px;height: 550px;background-color: #EBEEF5;border-radius: 25px;}");
		content.append(
				".title {width: 100%;height: 60px;border-top-left-radius: 25px;border-top-right-radius: 25px;background-color: #83AF9B;border-bottom: 2px;text-align: center;}");
		content.append(".title-word {font-size: 35px;line-height: 1.5;color: #FFF5F7;}");
		content.append(
				".center {width: 80%;margin-top: 40px;margin-left: auto;margin-right: auto;font-size: 24px;color: #6a7783;}");
		content.append(
				".code {width: 80%;margin-top: 60px;margin-left: auto;margin-right: auto;font-size: 60px;text-align: center;color: #b93465;border-radius: 5px;background-color: #DEB887;}");
		content.append(
				" .footer {height: 50px;margin: 0;padding: 0;line-height: 45px;margin: 0 auto;text-align: center;display: block;width: 100%;font-size: 12px;color: #666;text-align: center;margin-top: 120px;background-color: #83AF9B;border-bottom-right-radius: 25px;border-bottom-left-radius: 25px;}");
		content.append("</style>");
		message.setContent(content.toString(), "text/html;charset=UTF-8");
		message.setSentDate(new Date());
		message.saveChanges();
		return message;
	}
}
