package team.b8311.processMiningPlatform.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import team.b8311.processMiningPlatform.dao.IAlgorithmDAO;
import team.b8311.processMiningPlatform.domain.Algorithm;
import team.b8311.processMiningPlatform.service.IAlgorithmService;
import team.b8311.processMiningPlatform.utils.Config;
import team.b8311.processMiningPlatform.utils.ConfigParser;

@Service("AlgorithmService")
public class AlgorithmServiceImpl implements IAlgorithmService {

	@Resource
	private IAlgorithmDAO algorithmDAO;

	@Override
	public boolean acceptAlgorithm(HttpServletRequest req, MultipartFile[] files, Algorithm al) throws Exception {
		// TODO 自动生成的方法存根
		String jarSavePath = req.getSession().getServletContext().getRealPath("/") + "/AlgorithmJar/";
		String jsonSavePath = req.getSession().getServletContext().getRealPath("/") + "/AlgorithmJson/";
		if (files != null && files.length == 2) {
			// 遍历并保存文件
			File jsonFile;
			JSONObject jsonObject;
			for (MultipartFile file : files) {
				if (Pattern.matches(file.getOriginalFilename(), ".*\\.jar")) {
					al.setAlgorithmPath(jarSavePath + file.getOriginalFilename());
					file.transferTo(new File(jarSavePath + file.getOriginalFilename()));
				} else if (Pattern.matches(file.getOriginalFilename(), ".*\\.json")) {
					jsonFile = new File(jsonSavePath + file.getOriginalFilename());
					String content = FileUtils.readFileToString(jsonFile, "UTF-8");
					jsonObject = new JSONObject(content);
					if (jsonObject.getString("verify") == "Algorithm") { // 验证接收到的json文件
						al.setAlgorithmConfigPath(jsonSavePath + file.getOriginalFilename());
						file.transferTo(jsonFile);
						System.out.println("算法名：" + jsonObject.getString("name"));
						System.out.println("算法类名：" + jsonObject.getDouble("classpath"));
						System.out.println("算法包名：" + jsonObject.getJSONArray("package"));
						al.setAlgorithmName(jsonObject.getString("name"));
						al.setAlgorithmClassName(jsonObject.getString("classpath"));
						al.setAlgorithmPackage(jsonObject.getString("package"));
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
			if (!algorithmDAO.doCreate(al)) {
				return false;
			}
			System.out.println("新建算法id为" + al.getAlgorithmId());
		} else {
			return false;
		}
		return true;
	}

	@Override
	public Algorithm get(int id) throws Exception {
		try {
			return algorithmDAO.findById(id);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean insert(Algorithm algo) throws Exception {
		// TODO 自动生成的方法存根
		return algorithmDAO.doCreate(algo);
	}

	@Override
	public Map<String, Object> list(int currentPage, int lineSize, String column, String keyWord) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("allAlgorithms", algorithmDAO.findAllSplit((currentPage - 1) * lineSize, lineSize, column, keyWord));
		map.put("algorithmCount", algorithmDAO.getAllCount(column, keyWord));
		return map;
	}

	@Override
	public boolean delete(Set<Integer> ids) throws Exception {
		return algorithmDAO.doRemoveBatch(ids);
	}

	@Override
	public Config getAlgoConfig(int algoId) throws Exception {
		Algorithm algo = algorithmDAO.findById(algoId);
		Config config = ConfigParser.parseConfig(algo.getAlgorithmConfigPath());
		return config;
	}

	@Override
	public Map<String, Object> listAll() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("allAlgorithms", algorithmDAO.findAll());
		return map;
	}
	@Override
	public Map<String, Object> listAvailabel() throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("allAlgorithms", algorithmDAO.findAvailable());
		return map;
	}

	@Override
	public boolean updateAlgorithmAvailable(Algorithm vo) throws Exception {
		// TODO Auto-generated method stub
		return algorithmDAO.doUpdateAlgoAvailable(vo);
	}



	// @Override
	// public Miningresult applyAlgorithm() {
	// // TODO 自动生成的方法存根
	// return null;
	// }

}
