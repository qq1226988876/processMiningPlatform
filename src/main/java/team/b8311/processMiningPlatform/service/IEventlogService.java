package team.b8311.processMiningPlatform.service;

import java.io.InputStream;
import java.util.Map;
import java.util.Set;

import team.b8311.processMiningPlatform.domain.Eventlog;

public interface IEventlogService {

	/**
	 * 查询事件日志是否存在，本次操作会调用IEventlogDAO接口的以下方法：<br>
	 * 调用IEventlogDAO.findByNameProjectId()方法,判断要添加的日志所属项目是否存在
	 * 
	 * @param name
	 *            事件日志名称
	 * @param id
	 *            项目id
	 * @return 如果存在返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常或者错误信息异常
	 */
	public boolean contains(String name, int id) throws Exception;

	/**
	 * 实现规范化日志的上传操作，本次操作需要调用IEventlogDAO接口的以下方法：<br>
	 * <li>需要调用IEventlogDAO.findByNameProjectId()方法,判断要添加的日志所属项目是否存在
	 * <li>需要调用IEventlogDAO.doCreate()方法，返回操作结果<br>
	 * 
	 * @param in
	 *            事件日志文件流
	 * @param vo
	 *            要上传日志vo对象
	 * @return 如果上传成功，返回true
	 * @throws Exception
	 *             SQL执行异常或者错误信息异常
	 */
	public boolean upload(InputStream in, Eventlog vo) throws Exception;

	/**
	 * 执行事件日志的删除操作，可以删除多个事件日志信息，调用IEventlogDAO接口的以下方法:<br>
	 * <li>调用IEventlogDAO.findByIdLogDetails()查询事件日志对应规范化日志是否存在<br>
	 * 如果存在则调用INormalizedlogDAO.findByIdDetails()获取规范化日志信息，并调用INormalizedlogDAO.
	 * doUpdate()清除表之间的相关关系
	 * <li>调用IEventlogDAO.findByIdMergeDetails()查询事件日志对应融合日志信息<br>
	 * 如果存在则调用IEventlogDAO.doRemoveMerge()删除融合日志的关联信息
	 * <li>调用HDFSUtils.deleteFile()删除HDFS上的文件，并调用IEventlogDAO.doRemoveBatch()
	 * 删除数据
	 * 
	 * @param ids
	 *            包含了要删除的数据集合，没有重复数据
	 * @return 删除成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常或者错误信息异常
	 */
	public boolean delete(Set<Integer> ids) throws Exception;

	/**
	 * 通过事件日志id查询相关内容，调用IEventlogDAO.findById()方法
	 * 
	 * @param id
	 *            事件日志id
	 * @return 如果表中有数据，封装为VO对象返回，否则返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public Eventlog get(int id) throws Exception;

	/**
	 * 实现数据的模糊查询和数据统计，要调用IEventlogDAO接口的两个方法：<br>
	 * <li>调用IEventlogDAO.findAllSplitLogDetails()方法，查询出所有的表数据，返回List<Rawlog>；
	 * <li>调用IEventlogDAO.getAllCount()方法，查询出所有的数据量，返回Integer
	 * 
	 * @param currentPage
	 *            当前所在页
	 * @param lineSize
	 *            每页显示的记录数
	 * @param column
	 *            模糊查询的数据列
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projectId
	 *            项目id
	 * @return 本方法需要返回多种数据类型，所以使用Map集合返回，由于类型不同，所有的value的类型置为Object<br>
	 *         <li>key = allEventlogs,value =
	 *         IEventDAO.findAllSplitLogDetails()方法返回结果，List <Eventlog>;
	 *         <li>key = EventlogCount,value =
	 *         IEventlogDAO.getAllCount()方法返回结果，Integer;
	 * @throws Exception
	 *             SQL执行异常或者错误信息异常
	 */
	public Map<String, Object> listDetails(int currentPage, int lineSize, String column, String keyWord, int projectId)
			throws Exception;

	/**
	 * 实现融合日志的模糊查询和数据统计，要调用IEventlogDAO接口的两个方法：<br>
	 * <li>调用IEventlogDAO.findAllSplitMergeDetails()方法，查询出融合事件相关信息，返回List
	 * <Eventlog>;
	 * <li>调用IEventlogDAO.getAllMergeCount()方法，查询出所有的数据量，返回Integer
	 * 
	 * @param currentPage
	 *            当前所在页
	 * @param lineSize
	 *            每页查询的数据列
	 * @param column
	 *            模糊查询的数据列
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projectId
	 *            项目id
	 * @return 本方法需要返回多种数据类型，所以使用Map集合返回，由于类型不同，所有的value的类型置为Object<br>
	 *         <li>key = allMergeEventlogs,value =
	 *         IEventDAO.findAllSplitMergeDetails()方法返回结果，List <Eventlog>;
	 *         <li>key = mergeEventlogCount,value =
	 *         IEventlogDAO.getAllMergeCount()方法返回结果，Integer;
	 * @throws Exception
	 *             SQL执行异常，或者错误信息异常
	 */
	public Map<String, Object> listMergeDetails(int currentPage, int lineSize, String column, String keyWord,
			int projectId) throws Exception;

}
