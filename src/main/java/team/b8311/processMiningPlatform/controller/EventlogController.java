package team.b8311.processMiningPlatform.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.deckfour.xes.model.XLog;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import sun.tools.jar.resources.jar;
import team.b8311.processMiningPlatform.domain.Algorithm;
import team.b8311.processMiningPlatform.domain.Eventlog;
import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.ResponseObj;
import team.b8311.processMiningPlatform.exception.BaseExceptionHandleAction;
import team.b8311.processMiningPlatform.exception.CustomException;
import team.b8311.processMiningPlatform.service.IAlgorithmService;
import team.b8311.processMiningPlatform.service.IEventlogService;
import team.b8311.processMiningPlatform.support.eventlogParse.IEventlogParse;
import team.b8311.processMiningPlatform.support.eventlogParse.impl.EventlogParseImpl;
import team.b8311.processMiningPlatform.utils.HDFSUtils;
import team.b8311.processMiningPlatform.utils.algo.MergeUtil;
import team.b8311.processMiningPlatform.utils.algo.SummarizeTool;
import team.b8311.processMiningPlatform.utils.algo.SummarizeXLog;

@Controller
@RequestMapping("/eventlogAction") // 外层地址
public class EventlogController extends BaseExceptionHandleAction {

	@Resource
	private IEventlogService eventlogService; // 自动载入service

	@Resource
	private IAlgorithmService algorithmService; // 自动载入Service

	@Resource
	private MergeUtil mergeUtil; // 自动载入service

	@Resource
	private HDFSUtils hdfs; // 自动载入操作HDFS对象

	private ResponseObj responseObj; // Api接口返回对象

	/**
	 * 上传事件日志接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param file
	 *            上传的事件日志文件
	 * @param format
	 *            上传的事件日志格式
	 * @param projectId
	 *            规范化日志所属的项目id
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/upload" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object upload(HttpServletRequest req, HttpServletResponse res, MultipartFile file, String format,
			int projectId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<Eventlog>();
		if (file.getSize() == 0) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("规范化日志大小为0！");
			return responseObj;
		}

		Eventlog el = new Eventlog();
		el.setEventlogFormat(format);
		el.setEventlogName(file.getOriginalFilename());
		Project p = new Project();
		p.setProjectId(projectId);
		el.setProject(p);
		InputStream input = file.getInputStream();
		IEventlogParse parser = new EventlogParseImpl();
		XLog xlog = parser.eventLogParse(input);
		SummarizeTool st = SummarizeXLog.summarize(xlog);
		el.setEventlogAverageEvent(st.getAverage());
		el.setEventlogProcessActivityEvent(st.getProcessActivityEvent());
		el.setEventlogTotalEvent(st.getTotalEventNum());
		el.setEventlogTotalInstance(st.getTotalInstanceNum());
		el.setEventlogController(st.getController());
		if (eventlogService.upload(file.getInputStream(), el)) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("上传事件日志成功");
			responseObj.setData(eventlogService.get(el.getEventlogId()));
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("上传事件日志失败！");
			return responseObj;
		}
	}

	/**
	 * 下载事件日志文件
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param rawlogId
	 *            要下载的事件日志id
	 */
	@RequestMapping(value = "/download" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	public void download(HttpServletRequest req, HttpServletResponse res, int eventlogId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码

		Eventlog el = null;
		if ((el = eventlogService.get(eventlogId)) == null) {
			throw new CustomException("事件日志不存在");
		} else {
			InputStream in = new BufferedInputStream(
					hdfs.downloadFile(HDFSUtils.EVENTLOG_PATH_PREFIX + el.getEventlogHDFSId()));

			res.addHeader("Content-Disposition",
					"attachment;filename=" + new String(el.getEventlogName().getBytes("utf-8"), "iso-8859-1")); // 文件下载时获取文件名时的编码格式为iso-8859-1
			res.setContentType("multipart/form-data");

			OutputStream out = new BufferedOutputStream(res.getOutputStream());
			int temp = 0; // 表示每次读取的字节数据
			while ((temp = in.read()) != -1) {
				out.write(temp);
				out.flush();
			}
			out.close();
		}
	}

	/**
	 * 删除事件日志接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param ids
	 *            要删除的事件日志id数组
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/delete" // 内层地址
			, method = RequestMethod.GET// 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object delete(HttpServletRequest req, HttpServletResponse res, @RequestParam("ids") Set<Integer> ids)
			throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常

		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码

		System.out.println(ids);

		responseObj = new ResponseObj<Object>();
		if (ids.size() == 0) {
			responseObj.setCode(ResponseObj.EMPTY);
			responseObj.setMsg("请选择要删除的日志");
			return responseObj;
		}

		if (eventlogService.delete(ids)) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("删除日志成功！");
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("删除日志失败！");
			return responseObj;
		}
	}

	/**
	 * 获取事件日志日志信息接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param currentPage
	 *            当前所在页
	 * @param lineSize
	 *            每页显示的记录数
	 * @param keyWord
	 *            模糊查询的关键字
	 * @param projectId
	 *            项目id
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/list" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object list(HttpServletRequest req, HttpServletResponse res, int currentPage, int lineSize, String keyWord,
			int projectId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<Map>();

		Map<String, Object> map = eventlogService.listDetails(currentPage, lineSize, "eventlog_name", keyWord,
				projectId);
		if (null != map) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("获取规范化日志信息成功");
			responseObj.setData(map);
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("获取规范化日志信息失败！");
			return responseObj;
		}
	}

	/**
	 * 融合事件日志接口
	 * 
	 * @param req
	 *            http请求
	 * @param res
	 *            http相应
	 * @param el1Id
	 *            原事件日志1id
	 * @param el2Id
	 *            原事件日志2id
	 * @param algoId
	 *            融合算法id
	 * @param projectId
	 *            项目id
	 * @return 结果信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/merge" // 内层地址
			, method = RequestMethod.POST // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object merge(HttpServletRequest req, HttpServletResponse res, int el1Id, int el2Id, int algoId,
			int projectId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<Eventlog>();

		Eventlog el1 = eventlogService.get(el1Id);
		Eventlog el2 = eventlogService.get(el2Id);
		Algorithm algo = algorithmService.get(algoId);
		System.out.println(algo);
		// algo.setAlgorithmId(algoId);
		// algo.setAlgorithmPath("F:\\study\\process mining\\jars\\AIA.jar");
		// algo.setAlgorithmPackage("net.ProcessMining.logMerge.aia");
		// algo.setAlgorithmClassName("AIAMergerPlugin");
		// algo.setAlgorithmMethod("merge");
		Eventlog result = mergeUtil.merge(el1, el2, algo, projectId);
		responseObj.setCode(ResponseObj.OK);
		responseObj.setMsg("融合日志成功");
		responseObj.setData(result);
		return responseObj;
	}

	@RequestMapping(value = "/listMerge" // 内层地址
			, method = RequestMethod.GET // 限定请求方式
			, produces = "application/json;charset=utf-8") // 设置返回值是json类型,返回结果要求用utf-8方式解码
	@ResponseBody
	public Object listMerge(HttpServletRequest req, HttpServletResponse res, int currentPage, int lineSize,
			String keyWord, int projectId) throws Exception {
		res.setHeader("Access-Control-Allow-Origin", "*"); // 防止出现跨域访问异常
		res.setCharacterEncoding("UTF-8"); // 防止测试接口乱码
		responseObj = new ResponseObj<Map>();

		Map<String, Object> map = eventlogService.listMergeDetails(currentPage, lineSize, "eventlog_name", keyWord,
				projectId);
		if (null != map) {
			responseObj.setCode(ResponseObj.OK);
			responseObj.setMsg("获取规范化日志信息成功");
			responseObj.setData(map);
			return responseObj;
		} else {
			responseObj.setCode(ResponseObj.FAILED);
			responseObj.setMsg("获取规范化日志信息失败！");
			return responseObj;
		}
	}

}
