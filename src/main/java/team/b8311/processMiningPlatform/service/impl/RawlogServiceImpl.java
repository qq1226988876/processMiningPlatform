package team.b8311.processMiningPlatform.service.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import team.b8311.processMiningPlatform.dao.INormalizedlogDAO;
import team.b8311.processMiningPlatform.dao.IProjectDAO;
import team.b8311.processMiningPlatform.dao.IRawlogDAO;
import team.b8311.processMiningPlatform.domain.Normalizedlog;
import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.Rawlog;
import team.b8311.processMiningPlatform.exception.CustomException;
import team.b8311.processMiningPlatform.service.IRawlogService;
import team.b8311.processMiningPlatform.support.normalize.FormatInfo;
import team.b8311.processMiningPlatform.support.normalize.LogConfiguration;
import team.b8311.processMiningPlatform.support.normalize.TempLine;
import team.b8311.processMiningPlatform.utils.FileReadLine;
import team.b8311.processMiningPlatform.utils.HDFSUtils;

@Service("RawlogService") // 给业务层类起别名纳入到Spring容器
public class RawlogServiceImpl implements IRawlogService {

	@Resource
	private IRawlogDAO rawlogDAO; // 注入DAO层对象
	@Resource
	private IProjectDAO projectDAO; // 注入DAO层对象
	@Resource
	private INormalizedlogDAO normalizedlogDAO; // 注入DAO层对象
	@Resource
	private HDFSUtils hdfs; // 注入HDFS工具类对象

	@Override
	public boolean insert(Rawlog rawlog) throws Exception {
		if (projectDAO.findById(rawlog.getProject().getProjectId()) == null) {
			throw new CustomException("日志所属的项目id不存在！");
		} else if (rawlogDAO.findByNameProjectId(rawlog.getRawlogName(), rawlog.getProject().getProjectId()) != null) {
			throw new CustomException("日志名已存在！");
		} else {
			if (rawlogDAO.doCreate(rawlog)) {
				System.out.println("原始日志id为" + rawlog.getRawlogId());
				return true;
			}
			return false;
		}
	}

	@Override
	public Rawlog get(int id) throws Exception {
		try {
			return rawlogDAO.findById(id);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean upload(InputStream in, Rawlog vo) throws Exception {
		if (rawlogDAO.findByNameProjectId(vo.getRawlogName(), vo.getProject().getProjectId()) != null) {
			throw new CustomException("日志名已存在！");
		} else {
			String hdfsId = hdfs.uploadFile(in, HDFSUtils.RAWLOG_PATH_PREFIX);
			vo.setRawlogHDFSId(hdfsId);
			if (rawlogDAO.doCreate(vo)) {
				System.out.println("原始日志id为" + vo.getRawlogId());
				return true;
			}
			return false;
		}
	}

	@Override
	public boolean delete(Set<Integer> ids) throws Exception {
		Rawlog rl = null;
		Normalizedlog nl = null;
		Iterator it = ids.iterator();
		while (it.hasNext()) {
			rl = rawlogDAO.findByIdDetails((Integer) it.next());
			if (rl == null) {
				throw new CustomException("要删除的原始日志不存在");
			} else if (rl.getNormalizedlog() != null) { // 存在与原始日志相关的规范化日志文件
				System.out.println(rl);
				nl = normalizedlogDAO.findByIdDetails(rl.getNormalizedlog().getNormalizedlogId());
				nl.setRawlog(null);
				normalizedlogDAO.doUpdate(nl);
			}
			hdfs.deleteFile(HDFSUtils.RAWLOG_PATH_PREFIX + rl.getRawlogHDFSId());
		}
		if (rawlogDAO.doRemoveBatch(ids)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Map<String, Object> listDetails(int currentPage, int lineSize, String column, String keyWord, int projectId)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("allRawlogs",
				rawlogDAO.findAllSplitDetails((currentPage - 1) * lineSize, lineSize, column, keyWord, projectId));
		map.put("rawlogCount", rawlogDAO.getAllCountByProjectId(column, keyWord, projectId));
		return map;
	}

	@Override
	public boolean convertToNormalizelog(int id, String formats, String timeNames, String targetTimeName,
			String dataNames, String oriitemSeparator, String orinameValSeparator, String orinulVal,
			String targetitemSeparator, String targetnameValSeparator, String targetnulVal, Normalizedlog nl)
			throws Exception {
		boolean result = false;

		// 获得原始日志文件流
		InputStream input = hdfs.downloadFile(HDFSUtils.RAWLOG_PATH_PREFIX + this.get(id).getRawlogHDFSId());

		// BufferedReader reader = new BufferedReader(new
		// InputStreamReader(input, "utf-8"));
		// String line2 ;
		// while((line2 = reader.readLine())!=null){
		// System.out.println(line2);
		// }

		// 总共写入条数
		int ret = 0;

		String nulVal = " ";
		String seperator = "\t";

		// 规范化日志buffer
		StringBuilder normLogBuffer = new StringBuilder();
		if (input != null) {
			FileReadLine readLine = new FileReadLine(input);
			LogConfiguration LC = new LogConfiguration(formats, timeNames, targetTimeName, dataNames, oriitemSeparator,
					orinameValSeparator, orinulVal);
			while (readLine.hasNext()) {
				String line = readLine.getLine();
				if (!line.equals("")) {
					// 将一行录入TempLine
					TempLine tempLine = null;
					if (LC.getNameValSeparator() == null)
						tempLine = new TempLine(line, LC.getItemSeparator(), LC.getNulVal());
					else
						tempLine = new TempLine(line, LC.getNameValSeparator(), LC.getItemSeparator(), LC.getNulVal());

					// 第一步：对所有数据项格式化
					FormatInfo[] tempFormatInfos = LC.getFormatInfos();
					Map<String, String> itemMap = tempLine.getItemMap();
					for (int i = 0; i < tempFormatInfos.length; i++) {
						FormatInfo tempFormatInfo = tempFormatInfos[i];
						String temp = itemMap.get(tempFormatInfo.getItemNameOrIndex());
						/*
						 * System.out.println(
						 * "start-------------------------------------------------------------"
						 * );
						 * System.out.println(tempFormatInfo.getItemNameOrIndex(
						 * )); System.out.println(temp); System.out.println(
						 * "end-------------------------------------------------------------"
						 * );
						 */
						temp = tempFormatInfos[i].formatTransform(temp,
								itemMap.get(tempFormatInfos[i].getFormatTypeNameOrIndex()));
						/*
						 * System.out.println(
						 * "start**************************************************************"
						 * ); System.out.println(temp); for (String key :
						 * tempLine.getItemMap().keySet()) {
						 * 
						 * System.out.println("key= "+ key + " and value= " +
						 * tempLine.getItemMap().get(key));
						 * 
						 * } System.out.println(tempFormatInfos[i].
						 * getFormatTypeNameOrIndex());
						 * System.out.println(tempLine.getItemMap().get(
						 * tempFormatInfos[i].getFormatTypeNameOrIndex()));
						 * System.out.println(
						 * "end**************************************************************"
						 * );
						 */
						tempLine.modifyValue(tempFormatInfo.getItemNameOrIndex(), temp);
					}

					// 第二步：合并时间项
					tempLine.mergeTimeItems(LC.getTargetTimeName(), LC.getTimeItems());

					// 第三步：整合其他项
					tempLine.renameORmerge(LC.getRenameORmergeItems());

					// 第四步：写入新日志
					if (ret == 0) {
						normLogBuffer.append(tempLine.generateItemNamesLine(seperator) + "\r\n");// 写入文件首行
						// System.out.println(tempLine.generateItemNamesLine(seperator)+"\r\n");
					}
					normLogBuffer.append(tempLine.generateNewLine(seperator, nulVal) + "\r\n");
					// System.out.println(tempLine.generateNewLine(seperator,
					// nulVal)+"\r\n");

					ret++;// 记录总共写入多少条数据
				}
			}
		}

		// 生成规范化日志名字
		Rawlog rl = rawlogDAO.findByIdDetails(id);
		// 如果rl存在规范化日志则删除之
		if (rl.getNormalizedlog() != null) {
			Set<Integer> ids = new HashSet<Integer>();
			ids.add(rl.getNormalizedlog().getNormalizedlogId());
			hdfs.deleteFile(HDFSUtils.NORMALIZEDLOG_PATH_PREFIX
					+ normalizedlogDAO.findById(rl.getNormalizedlog().getNormalizedlogId()).getNormalizedlogHDFSId());
			normalizedlogDAO.doRemoveBatch(ids);
		}
		System.out.println(rl);
		String rawlogName = rl.getRawlogName();
		String[] arr = rawlogName.split("\\.");
		StringBuilder normlogNameSB = new StringBuilder();
		normlogNameSB.append(arr[0]);
		normlogNameSB.append("_" + rl.getRawlogId());
		normlogNameSB.append("_norm.");
		normlogNameSB.append(arr[1]);
		String normlogName = normlogNameSB.toString();

		// 往hdfs写规范化日志
		InputStream normLogInput = new ByteArrayInputStream(normLogBuffer.toString().getBytes("utf-8"));
		String normalizedLogHDFSID = hdfs.uploadFile(normLogInput, hdfs.NORMALIZEDLOG_PATH_PREFIX);
		if (normalizedLogHDFSID != null) {
			// 向数据库添加规范化日志记录
			nl.setNormalizedlogFormat(rl.getRawlogFormat());
			nl.setNormalizedlogHDFSId(normalizedLogHDFSID);
			nl.setNormalizedlogName(normlogName);
			Project p = new Project();
			p.setProjectId(rl.getProject().getProjectId());
			nl.setProject(p);
			nl.setRawlog(rl);
			if (normalizedlogDAO.doCreate(nl)) {
				System.out.println("规范化日志id为" + nl.getNormalizedlogId());
				Normalizedlog newnl = new Normalizedlog();
				newnl.setNormalizedlogId(nl.getNormalizedlogId());
				rl.setNormalizedlog(newnl);
				rawlogDAO.doUpdate(rl);
				result = true;
			}
		}
		return result;
	}

	// @Override
	// public boolean deleteByProjectId(int id) throws Exception {
	// return rawlogDAO.doRemoveByProjectId(id);
	// }

	// @Override
	// public InputStream download(int id) throws Exception {
	// Rawlog rl = null;
	// if ((rl = rawlogDAO.findById(id)) == null) {
	// throw new CustomException("原始日志文件不存在");
	// } else {
	// return new HDFSUtils().downloadFile(HDFSUtils.RAWLOG_PATH_PREFIX +
	// rl.getRawlogHDFSId());
	// }
	// }

}
