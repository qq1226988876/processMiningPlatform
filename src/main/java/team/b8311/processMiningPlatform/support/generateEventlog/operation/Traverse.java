package team.b8311.processMiningPlatform.support.generateEventlog.operation;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XAttributeTimestampImpl;
import org.deckfour.xes.model.impl.XEventImpl;

import team.b8311.processMiningPlatform.support.generateEventlog.entity.PetriNet;
import team.b8311.processMiningPlatform.support.generateEventlog.entity.Transition;

/**
 * 遍历类
 * 
 * @author qiuto
 *
 */
public class Traverse {
	private List<Integer> places; // 待遍历库所集
	private List<Integer> enabledTransitions; // 使能变迁集
	private List<Integer> unenabledTransitions; // 不使能变迁集
	private PetriNet petriNet; // 需要遍历的petri网

	private XTrace trace; // 此次遍历生成的trace

	private boolean end; // 标志

	public Traverse() {
		// 无参构造方法
	}

	public Traverse(List<Integer> places, List<Integer> enabledTransitions, List<Integer> unenabledTransitions,
			PetriNet petriNet, XTrace trace) {
		super();
		this.places = places;
		this.enabledTransitions = enabledTransitions;
		this.unenabledTransitions = unenabledTransitions;
		this.petriNet = petriNet;
		this.trace = trace;
		this.end = false;
	}

	public List<Integer> getPlaces() {
		return places;
	}

	public void setPlaces(List<Integer> places) {
		this.places = places;
	}

	public boolean isEnd() {
		return end;
	}

	public void setEnd(boolean end) {
		this.end = end;
	}

	/**
	 * 向使能变迁集中添加变迁
	 * 
	 * @param transitionId
	 *            需要添加的变迁编号
	 */
	public void addEnabledTransition(Integer transitionId) {
		this.enabledTransitions.add(transitionId);
	}

	/**
	 * 从使能变迁集中移除变迁
	 * 
	 * @param transitionId
	 *            需要移除的变迁编号
	 */
	public void removeEnabledTransition(Integer transitionId) {
		this.enabledTransitions.remove(transitionId);
	}

	/**
	 * 向不使能变迁集中添加变迁
	 * 
	 * @param transitionId
	 *            需要添加的变迁编号
	 */
	public void addUnenabledTransition(Integer transitionId) {
		this.unenabledTransitions.add(transitionId);
	}

	/**
	 * 从不使能变迁集中移除变迁
	 * 
	 * @param transitionId
	 *            需要移除的变迁编号
	 */
	public void removeUnenabledTransition(Integer transitionId) {
		this.unenabledTransitions.remove(transitionId);
	}

	/**
	 * 向待遍历库所集中添加库所
	 * 
	 * @param placeId
	 *            需要添加的库所编号
	 */
	public void addPlace(Integer placeId) {
		this.places.add(placeId);
	}

	/**
	 * 从待遍历库所集中删除库所
	 * 
	 * @param placeId
	 *            需要添加的库所编号
	 */
	public void removePlace(Integer placeId) {
		this.places.remove(placeId);
	}

	/**
	 * 遍历库所
	 */
	public synchronized void traversePlace() {
		if (this.places.size() == 0) {
			try {
				super.wait();
			} catch (InterruptedException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}
		int placeId = this.places.get(new Random().nextInt(this.places.size())); // 随机获取要遍历的库所编号
		// System.out.println("库所：" +
		// this.petriNet.getPlace(placeId).getPlaceName());
		List<Integer> nextTransitions = this.petriNet.nextNodes(placeId);
		if (nextTransitions.size() == 0) {
			this.end = true;
			super.notify();
			return;
		}
		Integer chosenTransition = nextTransitions.get(new Random().nextInt(nextTransitions.size())); // 随机选择当前库所的后继变迁作为待遍历变迁
		// System.out.println("变迁：" + chosenTransition);
		if (this.petriNet.isenabled(chosenTransition)) { // 如果选择的变迁是使能的
			if (!this.enabledTransitions.contains(chosenTransition)) { // 使能变迁集中不存在存在该变迁
				this.addEnabledTransition(chosenTransition);
			}
			if (this.unenabledTransitions.contains(chosenTransition)) { // 不使能变迁集中存在该变迁
				this.removeUnenabledTransition(chosenTransition);
			}
		} else { // 选择的变迁是不使能的
			if (!this.unenabledTransitions.contains(chosenTransition)) { // 不使能变迁集中不存在该变迁
				this.addUnenabledTransition(chosenTransition);
			}
		}
		this.removePlace(placeId); // 遍历完成删除当前库所
		super.notify();
	}

	/**
	 * 遍历变迁
	 */
	public synchronized void traverseTransition() {
		while (this.enabledTransitions.size() == 0) {
			try {
				super.wait();
				if (this.end) {
					return;
				}
			} catch (InterruptedException e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}
		}
		System.out.println("使能变迁集合" + this.enabledTransitions);
		Integer transition = this.enabledTransitions.get(new Random().nextInt(this.enabledTransitions.size())); // 随机获取要遍历的变迁编号

		this.petriNet.tranformToken(transition); // 消耗并生产托肯

		// 往事件日志中添加事件
		Transition t = petriNet.getTransition(transition);
		// System.out.println("变迁：" + t);
		if (!t.getTransitionName().equals("Tau")) {
			XAttributeMap attrMap = new XAttributeMapImpl();
			attrMap.put("concept:name", new XAttributeLiteralImpl("concept:name", t.getTransitionName()));
			attrMap.put("time:timestamp", new XAttributeTimestampImpl("time:timestamp", new Date()));
			attrMap.put("lifecycle:transition", new XAttributeLiteralImpl("lifecycle:transition", "complete"));
			XEvent event = new XEventImpl(attrMap);
			this.trace.add(event);
		}

		// 删除当前变迁，并添加新的库所
		List<Integer> places = petriNet.nextNodes(transition);
		Iterator<Integer> iter = places.iterator();
		while (iter.hasNext()) {
			this.addPlace(iter.next());
		}
		this.removeEnabledTransition(transition);
		super.notify();
	}

}
