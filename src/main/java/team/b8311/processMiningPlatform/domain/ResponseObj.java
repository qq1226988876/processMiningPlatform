package team.b8311.processMiningPlatform.domain;

public class ResponseObj<T> {
	public final static int OK = 1,  FAILED= -1, EMPTY = 0;
	public final static String OK_STR = "成功", FAILED_STR = "失败", EMPTY_STR = "数据为空";
	
	private int code; // 状态码，1成功;0空数据;-1请求失败״
	private String msg;
	private T data;

	public int getCode() {
		return this.code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return this.msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
