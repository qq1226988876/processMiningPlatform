package team.b8311.processMiningPlatform.support.transToEvent.service;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import team.b8311.processMiningPlatform.support.transToEvent.entity.Log4Normal;

public interface ITransToEventService {
	/**
	 * 通过规范化日志的名字，封装规范化日志对象Log4Normal
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Log4Normal getLog(Integer id) throws Exception;

	/**
	 * 计算关联关系
	 * 
	 * @param log
	 * @param path
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	public File runEC(Log4Normal log, String path) throws IOException, ParseException;
}
