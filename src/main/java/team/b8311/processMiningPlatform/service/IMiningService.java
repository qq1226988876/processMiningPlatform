package team.b8311.processMiningPlatform.service;

import java.util.Map;

import org.deckfour.xes.model.XLog;

import team.b8311.processMiningPlatform.domain.Algorithm;
import team.b8311.processMiningPlatform.utils.MiningResult;
import team.b8311.processMiningPlatform.utils.request.MiningData;

public interface IMiningService {
	MiningResult logMine(XLog eventLog, Algorithm algo, Map<String, Object> map,
			Integer imageType) throws Exception;
}
