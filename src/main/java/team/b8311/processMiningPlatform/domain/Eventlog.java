package team.b8311.processMiningPlatform.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 创建事件日志表：<br>
 * CREATE TABLE `eventlog` (<br>
 * `eventlog_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '事件日志id',<br>
 * `eventlog_name` varchar(50) NOT NULL COMMENT '事件日志名',<br>
 * `eventlog_format` varchar(10) NOT NULL COMMENT '事件日志格式',<br>
 * `eventlog_createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE
 * CURRENT_TIMESTAMP COMMENT '事件日志创建时间',<br>
 * `eventlog_hdfs_id` varchar(50) NOT NULL COMMENT 'HDFS文件id',<br>
 * `eventlog_controller` tinytext NOT NULL COMMENT '流程活动操作人',<br>
 * `eventlog_total_instance` int(10) unsigned NOT NULL COMMENT '总实例数',<br>
 * `eventlog_total_event` int(10) unsigned NOT NULL COMMENT '总事件数',<br>
 * `eventlog_average_event` int(10) unsigned NOT NULL COMMENT '平均每实例中事件数',<br>
 * `eventlog_process_activity_event` tinytext NOT NULL COMMENT '流程活动事件',<br>
 * `normalizedlog_id` int(10) unsigned DEFAULT NULL COMMENT '规范化日志id',<br>
 * `project_id` int(10) unsigned DEFAULT NULL COMMENT '项目id',<br>
 * PRIMARY KEY (`eventlog_id`),<br>
 * UNIQUE KEY `eventlog_name` (`eventlog_name`),<br>
 * KEY `IDX_EVENTLOG_NORMALIZEDLOG_ID` (`normalizedlog_id`),<br>
 * KEY `IDX_EVENTLOG_PROJECT_ID` (`project_id`)<br>
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 * 
 * @author qiuto
 *
 */
public class Eventlog implements Serializable {
	private Integer eventlogId; // 事件日志id
	private String eventlogName; // 事件日志名
	private String eventlogFormat; // 事件日志文件格式
	private Date eventlogCreatetime; // 事件日志创建时间
	private String eventlogHDFSId; // HDFS文件id
	private String eventlogController; // 流程活动操作人
	private Integer eventlogTotalInstance; // 总实例数
	private Integer eventlogTotalEvent; // 总事件数
	private Integer eventlogAverageEvent; // 平均每实例中事件数
	private String eventlogProcessActivityEvent; // 流程活动事件

	private Project project; // 所属的项目
	private Normalizedlog normalizedlog; // 对应的规范化日志
	private Eventlog sourceEvent1; // 对应的原始日志
	private Eventlog sourceEvent2; // 对应的原始日志
	// private Eventlog targetEvent; // 对应的目标日志
	private Algorithm algorithm; // 融合使用的算法

	public Eventlog() {
		// 无参构造方法
	}

	public Integer getEventlogId() {
		return eventlogId;
	}

	public void setEventlogId(Integer eventlogId) {
		this.eventlogId = eventlogId;
	}

	public String getEventlogName() {
		return eventlogName;
	}

	public void setEventlogName(String eventlogName) {
		this.eventlogName = eventlogName;
	}

	public String getEventlogFormat() {
		return eventlogFormat;
	}

	public void setEventlogFormat(String eventlogFormat) {
		this.eventlogFormat = eventlogFormat;
	}

	public Date getEventlogCreatetime() {
		return eventlogCreatetime;
	}

	public void setEventlogCreatetime(Date eventlogCreatetime) {
		this.eventlogCreatetime = eventlogCreatetime;
	}

	public String getEventlogHDFSId() {
		return eventlogHDFSId;
	}

	public void setEventlogHDFSId(String eventlogHDFSId) {
		this.eventlogHDFSId = eventlogHDFSId;
	}

	public String getEventlogController() {
		return eventlogController;
	}

	public void setEventlogController(String eventlogController) {
		this.eventlogController = eventlogController;
	}

	public Integer getEventlogTotalInstance() {
		return eventlogTotalInstance;
	}

	public void setEventlogTotalInstance(Integer eventlogTotalInstance) {
		this.eventlogTotalInstance = eventlogTotalInstance;
	}

	public Integer getEventlogTotalEvent() {
		return eventlogTotalEvent;
	}

	public void setEventlogTotalEvent(Integer eventlogTotalEvent) {
		this.eventlogTotalEvent = eventlogTotalEvent;
	}

	public Integer getEventlogAverageEvent() {
		return eventlogAverageEvent;
	}

	public void setEventlogAverageEvent(Integer eventlogAverageEvent) {
		this.eventlogAverageEvent = eventlogAverageEvent;
	}

	public String getEventlogProcessActivityEvent() {
		return eventlogProcessActivityEvent;
	}

	public void setEventlogProcessActivityEvent(String eventlogProcessActivityEvent) {
		this.eventlogProcessActivityEvent = eventlogProcessActivityEvent;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Normalizedlog getNormalizedlog() {
		return normalizedlog;
	}

	public void setNormalizedlog(Normalizedlog normalizedlog) {
		this.normalizedlog = normalizedlog;
	}

	public Eventlog getSourceEvent1() {
		return sourceEvent1;
	}

	public void setSourceEvent1(Eventlog sourceEvent1) {
		this.sourceEvent1 = sourceEvent1;
	}

	public Eventlog getSourceEvent2() {
		return sourceEvent2;
	}

	public void setSourceEvent2(Eventlog sourceEvent2) {
		this.sourceEvent2 = sourceEvent2;
	}

	public Algorithm getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(Algorithm algorithm) {
		this.algorithm = algorithm;
	}

	@Override
	public String toString() {
		return "Eventlog [eventlogId=" + eventlogId + ", eventlogName=" + eventlogName + ", eventlogFormat="
				+ eventlogFormat + ", eventlogCreatetime=" + eventlogCreatetime + ", eventlogHDFSId=" + eventlogHDFSId
				+ ", eventlogController=" + eventlogController + ", eventlogTotalInstance=" + eventlogTotalInstance
				+ ", eventlogTotalEvent=" + eventlogTotalEvent + ", eventlogAverageEvent=" + eventlogAverageEvent
				+ ", eventlogProcessActivityEvent=" + eventlogProcessActivityEvent + ", project=" + project
				+ ", normalizedlog=" + normalizedlog + ", sourceEvent1=" + sourceEvent1 + ", sourceEvent2="
				+ sourceEvent2 + ", algorithm=" + algorithm + "]";
	}

}
