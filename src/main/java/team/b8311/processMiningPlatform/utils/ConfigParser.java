package team.b8311.processMiningPlatform.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ConfigParser {
	public static Config parseConfig(String path) {
		String strResult = "";
		ObjectMapper objectMapper = new ObjectMapper();
		Config config = null;
		try {
			JsonNode rootNode = objectMapper.readTree(new File(path));
			strResult = rootNode.toString();
			config = objectMapper.readValue(strResult, Config.class);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return config;
	}

	public static void main(String[] args) {
		Config config = ConfigParser.parseConfig("F:\\config.json");
		List<Param> p = config.getParams();
		for (Param pp : p) {
			System.out.println(pp.getParameterName());
		}
	}
}
