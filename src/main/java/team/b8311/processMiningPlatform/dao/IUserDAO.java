package team.b8311.processMiningPlatform.dao;

import java.util.List;

import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.User;

public interface IUserDAO extends IDAO<Integer, User> {
	/**
	 * 实现数据的删除操作
	 * 
	 * @param id
	 *            要删除的用户id
	 * @return 删除成功返回true，否则返回false
	 * @throws Exception
	 *             SQL执行异常
	 */
	public boolean doRemove(Integer id) throws Exception;

	/**
	 * 根据用户邮箱查询指定用户内容
	 * 
	 * @param email
	 *            要查询的用户邮箱
	 * @return 如果用户存在，则以vo类对象的形式返回，否则返回null
	 * @throws Exception
	 *             SQL执行异常
	 */
	public User findByEmail(String email) throws Exception;
	
	/**
	 * 查找基本用户
	 * @return
	 * @throws Exception
	 */
	public List<User> findBasicUser() throws Exception;
}
