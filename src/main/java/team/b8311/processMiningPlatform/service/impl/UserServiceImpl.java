package team.b8311.processMiningPlatform.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import team.b8311.processMiningPlatform.dao.IUserDAO;
import team.b8311.processMiningPlatform.domain.Project;
import team.b8311.processMiningPlatform.domain.User;
import team.b8311.processMiningPlatform.service.IUserService;
import team.b8311.processMiningPlatform.utils.MD5Utils;

@Service("userService")
public class UserServiceImpl implements IUserService {

	@Resource
	private IUserDAO userDAO;

	@Override
	public User get(int id) throws Exception {
		return userDAO.findById(id);
	}

	@Override
	public User get(String email) throws Exception {
		return userDAO.findByEmail(email);
	}

	@Override
	public boolean insert(User user) throws Exception {
		if (userDAO.findByEmail(user.getEmail()) == null) {
			user.setPassword(MD5Utils.MD5(user.getPassword())); // 对用户的密码字段进行加密
			return userDAO.doCreate(user);
		}
		return false;
	}

	@Override
	public List<User> getBasicUser() throws Exception {
		// TODO Auto-generated method stub
		return userDAO.findBasicUser();
	}

	@Override
	public boolean delete(int id) throws Exception {
		// TODO Auto-generated method stub
		return userDAO.doRemove(id);
	}

}
